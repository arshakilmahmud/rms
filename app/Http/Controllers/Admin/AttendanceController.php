<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Attendance;
use App\Models\Staff;
use App\Models\Leave;
use Auth;
use Str;
use Image;
use DB;
use Carbon\Carbon;

class AttendanceController extends Controller
{
    
    var $path = 'admin.attendance';
    var $prifix = 'admin.attendances';
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view($this->path.'.index',['attendances'=>Attendance::latest()->get(),'staff'=>Staff::where('status',1)->latest()->get(),'collapsedMenu'=>true]);
    }
    public function get(Request $request)
    {
        $date = $request->date;
        $data = Staff::get()->map(function ($staff, $key) use ($date) {
            $data = Attendance::where('date',$date)->where('staff_id',$staff->id)->first();
            if ($data==null) {
                Attendance::create([
                    'date'=>$date,
                    'staff_id'=>$staff->id,
                ]);
            }
            $s_date = Carbon::parse($date)->format('Y-m-d h:i:s');
            $leave = Leave::where('staff_id',$staff->id)->where('status',1)->whereDate('start_date', '<=', $s_date)
            ->whereDate('end_date', '>=', $s_date)->first();
            return [
                'id' => $staff->id,
                'name' => $staff->name,
                'type' => $staff->type->name,
                'date' => $date,
                'start_time' => $data!=null? $data->start_time :null,
                'end_time' => $data!=null? $data->end_time :null,
                'valid' => $data!=null? $data->valid :null,
                'leave' => $leave,
            ];
        });
       return response()->json($data,200);
    }
    public function getById($id)
    {
       return response()->json(Attendance::with('user','staff')->findOrFail($id));
    }
    public function create ()
    {
        return view($this->path.'.add',['collapsedMenu'=>true]);
    }
    public function edit($id)
    {
        return view($this->path.'.edit',['attendance'=>Attendance::findOrFail($id),'collapsedMenu'=>true]);
    }
    public function store(Request $request)
    {
        //return $request;
         $this->validate($request,[
            'date'=>'required|max:25',
            'staff_id'=>'required|max:190',
            'start_time'=>'required|max:190',
            'approve_remarks'=>'nullable|max:190',
          ]);
         $hh = $request->start_time['hh'];
        if ($request->start_time['A'] =='PM') {
            $hh = $request->start_time['hh']+12;
        }
        $start_time = $request->date.' '.$hh.':'.$request->start_time['mm'].':00';
        $start_time = \Carbon\Carbon::parse($start_time)->format('Y-m-d H:i:s');

        try {
            $check = Attendance::where('date',$request->date)
            ->where('staff_id',$request->staff_id)
            ->first();
            if ($check->start_time == null) {
                $check->update([
                    'date'=>$request->date,
                    'staff_id'=>$request->staff_id,
                    'start_time' => $start_time,
                    'valid' => 1,
                ]);
                return response()->json(['Saved Successfully'],201);
            }else{
                return response()->json(['Attendance already exists in database.'],409);
            }

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
    }

    public function show($id)
    {
        return response()->json(Attendance::findOrFail($id));
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'date'=>'required|max:25',
            'staff_id'=>'required|max:190',
            'end_time'=>'required|max:190',
            'approve_remarks'=>'nullable|max:190',
          ]);
         $hh = $request->end_time['hh'];
        if ($request->end_time['A'] =='PM') {
            $hh = $request->end_time['hh']+12;
        }
        $end_time = $request->date.' '.$hh.':'.$request->end_time['mm'].':00';
        $end_time = \Carbon\Carbon::parse($end_time)->format('Y-m-d H:i:s');

        try {
            $check = Attendance::where('date',$request->date)
            ->where('staff_id',$request->staff_id)
            ->first();
            if ($check->start_time != null and  $check->end_time == null) {
                $check->end_time = $end_time;
                    $check->approve_remarks = $check->approve_remarks.'  -  '.$request->approve_remarks;
                    $check->save();
                    return response()->json(['Checkout Successfully'],201);
            }else{
                if ($check->end_time !=null){
                    return response()->json(['This Person Alredy Checkd Out'],406);
                }elseif  ($check->start_time != null) {
                    return response()->json(['This Person is Not Present'],406);
                }
                else{
                    return response()->json(['This Person is Not Present'],406);
                }                
            }

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
        
    }
    public function destroy($id)
    {
        try {
            $attendance = Attendance::findOrFail($id)->delete();  
            return response()->json(['Deleted Successfully'],201);

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
    }
}

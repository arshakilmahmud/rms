<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ExpenceCategory;
class ExpenceCategoryController extends Controller
{
    var $path = 'admin.expence_category';
    var $prifix = 'admin.expence-categories';
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view($this->path.'.index',['categories'=>ExpenceCategory::latest()->get()]);       
    }
    public function create ()
    {
        return view($this->path.'.add');
    }
    public function edit($id)
    {
        return view($this->path.'.edit',['category'=>ExpenceCategory::findOrFail($id)]);
    }
    public function store(Request $request)
    {
         $this->validate($request,[
             'name'=>'required|min:2|max:190',
             'status'=>'nullable|max:1',
          ]);
          
        try {
            $request['status'] = $request->status;
            $category = ExpenceCategory::create($request->except('_token'));           
            
             notify()->success('Saved Successfully');
             
            if ($request->submit =='s&c') {
                return redirect(route($this->prifix.'.create'));
            }else{
                return redirect(route($this->prifix.'.index'));
            }

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            return $err_message;
            notify()->error($err_message);
            return back();
        }
    }

    public function show($id)
    {
        return response()->json(ExpenceCategory::findOrFail($id));
    }


    public function update(Request $request, $id)
    {
         $this->validate($request,[
             'name'=>'required|min:2|max:190',
             'status'=>'nullable|max:1',
             'image' => 'nullable|mimes:jpeg,jpg,png,gif||max:10000', // max 10000kb
          ]);

        try {
            $request['status'] = $request->status;
            $category = ExpenceCategory::findOrFail($id)->update($request->except('_token'));
            $category = ExpenceCategory::findOrFail($id);


            notify()->success('Updated Successfully'); 
            
            if ($request->submit =='s&c') {
                return redirect(route($this->prifix.'.edit',$id));
            }else{
                return redirect(route($this->prifix.'.index'));
            }   
         return back();
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            notify()->error($err_message);
            return $err_message;
        }
        
    }
    public function destroy($id)
    {
        try {
        $category = ExpenceCategory::findOrFail($id)->delete();                
        notify()->success('Removed Successfully');
        return redirect(route($this->prifix.'.index'));
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            notify()->error($err_message);
            return redirect(route($this->prifix.'.index'));
        }
    }
}

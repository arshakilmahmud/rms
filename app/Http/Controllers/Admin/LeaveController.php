<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Leave;
use App\Models\Staff;
use Auth;
use DB;
use Carbon\Carbon;

class LeaveController extends Controller
{
    
    var $path = 'admin.leave';
    var $prifix = 'admin.leaves';
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view($this->path.'.index',['leaves'=>Leave::latest()->get(),'staff'=>Staff::where('status',1)->latest()->get(),'collapsedMenu'=>true]);
    }
    
    public function get(Request $request)
    {
        if ($request->type=='day') {         
            $range = $this->thisDay($request);   
        }elseif ($request->type=='month') {
            $range = $this->month($request);
        }else{
            $range = $this->month($request);
        }
        $data = Leave::latest()
            ->whereBetween('start_date', $range)
            ->orWhereBetween('end_date', $range)->with('staff')
            ->get()->map(function ($leave, $key) {
            return [
                'id' => $leave->id,
                'staff_id' => $leave->staff_id,
                'reason' => $leave->reason,
                'description' => $leave->description,
                'start_date' => $leave->start_date,
                'end_date' => $leave->end_date,
                'duration' => $this->duration($leave->start_date,$leave->end_date),
                'status' => $leave->status,
                'staff' => $leave->staff,
            ];
        });
       return response()->json($data,200);
    }
    public function duration($fromDate,$toDate)
    {
        
        $fromDate =  Carbon::parse($fromDate);
        $toDate = Carbon::parse($toDate);
        return ($fromDate->diff($toDate)->days < 1) ? 1 : $fromDate->diffInDays($toDate)+1;
    }
    public function thisDay($request)
    {
        $date  = Carbon::now()->toDateTimeString();
        $dateFrom =  Carbon::parse($date)->startOfDay()->toDateTimeString();
        $dateTo = Carbon::parse($date)->endOfDay()->toDateTimeString();
        return [$dateFrom,$dateTo];
    }
    public function month(Request $request)
    {
        $date  = $request->date ?  $request->date : Carbon::now()->toDateTimeString();
        $dateFrom =  Carbon::parse($date)->startOfMonth()->toDateTimeString();
        $dateTo = Carbon::parse($date)->endOfMonth()->toDateTimeString();
        return [$dateFrom,$dateTo];
    }
    public function getById($id)
    {
       return response()->json(Leave::with('user','staff')->findOrFail($id));
    }
    public function create ()
    {
        return view($this->path.'.add',['collapsedMenu'=>true]);
    }
    public function edit($id)
    {
        $leave = Leave::findOrFail($id);
        $data = [
                'id' => $leave->id,
                'staff_id' => $leave->staff_id,
                'reason' => $leave->reason,
                'description' => $leave->description,
                'start_date' => $this->vueDatetimeFormet($leave->start_date),
                'end_date' => $this->vueDatetimeFormet($leave->end_date),
                'status' => $leave->status,
            ];
        return view($this->path.'.edit',[
            'leave'=>$data,
            'collapsedMenu'=>true,
        ]);
    }
    public function vueDatetimeFormet($t)
    {
        return Carbon::parse($t)->format('Y-m-d').'T'.Carbon::parse($t)->format('H:m').':00.000+06:00';
    }
    public function store(Request $request)
    {
         $this->validate($request,[
            'staff_id'=>'required|max:190',
            'reason'=>'required|max:190',
            'description'=>'nullable|max:1000',
            'start_date'=>'required|max:190',
            'end_date'=>'required|max:190',
            'status'=>'required|max:1',
          ]);
         $dateCheck = '';

        try {
            if (1>0) {
                
                Leave::create($request->except('_token'));
                return response()->json(['Saved Successfully'],201);
            }else{
                return response()->json(['Somthing Went Wrong'],409);
            }


        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
    }

    public function show($id)
    {
        return response()->json(Leave::findOrFail($id));
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'staff_id'=>'required|max:190',
            'reason'=>'required|max:190',
            'description'=>'nullable|max:1000',
            'start_date'=>'required|max:190',
            'end_date'=>'required|max:190',
            'status'=>'required|max:1',
          ]);
        try {
            if (1>0) {
                
                Leave::findOrFail($request->id)->update($request->except('_token'));
                return response()->json(['Saved Successfully'],201);
            }else{
                return response()->json(['Somthing Went Wrong'],409);
            }

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
        
    }
    public function destroy($id)
    {
        try {
            $leave = Leave::findOrFail($id)->delete();  
            return response()->json(['Deleted Successfully'],201);

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
    }
}

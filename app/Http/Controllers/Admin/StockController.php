<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Purchase;
use App\Models\PurchaseItem;
use App\Models\Item;
use App\Models\Type;
use App\Models\Stock;
use Auth;
use Str;
use Image;
use DB;
use Carbon\Carbon;

class StockController extends Controller
{
    var $path = 'admin.stock';
    var $prifix = 'admin.stocks';
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view($this->path.'.index',['types'=>Type::where('status',1)->get(),'collapsedMenu'=>true]);       
    }
    public function get(Request $request)
    {
        $data = Item::withFilters($request)->with('type','unit')->get()->map(function ($item, $key) {
            $stocks = Stock::where('item_id',$item->id)->where('status','!=',2)->get();
            $quantity = 0;
            foreach ($stocks as $key => $stock) {
                if ($stock->in==1) {
                   $quantity = $quantity + $stock->quantity;
                }
                if ($stock->weast==1) {
                   $quantity = $quantity - $stock->quantity;
                }
                if ($stock->out==1) {
                   $quantity = $quantity - $stock->quantity;
                }
            }
            
            return [
                'id' => $item->id,
                'image' => $item->image,
                'name' => $item->name,
                'type' => $item->type->name,
                'type_id' => $item->type_id,
                'unit' => $item->unit->name,
                'quantity' => $quantity,
            ];
        });
       return response()->json($data,200);
    }
    public function getById(Request $request,$id)
    {
         $pre_balance = $this->previousBalance($request,$id);
        $balance = $pre_balance;
        $data = Stock::where('item_id',$id)->where('status','!=',2)->withFilters($request)->get();
        
        $bata = [];
        foreach($data as $key => $row){

            if ($row->in==1) {
               $balance = $balance + $row->quantity;
            }
            if ($row->weast==1) {
               $balance = $balance - $row->quantity;
            }
            if ($row->out==1) {
               $balance = $balance - $row->quantity;
            }
            $bata[$key] = [
                "id"=> $row['id'],
                "perches_item_id"=> $row['perches_item_id'],
                "item_id"=> $row['item_id'],
                "in"=> $row['in'],
                "weast"=> $row['weast'],
                "out"=> $row['out'],
                "quantity"=> $row['quantity'],
                "balance"=> $balance,
                "date"=> $row['date'],
                "reason"=> $row['reason'],
                "user_id"=> $row['user_id'],
                "status"=> $row['status'],
            ];  
        }
       return response()->json(['details'=>[
                    'count'=>count($bata),
                    'pre_balance'=>$pre_balance,
                    'in'=>$data->where('in',1)->sum('quantity'),
                    'out'=>$data->where('out',1)->sum('quantity'),
                    'weast'=>$data->where('weast',1)->sum('quantity'),
                    'total'=>$balance -$pre_balance,
                    'balance'=>$balance,
                ],
                'data'=>$bata],200);
    }
    public function create ()
    {
        return view($this->path.'.add',['collapsedMenu'=>true]);
    }
    public function edit($id)
    {
        return view($this->path.'.edit',['data'=>Item::with('stocks')->findOrFail($id),'collapsedMenu'=>true]);
    }
    public function store(Request $request)
    {
         $this->validate($request,[
            'date'=>'required',
            'item_id'=>'required',
            'stock_type'=>'required',
            'quantity'=>'required',
            'reason'=>'nullable',
          ]);
          
        try {
            $stocks = Stock::where('item_id',$request->item_id)->where('status',1)->get();

            $quantity = 0;
            foreach ($stocks as $key => $stock) {
                if ($stock->in==1) {
                   $quantity = $quantity + $stock->quantity;
                }
                if ($stock->weast==1) {
                   $quantity = $quantity - $stock->quantity;
                }
                if ($stock->out==1) {
                   $quantity = $quantity - $stock->quantity;
                }
            }
            if ($request->quantity <= $quantity and $request->quantity >0) {
                $stock = new Stock;
                $stock->date = $request->date;
                $stock->item_id = $request->item_id;
                $stock->quantity = $request->quantity;
                $stock->in = $request->stock_type == 'in' ? 1 : 0;
                $stock->weast = $request->stock_type == 'weast' ? 1 : 0;
                $stock->out = $request->stock_type == 'out' ? 1 : 0;
                $stock->reason = $request->stock_type;
                $stock->status = 1;
                $stock->save();

                return response()->json(['Saved Successfully'],201);
            }else{
                return response()->json(['Not acceptable'],409);
            }

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
    }

    public function show($id)
    {
        return view($this->path.'.view',['data'=>Item::with('unit','type')->findOrFail($id),'collapsedMenu'=>true]);
    }

    public function update(Request $request, $id)
    {
        
         $this->validate($request,[
            'date'=>'required|max:190',
            'supplier_id'=>'required|max:190',
            "products"    => "required|array|min:1",
            "paid"    => "nullable",
            "products.*"  => "required|array",
            'products.*.id' => 'required|max:255',
            'products.*.price' => 'required|max:255',
            'products.*.quantity' => 'required|max:255',
          ]);
          
        try {
            //$request['status'] = $request->status;
            $purchase = Stock::where('id',$id)->first()->update([
                'supplier_id'=>$request->supplier_id,
                'sub_total' => $this->sub_total($request),
                'discount' => $request->discount,
                'total' => $this->total($request),
                'paid' => $request->paid!=null? $request->paid  : 0,
                'due' => $this->total($request) - $request->paid,
                'date' => $request->date,
            ]);
            $purchase = Stock::where('id',$id)->first();

            DB::table('purchase_items')->where('purchase_id',$id)->delete();
            foreach ($request->products as $key => $row) {
                PurchaseItem::create([
                    'purchase_id'=>$purchase->id,
                    'item_id'=>$row['id'],
                    'price'=>$row['price'],
                    'quantity'=>$row['quantity'],
                    'total'=>$row['quantity'] * $row['price'],
                    'previous_stock'=>0,
                    'current_stock'=>0,
                ]);
            }
             return response()->json(['Updated Successfully'],201);

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
        
    }
    public function destroy($id)
    {
        try {
            $purchase = Stock::findOrFail($id)->delete();  
            return response()->json(['Deleted Successfully'],201);

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
    }

    public function month($request)
    {
        $month = $request->month;
        $date  = $month ?  $month : Carbon::now()->toDateTimeString();
        $dateFrom =  Carbon::parse($date)->startOfMonth()->toDateTimeString();
        $dateTo = Carbon::parse($date)->endOfMonth()->toDateTimeString();
        return [$dateFrom,$dateTo];
    }
    
    public function range($request)
    {
        if ($request->month==null) {
            $start_date = $request->start_date !=null ? Carbon::parse($request->start_date)->startOfDay()->toDateTimeString() : Carbon::now()->parse()->startOfDay()->toDateTimeString();
            $end_date = $request->end_date !=null ? Carbon::parse($request->end_date)->endOfDay()->toDateTimeString() : Carbon::now()->parse()->endOfDay()->toDateTimeString();   
            return [$start_date,$end_date];
        }else{
            return $this->month($request);
        }
        
    }

    
    public function previousBalance($request,$id)
    {
        $range = $this->range($request);
        $stocks = Stock::where('item_id',$id)->where('status','!=',2)->whereDate('date', '<', $range[0])->get();

            $quantity = 0;
            foreach ($stocks as $key => $stock) {
                if ($stock->in==1) {
                   $quantity = $quantity + $stock->quantity;
                }
                if ($stock->weast==1) {
                   $quantity = $quantity - $stock->quantity;
                }
                if ($stock->out==1) {
                   $quantity = $quantity - $stock->quantity;
                }
            }
        return $quantity;
        
    }
}

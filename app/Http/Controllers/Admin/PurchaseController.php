<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Purchase;
use App\Models\PurchaseItem;
use App\Models\Ledger;
use App\Models\Stock;
use Auth;
use Str;
use Image;
use DB;
use Carbon\Carbon;

class PurchaseController extends Controller
{
   var $path = 'admin.purchase';
    var $prifix = 'admin.purchases';
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view($this->path.'.index',['purchases'=>Purchase::latest()->get(),'collapsedMenu'=>true]);       
    }
    public function get(Request $request)
    {
        $status= $request->status !=null ? $request->status : null;
        $paginate = $request->paginate=!null ? $request->paginate : 99999;
        $data = Purchase::WithFilters($request)->with('supplier','user')->withCount('items')->paginate($paginate);
        $xdata = Purchase::WithFilters($request)->with('supplier','user')->withCount('items')->get();
        $pre_balance = 0;
        $balence = 0;
        $total = $xdata->where('status','!=',2)->sum('total');
       return response()->json(['details'=>['total'=>$total,'pre_balance'=>$pre_balance,'balence'=>$balence,],'data'=>$data]);
    }
    public function getById($id)
    {
       return response()->json(Purchase::with('supplier','user','items')->withCount('items')->findOrFail($id));
    }
    public function create ()
    {
        return view($this->path.'.add',['collapsedMenu'=>true]);
    }
    public function edit($id)
    {
        return view($this->path.'.edit',['purchase'=>Purchase::findOrFail($id),'collapsedMenu'=>true]);
    }

    public function invoice_id()
    {
        $latestOrder = Purchase::orderBy('created_at','DESC')->first();
        $lastOrderId = ($latestOrder !=null) ? $latestOrder->id : 0;
        return '#'.Str::padLeft($lastOrderId + 1, 8, "0", STR_PAD_LEFT);
    }
    public function sub_total($request)
    {
        $sub_total = 0;
        foreach ($request->products as $key => $value) {
           // $total = $value['price'] * $value['quantity'];
            $total = $value['total'];
            $sub_total += $total;
        }
        return $sub_total;
    }
    public function discount($request)
    {
        $total_discount = 0;
        foreach ($request->products as $key => $value) {
            $discount = $value['discount'] * $value['quantity'];
            $total_discount += $discount;
        }
        return $total_discount;
    }
    public function total($request)
    {
        $sub_total = $this->sub_total($request);
        return $sub_total - $request->discount;
    }
    public function store(Request $request)
    {
         $this->validate($request,[
            'date'=>'required|max:190',
            'supplier_id'=>'required|max:190',
            "products"    => "required|array|min:1",
            "paid"    => "nullable",
            "products.*"  => "required|array",
            'products.*.id' => 'required|max:255',
            'products.*.price' => 'required|max:255',
            'products.*.quantity' => 'required|max:255',
          ]);
          
        try {
            //$request['status'] = $request->status;
            $purchase = Purchase::create([
                'invoice_id'=>$this->invoice_id(),
                'supplier_id'=>$request->supplier_id,
                'sub_total' => $this->sub_total($request),
                'discount' => $request->discount,
                'total' => $this->total($request),
                'paid' => $request->paid!=null? $request->paid  : 0,
                'due' => $this->total($request) - $request->paid,
                'date' => Carbon::parse($request->date)->startOfDay()->format('Y-m-d h:i:s'),
            ]);
            foreach ($request->products as $key => $row) {
                PurchaseItem::create([
                    'purchase_id'=>$purchase->id,
                    'item_id'=>$row['id'],
                    //'price'=>$row['price'],
                    'price'=>$row['total']/$row['quantity'],
                    'quantity'=>$row['quantity'],
                    //'total'=>$row['quantity'] * $row['price'],
                    'total'=>$row['total'],
                    'previous_stock'=>0,
                    'current_stock'=>0,
                    'date' => $purchase->date,
                ]);
            }
             return response()->json(['Saved Successfully'],201);

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
    }

    public function show($id)
    {
        return response()->json(Purchase::findOrFail($id));
    }

    public function update(Request $request, $id)
    {
        
         $this->validate($request,[
            'date'=>'required|max:190',
            'supplier_id'=>'required|max:190',
            "products"    => "required|array|min:1",
            "paid"    => "nullable",
            "products.*"  => "required|array",
            'products.*.id' => 'required|max:255',
            'products.*.price' => 'required|max:255',
            'products.*.quantity' => 'required|max:255',
          ]);
          
        try {
            $purchase = Purchase::where('id',$id)->first()->update([
                'supplier_id'=>$request->supplier_id,
                'sub_total' => $this->sub_total($request),
                'discount' => $request->discount,
                'total' => $this->total($request),
                'paid' => $request->paid!=null? $request->paid  : 0,
                'due' => $this->total($request) - $request->paid,
                'date' => Carbon::parse($request->date)->startOfDay()->format('Y-m-d h:i:s'),
            ]);
            $purchase = Purchase::where('id',$id)->first();

            $items  = PurchaseItem::with('stock')->where('purchase_id',$id)->get();
            foreach($items as $key => $item){
                DB::table('stocks')->where('perches_item_id',$item->id)->delete();
                $item->delete();
            }
            DB::table('purchase_items')->where('purchase_id',$id)->delete();

            foreach ($request->products as $key => $row) {
                PurchaseItem::create([
                    'purchase_id'=>$purchase->id,
                    'item_id'=>$row['id'],
                    //'price'=>$row['price'],
                    'price'=>$row['total']/$row['quantity'],
                    'quantity'=>$row['quantity'],
                    //'total'=>$row['quantity'] * $row['price'],
                    'total'=>$row['total'],
                    'previous_stock'=>0,
                    'current_stock'=>0,
                    'date' => $purchase->date,
                ]);
            }
             return response()->json(['Updated Successfully'],201);

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
        
    }
    public function destroy($id)
    {
        $product = Purchase::findOrFail($id);
            $items  = PurchaseItem::with('stock')->where('purchase_id',$id)->get();
            foreach($items as $key => $item){
                DB::table('stocks')->where('perches_item_id',$item->id)->delete();
                $item->delete();
            }
            DB::table('purchase_items')->where('purchase_id',$id)->delete();
            $product->ledger()->delete();
            $product->delete();
            return response()->json(['data'=>'Successfully Delted'],202);
        try {
            
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());            
            return response()->json(['data'=>$err_message,'status'=>'error','print'=>'false']);
        }
    }
    public function status(Request $request, $id)
    {        
        try {
            $data = Purchase::findOrFail($id);
            $data->status = $request->status;
            $items  = PurchaseItem::with('stock')->where('purchase_id',$id)->get();

            foreach ($items as $key => $item) {
                $stock = Stock::where('perches_item_id',$item->id)
                        ->where('item_id',$item->item_id)
                        ->first();
                if($stock){
                    $stock->status = $request->status;
                    $stock->save();
                }
                
            }

            Ledger::where('ledgerable_type','App\Models\Purchase')
            ->where('ledgerable_id',$data->id)
            ->first()
            ->update([
                'status'=>$request->status,
                'approved_by'=>Auth::id(),
            ]);

            $data->save();
            return response()->json(['data'=>$data,'message'=>'Successfully Saved'],202);  
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());            
            return response()->json(['data'=>$err_message,'status'=>'error']);
        }
    }
}

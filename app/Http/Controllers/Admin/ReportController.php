<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ledger;
use App\Models\OrderItem;
use App\Models\ExpenceCategory;
use App\Models\IncomeCategory;
use App\Models\Product;
use App\Models\Order;
use App\Models\Category;
use App\Models\Customer;
use App\Models\CustomerAccount;
use DB;
use Carbon\Carbon;
class ReportController extends Controller
{
    var $path = 'admin.report';
    var $prifix = 'admin.reports';
    
    public function __construct()
    {
        $this->middleware('auth');
        // $all  = CustomerAccount::all();
        // foreach ($all as $key => $row) {
        //     $row->credit =1;
        //     $row->debit =0;
        //     $row->save();
        // }
    }
    public function index(Request $request)
    {
        $reasons = $this->banglaName();
        return view($this->path.'.index',[
            'reasons'=>$reasons,
            'collapsedMenu'=>true,
        ]);       
    }
    public function get(Request $request)
    {
        $pre_balance = $this->previousBalance($request);
        $balence = $pre_balance;
        $total = 0;
        $data = Ledger::withFilters($request)
        ->with('ledgerable')
        ->get()
        ->map(function ($data, $key){           
            
            return [
                'id' => $data->id,
                'date' => $data->date,
                'reason' => $data->reason,
                'credit' => $data->credit,
                'debit' => $data->debit,
                'amount' => $data->amount,
                'current_balance' => null,//$balence,
                'ledgerable' => $data->ledgerable,
                'status' => $data->ledgerable !=null ? $data->ledgerable->status : null,
            ];
        });

        $bata = [];
        foreach($data as $key => $row){

            if ($row['credit']==1) {
               $balence = $balence - $row['amount'];
            }
            if ($row['debit']==1) {
               $balence = $balence + $row['amount'];
            }
            $bata[$key] = [
                'id' => $row['id'],
                'date' => $row['date'],
                'reason' => $row['reason'],
                'credit' => $row['credit'],
                'debit' => $row['debit'],
                'amount' => $row['amount'],
                'current_balance' => $balence,
                'ledgerable' => $row['ledgerable'],
                'status' => $row['status'],
            ];
            $total = $total +  $row['amount'];   
        }
        $profit = null;
        $income = $data->where('debit',1)->sum('amount');
        $expense = $data->where('credit',1)->sum('amount');
        if ($balence < 0){
               $profit = 0;
        }
        if ($balence > 0){
               $profit = 1;
        }

       return response()->json(
                ['details'=>['total'=>$total,'income'=>$income,'expense'=>$expense,'pre_balance'=>$pre_balance,'balence'=>$balence,'profit'=>$profit,],
                'data'=>$bata],200);
    }
    public function getById($id)
    {
       return response()->json(Ledger::findOrFail($id),200);
    }
    public function orders(Request $request)
    {
        $types = DB::table('products')->select('products.id','products.type as name')->groupBy('type')->get();
        return view($this->path.'.orders',[
            'types'=>$types,
            'collapsedMenu'=>true,
        ]); 
    }
    public function ordersGet(Request $request)
    {
        $balence = 0;
        $pre_balance = 0;
        $data = OrderItem::with('order','product')->withFilters($request)->paginate(9999999999);
        $datax = OrderItem::with('order','product')->withFilters($request)
        ->get();

        $total = $datax->sum('subtotal');
        $item_count = $datax->sum('quantity');

        //return $data;

       return response()->json(
                ['details'=>['total'=>$total,'item_count'=>$item_count],
                'data'=>$data],200);
    }
    public function products(Request $request)
    {
        return view($this->path.'.products',[
            'types'=>Category::where('status',1)->get(),
            'collapsedMenu'=>true,
        ]); 
    }
    public function productsGet(Request $request)
    {
        $data = Product::with('categories')->withFilters($request->name,$request->category_id)->get()->map(function ($product, $key) use($request) {
            $orders = OrderItem::where('product_id',$product->id)->withOrder($request,$product->id)->get();
            $quantity = $orders->sum('quantity');
            return [
                'id' => $product->id,
                'image' => $product->image,
                'name' => $product->name,
                'type' => $product->type,
                'count' => count($orders),
                'quantity' => $quantity,
            ];
        });
        $bata = [];
        foreach($data as $key => $row){
            $bata[$key] = [
                'id' => $row['id'],
                'image' => $row['image'],
                'name' => $row['name'],
                'type' => $row['type'],
                'count' =>$row['count'],
                'quantity' =>$row['quantity'],
            ];
        }
        
        $columns = array_column($bata, "quantity");
        array_multisort($columns, SORT_DESC, $bata);
       return response()->json(
                ['details'=>['item_count'=>count($data)],
                'data'=>$bata],200);
    }
    public function customers(Request $request)
    {
        return view($this->path.'.customers',[
            'collapsedMenu'=>true,
        ]); 
    }
    public function customersGet(Request $request)
    {
        $data = Customer::withFilters($request)->get()->map(function ($customer, $key) use($request) {
            $credits = CustomerAccount::where('customer_id',$customer->id)->withCustomers($request)->get();
            // dd($credits);
            $credit = $credits->where('credit',1)->sum('amount');
            $debit = $credits->where('debit',1)->sum('amount');
            $balance = $debit - $credit;
            $balance_status = 1;
            if($balance>0){
                $balance_status = 1;
            }elseif($balance==0){
                $balance_status = 1;
            }else{
                 $balance_status = 0;
            }
            return [
                'id' => $customer->id,
                'image' => $customer->image!=null ? $customer->image: 'https://via.placeholder.com/40',
                'name' => $customer->name,
                'phone' => $customer->phone,
                'email' => $customer->email,
                'address' => $customer->address,
                'credit' => $credit,
                'debit' => $debit,
                'count' => count($credits),
                'balance' => $balance,
                'balance_status' => $balance_status,
                'status' => $customer->status,
            ];
        });
       return response()->json(
                ['details'=>['item_count'=>count($data)],
                'data'=>$data],200);
    }
    public function credits(Request $request)
    {
        $reasons  = CustomerAccount::select('customer_accounts.id','customer_accounts.reason as name','customer_accounts.ledgerable_type','customer_accounts.ledgerable_id')->groupBy('reason')->get()
        ->map(function ($data, $key){  
            return [
                'id' => $data->id,
                'name' => $data->name,
                'full_name' => $data->name,
            ];
        });
        return view($this->path.'.credits',[
            'reasons'=>$reasons,
            'collapsedMenu'=>true,
        ]);
    }
    public function creditsGet(Request $request)
    {
        $pre_balance = $this->previousCreditBalance($request);
        $balence = $pre_balance;
        $total = 0;
        $data = CustomerAccount::withFilters($request)
        ->with('ledgerable','customer')
        ->get()
        ->map(function ($data, $key){           
            
            return [
                'id' => $data->id,
                'customer' => $data->customer,
                'date' => $data->date,
                'reason' => $data->reason,
                'credit' => $data->credit,
                'debit' => $data->debit,
                'amount' => $data->amount,
                'current_balance' => null, //$balence,
                'ledgerable' => $data->ledgerable,
                'status' => $data->ledgerable !=null ? $data->ledgerable->status : null,
            ];
        });
        
        $bata = [];
        foreach($data as $key => $row){

            if ($row['credit']==1) {
               $balence = $balence - $row['amount'];
            }
            if ($row['debit']==1) {
               $balence = $balence + $row['amount'];
            }
            $bata[$key] = [
                'id' => $row['id'],
                'customer' => $row['customer'],
                'date' => $row['date'],
                'reason' => $row['reason'],
                'credit' => $row['credit'],
                'debit' => $row['debit'],
                'amount' => $row['amount'],
                'current_balance' => $balence,
                'ledgerable' => $row['ledgerable'],
                'status' => $row['status'],
            ];
            $total = $total +  $row['amount'];   
        }
        $profit = null;
        $debit = $data->where('debit',1)->sum('amount');
        $credit = $data->where('credit',1)->sum('amount');
       return response()->json(
                ['details'=>['total'=>$total,'debit'=>$debit,'credit'=>$credit,'pre_balance'=>$pre_balance,'balence'=>$balence,],
                'data'=>$bata],200);
    }
    public function banglaName()
    {
       $data = Ledger::select('ledgers.id','ledgers.reason as name','ledgers.ledgerable_type','ledgers.ledgerable_id')->groupBy('reason')->get()
        ->map(function ($data, $key){  
            $full_name = $data->name; 
            if($data->ledgerable_type=='App\\Models\\Expence'){                
                $cat = ExpenceCategory::where('slug',$data->name)->first();
                if($cat){
                  $full_name = $cat->name;  
                }
            }
            if($data->ledgerable_type=='App\\Models\\Income'){
                $cat = IncomeCategory::where('slug',$data->name)->first();
                if($cat){
                  $full_name = $cat->name;  
                }
                
            }
            return [
                'id' => $data->id,
                'name' => $data->name,
                'full_name' => $full_name,
            ];
        });
        return $data;
    }

    public function month($request)
    {
        $month = $request->month;
        $date  = $month ?  $month : Carbon::now()->toDateTimeString();
        $dateFrom =  Carbon::parse($date)->startOfMonth()->toDateTimeString();
        $dateTo = Carbon::parse($date)->endOfMonth()->toDateTimeString();
        return [$dateFrom,$dateTo];
    }
    
    public function range($request)
    {
        if ($request->month==null) {
            $start_date = $request->start_date !=null ? Carbon::parse($request->start_date)->startOfDay()->toDateTimeString() : Carbon::now()->parse()->startOfDay()->toDateTimeString();
            $end_date = $request->end_date !=null ? Carbon::parse($request->end_date)->endOfDay()->toDateTimeString() : Carbon::now()->parse()->endOfDay()->toDateTimeString();   
            return [$start_date,$end_date];
        }else{
            return $this->month($request);
        }
        
    }
    
    public function previousBalance($request)
    {
        $range = $this->range($request);
        $data = Ledger::whereDate('date', '<', $range[0])->whereHas('ledgerable', function ($query) {
                $query->where('status',1);
            })->get();
        //dd($data);
        $pre_balance = 0;

        foreach ($data as $key => $row) {
            if ($row->credit==1) {
               $pre_balance = $pre_balance - $row->amount;
            }
            if ($row->debit==1) {
               $pre_balance = $pre_balance + $row->amount;
            }
        }
        return $pre_balance;
        
    }
    
    public function previousCreditBalance($request)
    {
        $range = $this->range($request);
        $data = CustomerAccount::whereDate('date', '<', $range[0])->get();
        // dd($range,$data);
        $pre_balance = 0;

        foreach ($data as $key => $row) {
            if ($row->credit==1) {
               $pre_balance = $pre_balance - $row->amount;
            }
            if ($row->debit==1) {
               $pre_balance = $pre_balance + $row->amount;
            }
        }
        return $pre_balance;
        
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AssetCategory;
use Auth;
use Str;
use Image;

class AssetCategoryController extends Controller
{
    
   var $path = 'admin.asset_category';
    var $prifix = 'admin.asset-categories';
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view($this->path.'.index',['asset_categories'=>AssetCategory::latest()->get()]);       
    }
    public function get(Request $request)
    {
        $status= $request->status !=null ? $request->status : null;
        $data = AssetCategory::latest()->when($status==1, function ($query) use ($status) {
            $query->where('status',1);
        })->get()->map(function ($item, $key) {            
            return [
                'code' => $item->id,
                'label' => $item->name,
            ];
        });
       return response()->json(['data'=>$data],200);
    }
    public function getById($id)
    {
       return response()->json(AssetCategory::findOrFail($id),200);
    }
    public function create ()
    {
        return view($this->path.'.add');
    }
    public function edit($id)
    {
        return view($this->path.'.edit',['asset_category'=>AssetCategory::findOrFail($id)]);
    }
    public function store(Request $request)
    {
         $this->validate($request,[
             'name'=>'required|min:2|max:190',
             'image' => 'mimes:jpeg,jpg,png,gif|nullable|max:1024', // max 1mb
          ]);
          
        try {
            $request['status'] = $request->status;
            $asset_category = AssetCategory::create($request->except('_token','image'));           

            if ($request->file('image')) {
                $photoUrl = 'image'.time().'.png';
                $path = public_path().'/uploads/images/asset_categories/';
                $url = '/uploads/images/asset_categories/';
                Image::make(file_get_contents($request->image))->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$photoUrl);
                $asset_category->image =  $url.$photoUrl;
                $asset_category->save();             
            }
            
             notify()->success('Saved Successfully');
             
            if ($request->submit =='s&c') {
                return redirect(route($this->prifix.'.create'));
            }else{
                return redirect(route($this->prifix.'.index'));
            }

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            //return $err_message;
            notify()->error($err_message);
            return back();
        }
    }

    public function show($id)
    {
        return response()->json(AssetCategory::findOrFail($id));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required|min:2|max:190',
            'image' => 'mimes:jpeg,jpg,png,gif|nullable|max:1024', // max 1mb
        ]);

        try {
            $request['status'] = $request->status;
            $asset_category = AssetCategory::findOrFail($id)->update($request->except('_token','image'));
            $asset_category = AssetCategory::findOrFail($id);
            if ($request->file('image')) {
               // return $request->image;
                // if ($asset_category->image and file_exists(public_path().$asset_category->image)) {
                //     unlink(public_path().$asset_category->image);
                // }
               
                $photoUrl = 'image'.time().'.png';
                $path = public_path().'/uploads/images/asset_categories/';
                $url = '/uploads/images/asset_categories/';
                Image::make(file_get_contents($request->image))->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$photoUrl);
                $asset_category->image =  $url.$photoUrl;
                $asset_category->save();                 
            }

            notify()->success('Updated Successfully'); 
            
            if ($request->submit =='s&c') {
                return redirect(route($this->prifix.'.edit',$id));
            }else{
                return redirect(route($this->prifix.'.index'));
            }   
         return back();
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            notify()->error($err_message);
            return back();
        }
    }
    public function destroy($id)
    {        
        try {
            $purchase = AssetCategory::findOrFail($id)->delete();  
            notify()->success('Deleted Successfully');             
            return redirect(route($this->prifix.'.index'));
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            notify()->error($err_message);
            return back();
        }
    }
}

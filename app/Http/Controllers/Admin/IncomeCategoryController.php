<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\IncomeCategory;
use Str;
class IncomeCategoryController extends Controller
{
    var $path = 'admin.income_category';
    var $prifix = 'admin.income-categories';
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view($this->path.'.index',['categories'=>IncomeCategory::latest()->get()]);       
    }
    public function create ()
    {
        return view($this->path.'.add');
    }
    public function edit($id)
    {
        return view($this->path.'.edit',['category'=>IncomeCategory::findOrFail($id)]);
    }
    public function store(Request $request)
    {
         $this->validate($request,[
             'name'=>'required|min:2|max:190',
             'status'=>'nullable|max:1',
          ]);
          
        try {
            $request['status'] = $request->status;
            $category = IncomeCategory::create([
                'name'=>$request->name,
                'slug'=>Str::slug($request->name),
                'status'=>$request->status,
            ]);           
            
             notify()->success('Saved Successfully');
             
            if ($request->submit =='s&c') {
                return redirect(route($this->prifix.'.create'));
            }else{
                return redirect(route($this->prifix.'.index'));
            }

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            return $err_message;
            notify()->error($err_message);
            return back();
        }
    }

    public function show($id)
    {
        return response()->json(IncomeCategory::findOrFail($id));
    }


    public function update(Request $request, $id)
    {
         $this->validate($request,[
             'name'=>'required|min:2|max:190',
             'status'=>'nullable|max:1',
          ]);

        try {
            $category = IncomeCategory::findOrFail($id)->update([
                'name'=>$request->name,
                'slug'=>Str::slug($request->name),
                'status'=>$request->status,
            ]);


            notify()->success('Updated Successfully'); 
            
            if ($request->submit =='s&c') {
                return redirect(route($this->prifix.'.edit',$id));
            }else{
                return redirect(route($this->prifix.'.index'));
            }   
         return back();
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            notify()->error($err_message);
            return $err_message;
        }
        
    }
    public function destroy($id)
    {
        try {
        $category = IncomeCategory::findOrFail($id)->delete();                
        notify()->success('Removed Successfully');
        return redirect(route($this->prifix.'.index'));
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            notify()->error($err_message);
            return redirect(route($this->prifix.'.index'));
        }
    }
}

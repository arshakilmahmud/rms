<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Payroll;
use App\Models\Ledger;
use Auth;
use Carbon\Carbon;
class PayrollController extends Controller
{
   var $path = 'admin.salary';
    var $prifix = 'admin.payrolls';
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view($this->path.'.index',['payrolls'=>null,'collapsedMenu'=>true]);       
    }
    public function get(Request $request)
    {
        $range = $this->month($request);
        $data = Payroll::latest()
            //->whereBetween('date', $range)
            ->with('staff')
            ->get();
        $total = $data->where('status','!=',2)->sum('total_paid');
       return response()->json($data,200);
    }

    public function month(Request $request)
    {
        //->subMonth()
        $date  = $request->date ?  $request->date : Carbon::now()->subMonth(1)->toDateTimeString();
        $dateFrom =  Carbon::parse($date)->startOfMonth()->toDateTimeString();
        $dateTo = Carbon::parse($date)->endOfMonth()->toDateTimeString();
        return [$dateFrom,$dateTo];
    }

    public function getById($id)
    {
       return response()->json(Payroll::with('supplier','user','items')->withCount('items')->findOrFail($id));
    }
    public function create ()
    {
        return view($this->path.'.add',['collapsedMenu'=>true]);
    }
    public function edit($id)
    {
        return view($this->path.'.edit',['purchase'=>Payroll::findOrFail($id),'collapsedMenu'=>true]);
    }
    public function store(Request $request)
    {
        //return 'dsada';
         $this->validate($request,[
            'staff_id'=>'required|max:190',
            'allawence'=>'nullable|max:190',
            "bonus"    => "nullable",
            "advance"    => "nullable",
            "advance_deduction"    => "nullable",
            "absent_deduction"    => "nullable",
            "other_deduction"    => "nullable",
            "basic_salary"    => "required",
            "total_paid"    => "nullable",
            "date"    => "nullable",
            "description"    => "nullable",
          ]);
          
        try {
            $purchase = Payroll::create($request->except('_token'));
            return response()->json(['Saved Successfully'],201);
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
    }

    public function show($id)
    {
        return response()->json(Payroll::findOrFail($id));
    }

    public function update(Request $request, $id)
    {
        
         $this->validate($request,[
            'date'=>'required|max:190',
            'supplier_id'=>'required|max:190',
            "products"    => "required|array|min:1",
            "paid"    => "nullable",
            "products.*"  => "required|array",
            'products.*.id' => 'required|max:255',
            'products.*.price' => 'required|max:255',
            'products.*.quantity' => 'required|max:255',
          ]);
          
        try {
            //$request['status'] = $request->status;
            $purchase = Payroll::where('id',$id)->first()->update([
                'supplier_id'=>$request->supplier_id,
                'sub_total' => $this->sub_total($request),
                'discount' => $this->discount($request),
                'total' => $this->total($request),
                'paid' => $request->paid!=null? $request->paid  : 0,
                'due' => $this->total($request) - $request->paid,
                'date' => $request->date,
            ]);
            $purchase = Payroll::where('id',$id)->first();

             return response()->json(['Updated Successfully'],201);

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
        
    }
    public function destroy($id)
    {
        try {
            Ledger::where('ledgerable_type','App\Models\Payroll')
            ->where('ledgerable_id',$id)
            ->first()->delete();
            $purchase = Payroll::findOrFail($id)->delete();  
            return response()->json(['Deleted Successfully'],201);

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
    }
    


    public function status(Request $request, $id)
    {        
        try {
            $data = Payroll::findOrFail($id);
            $data->status = $request->status;

            Ledger::where('ledgerable_type','App\Models\Payroll')
            ->where('ledgerable_id',$data->id)
            ->first()
            ->update([
                'status'=>$request->status,
                'approved_by'=>Auth::id(),
            ]);

            $data->save();
            return response()->json(['data'=>$data,'message'=>'Successfully Saved'],202);  
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());            
            return response()->json(['data'=>$err_message,'status'=>'error']);
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Asset;
use App\Models\AssetCategory;
use App\Models\AssetStock;
use App\Models\Unit;
use Image;
use DB;
use Carbon\Carbon;
class AssetController extends Controller
{
    var $path = 'admin.asset';
    var $prifix = 'admin.assets';
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view($this->path.'.index',['collapsedMenu'=>true,]);
    }
    public function get(Request $request)
    {
        $data = Asset::with('category')->withFilters($request)->get()->map(function($asset, $key){

            $stocks = AssetStock::where('asset_id',$asset->id)->get();

            $quantity = 0;
            foreach ($stocks as $key => $stock) {
                if ($stock->in==1) {
                   $quantity = $quantity + $stock->quantity;
                }
                if ($stock->weast==1) {
                   $quantity = $quantity - $stock->quantity;
                }
                if ($stock->out==1) {
                   $quantity = $quantity - $stock->quantity;
                }
            }

            return [
                "id"=> $asset->id,
                "name"=> $asset->name,
                "asset_category_id"=> $asset->asset_category_id,
                "slug"=> $asset->slug,
                "image"=> $asset->image,
                "description"=> $asset->description,
                "user_id"=> $asset->user_id,
                "status"=> $asset->status,
                "category"=> $asset->category,
                "quantity"=> $quantity,
                "unit"=> $asset->unit,
                "stock"=> $asset->stock,
            ];
        });
       return response()->json(['data'=>$data],200);
    }

    public function getitem($id)
    {
       return response()->json(Asset::findOrFail($id),200);
    }
    public function create ()
    {
        return view($this->path.'.add',[
            'asset_categories'=>AssetCategory::where('status',1)->get(),
            'units'=>Unit::where('status',1)->get()
        ]);
    }
    public function edit($id)
    {
        return view($this->path.'.edit',[
            'asset'=>Asset::findOrFail($id),
            'asset_categories'=>AssetCategory::where('status',1)->get(),
            'units'=>Unit::where('status',1)->get()
        ]);
    }
    public function store(Request $request)
    {
         $this->validate($request,[
             'name'=>'required|min:2|max:190',
             'asset_category_id'=>'required',
             'image' => 'mimes:jpeg,jpg,png,gif|nullable|max:1024', // max 1mb
          ]);
          
        try {
            $request['status'] = $request->status;
            $asset = Asset::create($request->except('_token','image'));           

            if ($request->file('image')) {
                $photoUrl = 'image'.time().'.png';
                $path = public_path().'/uploads/images/assets/';
                $url = '/uploads/images/assets/';
                Image::make(file_get_contents($request->image))->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$photoUrl);
                $asset->image =  $url.$photoUrl;
                $asset->save();             
            }
            
             notify()->success('Saved Successfully');
             
            if ($request->submit =='s&c') {
                return redirect(route($this->prifix.'.create'));
            }else{
                return redirect(route($this->prifix.'.index'));
            }

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            //return $err_message;
            notify()->error($err_message);
            return back();
        }
    }

    public function show($id)
    {
        $asset = Asset::with('unit','category')->findOrFail($id);
        return view($this->path.'.vew',['collapsedMenu'=>true,'asset'=>$asset]);
    }


    public function update(Request $request, $id)
    {
         $this->validate($request,[
             'name'=>'required|min:2|max:190',
             'asset_category_id'=>'required',
             'image' => 'mimes:jpeg,jpg,png,gif|nullable|max:1024', // max 1mb
          ]);

        try {
            $request['status'] = $request->status;
            $asset = Asset::findOrFail($id)->update($request->except('_token','image'));
            $asset = Asset::findOrFail($id);
            if ($request->file('image')) {
               // return $request->image;
                // if ($asset->image and file_exists(public_path().$asset->image)) {
                //     unlink(public_path().$asset->image);
                // }
               
                $photoUrl = 'image'.time().'.png';
                $path = public_path().'/uploads/images/assets/';
                $url = '/uploads/images/assets/';
                Image::make(file_get_contents($request->image))->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$photoUrl);
                $asset->image =  $url.$photoUrl;
                $asset->save();                 
            }

            notify()->success('Updated Successfully'); 
            
            if ($request->submit =='s&c') {
                return redirect(route($this->prifix.'.edit',$id));
            }else{
                return redirect(route($this->prifix.'.index'));
            }   
         return back();
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            notify()->error($err_message);
            return back();
        }
    }
    public function destroy($id)
    {
        try {
            $data = Asset::findOrFail($id);            
            DB::table('asset_stocks')->where('asset_id',$id)->delete();
            $data->delete();
            return response()->json(['data'=>'Successfully Delted'],202);
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());            
            return response()->json(['data'=>$err_message,'status'=>'error']);
        }
    }
    public function stockDestroy($id)
    {
        try {
            DB::table('asset_stocks')->where('id',$id)->delete();
            return response()->json(['data'=>'Successfully Delted'],202);
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());            
            return response()->json(['data'=>$err_message,'status'=>'error']);
        }
    }
    public function stock(Request $request)
    {        
         $this->validate($request,[
            'date'=>'required',
            'asset_id'=>'required',
            'stock_type'=>'required',
            'quantity'=>'required',
            'reason'=>'nullable',
          ]);
          
        try {
            $stocks = AssetStock::where('asset_id',$request->asset_id)->where('status',1)->get();

            $quantity = 0;
            foreach ($stocks as $key => $stock) {
                if ($stock->in==1) {
                   $quantity = $quantity + $stock->quantity;
                }
                if ($stock->weast==1) {
                   $quantity = $quantity - $stock->quantity;
                }
                if ($stock->out==1) {
                   $quantity = $quantity - $stock->quantity;
                }
            }
            if ($request->quantity <= $quantity and $request->quantity >0 and $request->stock_type != 'in') {
                $stock = new AssetStock;
                $stock->date = $request->date;
                $stock->asset_id = $request->asset_id;
                $stock->quantity = $request->quantity;
                $stock->in = $request->stock_type == 'in' ? 1 : 0;
                $stock->weast = $request->stock_type == 'weast' ? 1 : 0;
                $stock->out = $request->stock_type == 'out' ? 1 : 0;
                $stock->reason = $request->stock_type;
                $stock->status = 1;
                $stock->save();

                return response()->json(['Saved Successfully'],201);
            }elseif($request->stock_type == 'in'){
               $stock = new AssetStock;
                $stock->date = $request->date;
                $stock->asset_id = $request->asset_id;
                $stock->quantity = $request->quantity;
                $stock->in = $request->stock_type == 'in' ? 1 : 0;
                $stock->weast = $request->stock_type == 'weast' ? 1 : 0;
                $stock->out = $request->stock_type == 'out' ? 1 : 0;
                $stock->reason = $request->stock_type;
                $stock->status = 1;
                $stock->save();
                return response()->json(['Saved Successfully'],201);
            }else{
                return response()->json(['Not acceptable'],409);
            }

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
    }
    public function stocks(Request $request,$id)
    {
         $pre_balance = $this->previousBalance($request,$id);
        $balance = $pre_balance;
        $data = AssetStock::where('asset_id',$id)->withFilters($request)->get();
        
        $bata = [];
        foreach($data as $key => $row){

            if ($row->in==1) {
               $balance = $balance + $row->quantity;
            }
            if ($row->weast==1) {
               $balance = $balance - $row->quantity;
            }
            if ($row->out==1) {
               $balance = $balance - $row->quantity;
            }
            $bata[$key] = [
                "id"=> $row['id'],
                "asset_id"=> $row['asset_id'],
                "in"=> $row['in'],
                "weast"=> $row['weast'],
                "out"=> $row['out'],
                "quantity"=> $row['quantity'],
                "balance"=> $balance,
                "date"=> $row['date'],
                "reason"=> $row['reason'],
                "user_id"=> $row['user_id'],
                "status"=> $row['status'],
            ];  
        }
       return response()->json(['details'=>[
                    'count'=>count($bata),
                    'pre_balance'=>$pre_balance,
                    'in'=>$data->where('in',1)->sum('quantity'),
                    'out'=>$data->where('out',1)->sum('quantity'),
                    'weast'=>$data->where('weast',1)->sum('quantity'),
                    'total'=>$balance -$pre_balance,
                    'balance'=>$balance,
                ],
                'data'=>$bata],200);
    }

    public function month($request)
    {
        $month = $request->month;
        $date  = $month ?  $month : Carbon::now()->toDateTimeString();
        $dateFrom =  Carbon::parse($date)->startOfMonth()->toDateTimeString();
        $dateTo = Carbon::parse($date)->endOfMonth()->toDateTimeString();
        return [$dateFrom,$dateTo];
    }
    
    public function range($request)
    {
        if ($request->month==null) {
            $start_date = $request->start_date !=null ? Carbon::parse($request->start_date)->startOfDay()->toDateTimeString() : Carbon::now()->parse()->startOfDay()->toDateTimeString();
            $end_date = $request->end_date !=null ? Carbon::parse($request->end_date)->endOfDay()->toDateTimeString() : Carbon::now()->parse()->endOfDay()->toDateTimeString();   
            return [$start_date,$end_date];
        }else{
            return $this->month($request);
        }
        
    }

    
    public function previousBalance($request,$id)
    {
        $range = $this->range($request);
        $stocks = AssetStock::where('asset_id',$id)->whereDate('date', '<', $range[0])->get();

            $quantity = 0;
            foreach ($stocks as $key => $stock) {
                if ($stock->in==1) {
                   $quantity = $quantity + $stock->quantity;
                }
                if ($stock->weast==1) {
                   $quantity = $quantity - $stock->quantity;
                }
                if ($stock->out==1) {
                   $quantity = $quantity - $stock->quantity;
                }
            }
        return $quantity;
        
    }
}

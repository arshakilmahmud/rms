<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Income;
use App\Models\IncomeCategory;
use App\Models\Ledger;
use Auth;
use Str;
use Image;
use Carbon\Carbon;

class IncomeController extends Controller
{
    var $path = 'admin.income';
    var $prifix = 'admin.incomes';
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view($this->path.'.index',['types'=>IncomeCategory::where('status',1)->get(),'collapsedMenu'=>true,]);       
    }

    
    public function get(Request $request)
    {
        $pre_balance = $this->previousBalance($request);
        $balence = $pre_balance;
        $total = 0;
        $data = Income::WithFilters($request)->with('ledger','category')
        ->paginate(9999999999);
        $xdata = Income::WithFilters($request)->with('ledger','category')->get();

        $total = $xdata->where('status','!=',2)->sum('amount');
        $balence = $pre_balance +  $total;

      

       return response()->json(
                ['details'=>['total'=>$total,'pre_balance'=>$pre_balance,'balence'=>$balence],
                'data'=>$data],200);
    }

    
    public function previousBalance($request)
    {
        $range = $this->range($request);
        $data = Income::whereDate('date', '<', $range[0])->where('status',1)->get();
        //dd($data);
        $pre_balance = 0;

        foreach ($data as $key => $row) {
            
            if ($row->amount>0) {
               $pre_balance = $pre_balance + $row->amount;
            }
        }
        return $pre_balance;
        
    }

    public function month($request)
    {
        $month = $request->month;
        $date  = $month ?  $month : Carbon::now()->toDateTimeString();
        $dateFrom =  Carbon::parse($date)->startOfMonth()->toDateTimeString();
        $dateTo = Carbon::parse($date)->endOfMonth()->toDateTimeString();
        return [$dateFrom,$dateTo];
    }
    
    public function range($request)
    {
        if ($request->month==null) {
            $start_date = $request->start_date !=null ? Carbon::parse($request->start_date)->startOfDay()->toDateTimeString() : Carbon::now()->parse()->startOfDay()->toDateTimeString();
            $end_date = $request->end_date !=null ? Carbon::parse($request->end_date)->endOfDay()->toDateTimeString() : Carbon::now()->parse()->endOfDay()->toDateTimeString();   
            return [$start_date,$end_date];
        }else{
            return $this->month($request);
        }
        
    }

    public function create ()
    {
        return view($this->path.'.add',['categories'=>IncomeCategory::latest()->get()]);
    }
    public function edit($id)
    {
        return view($this->path.'.edit',[
            'income'=>Income::findOrFail($id),
            'categories'=>IncomeCategory::latest()->get(),
        ]);
    }
    public function store(Request $request)
    {
        $this->validate($request,[
             'income_category_id'=>'required',
             'name'=>'max:225',
             'amount'=>'required',
             'date'=>'required',
             'description'=>'max:500',
             'image' => 'mimes:jpeg,jpg,png,gif|nullable|max:10000', // max 10000kb
          ]);
          
        try {
            $income = Income::create($request->except('_token','image'));
            

            if ($request->file('image')) {
                $photoUrl = 'file'.time().'.png';
                $path = public_path().'/uploads/images/incomes';
                $url = '/uploads/images/incomes';

                $file = $request->file('image');
                $file->move($path,$photoUrl);
                $income->image = $url.'/'.$photoUrl;
                $income->save();                 
            }
            
             notify()->success('income Saved Successfully');
             
            if ($request->submit =='s&c') {
                return redirect(route($this->prifix.'.create'));
            }else{
                return redirect(route($this->prifix.'.index'));
            }

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            notify()->error($err_message);
            return back();
        }
    }

    public function show($id)
    {
        return response()->json(Income::findOrFail($id));
    }


    public function update(Request $request, $id)
    {         
        $this->validate($request,[
             'income_category_id'=>'required',
             'name'=>'max:225',
             'amount'=>'required',
             'description'=>'max:500',
             'image' => 'mimes:jpeg,jpg,png,gif|nullable|max:10000', // max 10000kb
          ]);

        try {
            $income = Income::findOrFail($id);
            Income::findOrFail($id)->update($request->except('_token','image'));

            if ($request->file('image')) {
                $photoUrl = 'image'.time().'.png';
                $path = public_path().'/uploads/images/incomes';
                $url = '/uploads/images/incomes';

                $file = $request->file('image');
                $file->move($path,$photoUrl);
                $income->image = $url.'/'.$photoUrl;
                $income->save();                 
            }

            notify()->success('income Updated Successfully'); 
            
            if ($request->submit =='s&c') {
                return redirect(route($this->prifix.'.edit',$id));
            }else{
                return redirect(route($this->prifix.'.index'));
            }   
         return back();
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            notify()->error($err_message);
            return back();
        }
        
    }
    public function destroy($id)
    {
        try {
            $product = Income::findOrFail($id);
            $product->delete();
            return response()->json(['data'=>'Successfully Delted'],202);
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());            
            return response()->json(['data'=>$err_message,'status'=>'error','print'=>'false']);
        }
    }

    public function status(Request $request, $id)
    {        
        try {
            $data = Income::findOrFail($id);
            $data->status = $request->status;

            Ledger::where('ledgerable_type','App\Models\Income')
            ->where('ledgerable_id',$data->id)
            ->first()
            ->update([
                'status'=>$request->status,
                'approved_by'=>Auth::id(),
            ]);

            $data->save();
            return response()->json(['data'=>$data,'message'=>'Successfully Saved'],202);  
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());            
            return response()->json(['data'=>$err_message,'status'=>'error']);
        }
    }
    
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use File;
use Illuminate\Support\Facades\Storage;
use Response;
class BackupController extends Controller
{
    var $path = 'admin.backup';
    var $prifix = 'admin.backups';
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $files = scandir(storage_path('app/Laravel'));
        $data1 = [];
        $last = null;
        $lastid = null;
        foreach ($files as $key => $row) {
            if (strlen($row)>3) {
                $data2 = explode(".",$row);
                if (in_array("zip", $data2)) {
                    $data1[] = [$row];
                    $filename = storage_path('app/Laravel/'.$row);
                    if (file_exists($filename)) {

                        $time = \Carbon\Carbon::parse(filemtime($filename));
                        //$time = filemtime($filename);

                        $result = $time->gt($last);
                        if ($result) {
                            $last = $time;
                            $lastid = $key;
                        }
                        //var_dump($result);
                    }
                }
                
            }
            
        }
        // bazar - salery  
        // advance 
        // gas
        // bidut 
        // chair table
        // bonus 
        // poshak 
        // staf khabr 
        // clining 
        // advertizement 
        // khata kolom 
        // shaban shampu
        // baki 
        // shopon chokroborti

        //echo "last   = ".$last.'<br>';
        //echo "last  = ".$files[$lastid];
        //return $data1;
        //dd($data1);
        //dd($files);

        return view($this->path.'.index',['backups'=>$data1]);       
    }

    public function download($slug)
    {
        $path = storage_path('app/Laravel/'.$slug);
        if (file_exists($path)) {
            return Response::download($path);
        }
    }

    public function destroy($slug)
    {
        $path = storage_path('app/Laravel/'.$slug);
        unlink($path);

        return redirect(route($this->prifix.'.index'));    
    }

    public function create()
    {
        try {
            // start the backup process
            \Artisan::call('backup:run', ['--only-db' => 'true']);
           return  $output = \Artisan::output();
            // log the results
            \Log::info("Backpack\BackupManager -- new backup started from admin interface \r\n" . $output);
            // return the results as a response to the ajax call
            //Alert::success('New backup created');
            return redirect()->back();
        } catch (\Exception $e) {
            \Flash::error($e->getMessage());
            return redirect()->back();
        }

        // \Artisan::call("backup:run"); 
        // dd('done');
        // return redirect(route($this->prifix.'.index'));
    }
}

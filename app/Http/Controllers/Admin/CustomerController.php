<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\CustomerAccount;
use App\Models\Ledger;
use Auth;
use Str;
use Image;
use Carbon\Carbon;
class CustomerController extends Controller
{
    var $path = 'admin.customer';
    var $prifix = 'admin.customers';
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view($this->path.'.index',['customers'=>Customer::latest()->get()]);
    }

    public function show($id)
    {
        $reasons  = CustomerAccount::select('customer_accounts.id','customer_accounts.reason as name','customer_accounts.ledgerable_type','customer_accounts.ledgerable_id')->groupBy('reason')->get()->map(function ($data, $key){  
            return [
                'id' => $data->id,
                'name' => $data->name,
                'full_name' => $data->name,
            ];
        });
        // return $reasons;
        return view($this->path.'.view',[
            'reasons'=>$reasons,
            'collapsedMenu'=>true,
            'customer'=>Customer::findOrFail($id),
        ]);
    }
    public function get(Request $request)
    {
        $paginate = $request->paginate=!null ? $request->paginate : 20;        
       return response()->json(Customer::latest()->withFilters($request)->paginate(20),200);
    }
    public function getWithCode(Request $request)
    {
        
        $customers = Customer::where('status',1)->orderBy('name')->get()->map(function ($item, $key) {            
            return [
                'code' => $item->id,
                'label' => $item->name.' - '.$item->phone ,
            ];
        });
       return response()->json(['data'=>$customers],200);
    }
    public function getById(Request $request,$id)
    {
        $data = Customer::findOrFail($id);
        $credits = CustomerAccount::where('customer_id',$data->id)->withCustomers($request)->get();
            $credit = $credits->where('credit',1)->sum('amount');
            $debit = $credits->where('debit',1)->sum('amount');
            $balance = $debit - $credit;
            $balance_status = 1;
            if($balance>0){
                $balance_status = 1;
            }elseif($balance==0){
                $balance_status = 1;
            }else{
                 $balance_status = 0;
            }
       return response()->json(['data'=>[
        "id"=> $data->id,
        "name"=> $data->name,
        "image"=> $data->image !=null ? $data->image : 'https://via.placeholder.com/500' ,
        "email"=> $data->email,
        "phone"=> $data->phone,
        "occupation"=> $data->occupation,
        "address"=> $data->address,
        "user_id"=> $data->user_id,
        "status"=> $data->status,
        "balance"=> $balance,
        "balance_status"=> $balance_status,
       ]],200);
    }
    public function reports(Request $request,$id)
    {
        $pre_balance = $this->previousCreditBalance($request);
        $balence = $pre_balance;
        $total = 0;
        $data = CustomerAccount::withFilters($request)
        ->with('ledgerable','customer')
        ->get()
        ->map(function ($data, $key){           
            
            return [
                'id' => $data->id,
                'customer' => $data->customer,
                'date' => $data->date,
                'reason' => $data->reason,
                'credit' => $data->credit,
                'debit' => $data->debit,
                'amount' => $data->amount,
                'current_balance' => null, //$balence,
                'ledgerable' => $data->ledgerable,
                'status' => $data->ledgerable !=null ? $data->ledgerable->status : null,
            ];
        });

        $bata = [];
        foreach($data as $key => $row){

            if ($row['credit']==1) {
               $balence = $balence - $row['amount'];
            }
            if ($row['debit']==1) {
               $balence = $balence + $row['amount'];
            }
            $bata[$key] = [
                'id' => $row['id'],
                'customer' => $row['customer'],
                'date' => $row['date'],
                'reason' => $row['reason'],
                'credit' => $row['credit'],
                'debit' => $row['debit'],
                'amount' => $row['amount'],
                'current_balance' => $balence,
                'ledgerable' => $row['ledgerable'],
                'status' => $row['status'],
            ];
            $total = $total +  $row['amount'];   
        }
        $profit = null;
        $debit = $data->where('debit',1)->sum('amount');
        $credit = $data->where('credit',1)->sum('amount');
       return response()->json(
                ['details'=>['total'=>$total,'debit'=>$debit,'credit'=>$credit,'balence'=>$balence,'pre_balance'=>$pre_balance],
                'data'=>$bata],200);
       return response()->json(['data'=>[]],200);
    }
    public function create ()
    {
        return view($this->path.'.add');
    }
    public function edit($id)
    {
        return view($this->path.'.edit',['customer'=>Customer::findOrFail($id)]);
    }
    public function store(Request $request)
    {
         $this->validate($request,[
             'name'=>'required|min:2|max:190',
             'phone'=>'required|min:6|max:25',
             'email'=>'nullable|min:4|max:190',
             'address'=>'nullable|min:4|max:500',
             'image' => 'mimes:jpeg,jpg,png,gif|nullable|max:1024', // max 1mb
          ]);
          
        try {
            $request['status'] = $request->status;
            $customer = Customer::create($request->except('_token','image'));           

            if ($request->file('image')) {
                $photoUrl = 'image'.time().'.png';
                $path = public_path().'/uploads/images/customers/';
                $url = '/uploads/images/customers/';
                Image::make(file_get_contents($request->image))->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$photoUrl);
                $customer->image =  $url.$photoUrl;
                $customer->save();             
            }
            
             notify()->success('Saved Successfully');
             
            if ($request->submit =='s&c') {
                return redirect(route($this->prifix.'.create'));
            }else{
                return redirect(route($this->prifix.'.index'));
            }

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            //return $err_message;
            notify()->error($err_message);
            return back();
        }
    }


    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required|min:2|max:190',
            'phone'=>'required|min:6|max:25',
            'email'=>'nullable|min:4|max:190',
            'address'=>'nullable|min:4|max:500',
            'image' => 'mimes:jpeg,jpg,png,gif|nullable|max:1024', // max 1mb
        ]);

        try {
            $request['status'] = $request->status;
            $customer = Customer::findOrFail($id)->update($request->except('_token','image'));
            $customer = Customer::findOrFail($id);
            if ($request->file('image')) {
               // return $request->image;
                // if ($customer->image and file_exists(public_path().$customer->image)) {
                //     unlink(public_path().$customer->image);
                // }
               
                $photoUrl = 'image'.time().'.png';
                $path = public_path().'/uploads/images/customers/';
                $url = '/uploads/images/customers/';
                Image::make(file_get_contents($request->image))->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$photoUrl);
                $customer->image =  $url.$photoUrl;
                $customer->save();                 
            }

            notify()->success('Updated Successfully'); 
            
            if ($request->submit =='s&c') {
                return redirect(route($this->prifix.'.edit',$id));
            }else{
                return redirect(route($this->prifix.'.index'));
            }   
         return back();
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            notify()->error($err_message);
            return back();
        }
    }
    public function destroy($id)
    {        
        try {
            
            $customer = Customer::findOrFail($id)->delete();  
            return response()->json(['Deleted Successfully'],201);

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
    }
    public function cash(Request $request)
    {
         $this->validate($request,[
            'customer_id'=>'required',
            'phone'=>'required',
            'date'=>'required',
            'amount'=>'required',
            'type'=>'required',
        ]);
        try {
            
            $customer = Customer::where('id',$request->customer_id)
            ->where('phone',$request->phone)
            ->firstOrFail();

                if($request->type == 'in'){
                    $account  = CustomerAccount::create([
                        'ledgerable_type'=>0,
                        'ledgerable_id'=>0,
                        'customer_id'=>$customer->id,
                        'reason'=>'cash-in',
                        'date'=>Carbon::parse($request->date)->startOfDay()->format('Y-m-d h:i:s'),
                        'debit'=>1,
                        'credit'=>0,
                        'status'=>1,
                        'amount'=>$request->amount,
                    ]);
                    Ledger::create([
                    'ledgerable_type'=>'App\Models\CustomerAccount',
                    'ledgerable_id'=>$account->id,
                    'reason'=> $account->reason,
                    'date'=>Carbon::parse($account->date)->startOfDay()->format('Y-m-d h:i:s'),
                    'debit'=>1,
                    'credit'=>0,
                    'status'=>1,
                    'amount'=>$account->amount,
            ]);
                }
            return response()->json(['TK.'.$account->amount.'Cash In Successfully'],201);

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
    }
    
    public function previousCreditBalance($request)
    {
        $range = $this->range($request);
        $data = CustomerAccount::where('customer_id',$request->customer_id)->whereDate('date', '<', $range[0])->get();
        // dd($range,$data);
        $pre_balance = 0;

        foreach ($data as $key => $row) {
            if ($row->credit==1) {
               $pre_balance = $pre_balance - $row->amount;
            }
            if ($row->debit==1) {
               $pre_balance = $pre_balance + $row->amount;
            }
        }
        return $pre_balance;
        
    }
    public function month($request)
    {
        $month = $request->month;
        $date  = $month ?  $month : Carbon::now()->toDateTimeString();
        $dateFrom =  Carbon::parse($date)->startOfMonth()->toDateTimeString();
        $dateTo = Carbon::parse($date)->endOfMonth()->toDateTimeString();
        return [$dateFrom,$dateTo];
    }
    
    public function range($request)
    {
        if ($request->month==null) {
            $start_date = $request->start_date !=null ? Carbon::parse($request->start_date)->startOfDay()->toDateTimeString() : Carbon::now()->parse()->startOfDay()->toDateTimeString();
            $end_date = $request->end_date !=null ? Carbon::parse($request->end_date)->endOfDay()->toDateTimeString() : Carbon::now()->parse()->endOfDay()->toDateTimeString();   
            return [$start_date,$end_date];
        }else{
            return $this->month($request);
        }
        
    }
}

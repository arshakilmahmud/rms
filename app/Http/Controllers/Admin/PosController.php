<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Customer;
use App\Models\CustomerAccount;
use Carbon\Carbon;
use Auth;
use Str;
use Cart;
use Session;
use DB;

class PosController extends Controller
{

    var $path = 'admin.pos';
    var $prifix = 'admin.pos';

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {

        return view($this->path.'.index',[
            'collapsedMenu'=>true,
        ]);
    }
    public function create()
    {
        return view($this->path.'.add',['categories'=>Category::latest()->where('status',1)->get()]);
    }
    public function searchProduct ()
    {
        $products = Product::withFilters(
            request()->input('name'),
            request()->input('category_id'),
        )->get();
        return response()->json($products);
    }
    public function order_code($id)
    {
        //$latestOrder = Order::orderBy('created_at','DESC')->first();
        //$lastOrderId = ($latestOrder !=null) ? $latestOrder->id : 0;
        return '#'.Str::padLeft($id, 8, "0", STR_PAD_LEFT);
    }
    public function order_id()
    {
        $latestOrder = Order::orderBy('created_at','DESC')->first();
        $lastOrderId = ($latestOrder !=null) ? $latestOrder->id : 0;
        return $lastOrderId;
    }
    public function store(Request $request)
    {
        try {
            $items =$request->items;
            if (count($items)>0) {
                $order = new Order;
                $order->name = $request->name?:'N/A';
                $order->table = $request->table;
                $order->kot = $request->kot;
                $order->tax = $request->tax;
                $order->vat = $request->vat;
                $order->discount = $request->discount;
                $order->discount_type = $request->discount_type;
                $order->total_discount = $request->total_discount;
                $order->service_charge = 0;
                $order->total_item = count($request->items);
                $order->total = $this->total($request->items);
                $order->grand_total = $request->grand_total;
                $order->collected =  $request->collected > $request->grand_total ? $request->grand_total :$request->collected ;
                $order->note = $request->note;
                $order->payment_status = 1;
                $order->date = Carbon::parse($request->date)->startOfDay()->format('Y-m-d h:i:s');
                $order->payment_type = $request->payment_type;
                $order->customer_id = $request->customer_id;
                $order->status = 1;
                $order->save();
                $order->order_code = $this->order_code( $order->id);
                $order->save();
                
                if($request->payment_type == 'credit'){
                    CustomerAccount::create([
                        'ledgerable_type'=>'App\Models\Order',
                        'ledgerable_id'=>$order->id,
                        'customer_id'=>$order->customer_id,
                        'reason'=>'credit',
                        'date'=>Carbon::parse($order->date)->startOfDay()->format('Y-m-d h:i:s'),
                        'debit'=>0,
                        'credit'=>1,
                        'amount'=>$order->grand_total - $order->collected,
                    ]);
                }

                foreach($items as $key => $item){
                    $order_item = new OrderItem;
                    $order_item->order_id = $order->id;
                    $order_item->order_code = $order->order_code;
                    $order_item->product_id = $item['id'];
                    $order_item->product_name = $item['name'];
                    $order_item->price = $item['price'];
                    $order_item->quantity = $item['quantity'];
                    $order_item->subtotal = $item['subtotal'];
                    $order_item->date = $order->date;
                    // $order_item->discount = $item['amount'];
                    // $order_item->discount_type = $item['type'];
                    $order_item->save();
                }
                if ($request->type =='s&p') {
                    return response()->json(['print'=>$order->id],201);
                }else{
                    return response()->json('',201);
                }
            }else{
                return response()->json(['print'=> $this->order_id()],200);
            }

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            return response()->json(['message'=>$err_message,'status'=>'error','print'=>'false']);
        }
    }


    public function discount()
    {
        $items = Cart::getContent();
        $total_discount = 0;
            foreach($items as $key => $item){

                    $price = $item->price;
                    $quantity = $item->quantity;
                    $total = $price * $quantity;
                if ($item->attributes->discount) {
                    $type = $item->attributes->type;
                    $discount = $item->attributes->amount;
                    $get_discount = 0;
                    if ($type=='percentage') {
                        $get_discount =$total * $discount/100;
                    }else{
                        $get_discount =$discount;
                    }
                }else{
                    $get_discount =0;
                }

                $total_discount += $get_discount;
            }
        return $total_discount;
    }


    public function total($items)
    {
        $sub_total = 0;
        foreach($items as $key => $item){
            // $price = $item['price'];
            // $quantity = $item['quantity'];
            // $total = $price * $quantity;
            $sub_total += $item['subtotal'];
        }
        return $sub_total;
    }

    public function show($id)
    {
        return response()->json(Category::with('customer')->findOrFail($id));
    }

    public function edit($id)
    {
        $data = Order::with('items','customer','credit','ledger')->findOrFail($id);
        return view($this->path.'.edit-order',[
            'order'=>$data,
            'collapsedMenu'=>true,
        ]);
    }

    public function update(Request $request, $id)
    {

        try {

            $items =$request->items;
            if (count($items)>0) {
                $order = Order::findOrFail($id);
                $order->name = $request->name?:'N/A';
                $order->table = $request->table;
                $order->kot = $request->kot;
                $order->tax = $request->tax;
                $order->vat = $request->vat;
                $order->discount = $request->discount;
                $order->total_discount = $request->total_discount;
                $order->service_charge = 0;
                $order->total_item = count($request->items);
                $order->total = $this->total($request->items);
                $order->grand_total = $request->grand_total;
                $order->discount_type = $request->discount_type;
                $order->collected =  $request->collected > $request->grand_total ? $request->grand_total :$request->collected ;
                $order->note = $request->note;                
                $order->date = Carbon::parse($request->date)->startOfDay()->format('Y-m-d h:i:s');
                $order->payment_type = $request->payment_type;
                $order->customer_id = $request->customer_id;
                $order->payment_status = 1;
                $order->status = 1;
                $order->user_id = Auth::id();
                $order->save();

                DB::table('customer_accounts')->where('ledgerable_type','App\Models\Order')
                    ->where('ledgerable_id',$order->id)
                    ->where('reason','credit')->delete();

                if($request->payment_type == 'credit'){                    
                    CustomerAccount::create([
                        'ledgerable_type'=>'App\Models\Order',
                        'ledgerable_id'=>$order->id,
                        'customer_id'=>$order->customer_id,
                        'reason'=>'credit',
                        'date'=>Carbon::parse($order->date)->startOfDay()->format('Y-m-d h:i:s'),
                        'debit'=>0,
                        'credit'=>1,
                        'amount'=>$order->grand_total - $order->collected,
                    ]);
                }

                DB::table('order_items')->where('order_id',$id)->delete();
                foreach($items as $key => $item){
                    $order_item = new OrderItem;
                    $order_item->order_id = $order->id;
                    $order_item->order_code = $order->order_code;
                    $order_item->product_id = $item['id'];
                    $order_item->product_name = $item['name'];
                    $order_item->price = $item['price'];
                    $order_item->quantity = $item['quantity'];
                    $order_item->subtotal = $item['subtotal'];
                    $order_item->date = $order->date;
                    // $order_item->discount = $item['amount'];
                    // $order_item->discount_type = $item['type'];
                    $order_item->save();
                }
                if ($request->type =='s&p') {
                    return response()->json(['print'=>$order->id],201);
                }else{
                    return response()->json('',201);
                }
            }else{
                return response()->json(['data'=>Order::with('items')->findOrFail($id),'print'=> $this->order_id()],200);
            }

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            return response()->json(['message'=>$err_message,'status'=>'error','print'=>'false']);
        }
    }
    public function print($id=null)
    {
        try {
            $order = Order::find($id);
            if ($order==null) {
                $order = Order::latest()->first();
            }

            return view('admin.pos.print.print',compact('order'));
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
             return response()->json($err_message);
            notify()->error($err_message);
            return redirect(route($this->prifix.'.index'));
        }
    }
}

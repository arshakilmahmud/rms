<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Holiday;
use App\Models\Staff;
use Auth;
use DB;
use Carbon\Carbon;

class HolidayController extends Controller
{
    
    var $path = 'admin.holiday';
    var $prifix = 'admin.holidays';
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view($this->path.'.index',['holidays'=>Holiday::latest()->get(),'staff'=>Staff::where('status',1)->latest()->get(),'collapsedMenu'=>true]);
    }
    
    public function get(Request $request)
    {
        $range = $this->month($request);
        $data = Holiday::latest()
            ->whereBetween('start_date', $range)
            ->orWhereBetween('end_date', $range)
            ->get()->map(function ($holiday, $key) {
            return [
                'id' => $holiday->id,
                'name' => $holiday->name,
                'start_date' => $holiday->start_date,
                'end_date' => $holiday->end_date,
                'duration' => $this->duration($holiday->start_date,$holiday->end_date),
                'status' => $holiday->status,
            ];
        });
       return response()->json($data,200);
    }
    public function duration($fromDate,$toDate)
    {
        $fromDate =  Carbon::parse($fromDate);
        $toDate = Carbon::parse($toDate);
        return ($fromDate->diff($toDate)->days < 1) ? 1 : $fromDate->diffInDays($toDate)+1;
    }
    public function thisDay($request)
    {
        $date  = Carbon::now()->toDateTimeString();
        $dateFrom =  Carbon::parse($date)->startOfDay()->toDateTimeString();
        $dateTo = Carbon::parse($date)->endOfDay()->toDateTimeString();
        return [$dateFrom,$dateTo];
    }
    public function month(Request $request)
    {
        $date  = $request->date ?  $request->date : Carbon::now()->toDateTimeString();
        $dateFrom =  Carbon::parse($date)->startOfMonth()->toDateTimeString();
        $dateTo = Carbon::parse($date)->endOfMonth()->toDateTimeString();
        return [$dateFrom,$dateTo];
    }
    public function getById($id)
    {
       return response()->json(Holiday::with('user','staff')->findOrFail($id));
    }
    public function create ()
    {
        return view($this->path.'.add',['collapsedMenu'=>true]);
    }
    public function edit($id)
    {
        return view($this->path.'.edit',[
            'holiday'=>Holiday::findOrFail($id),
            'collapsedMenu'=>false,
        ]);
    }
    public function vueDatetimeFormet($t)
    {
        return Carbon::parse($t)->format('Y-m-d').'T'.Carbon::parse($t)->format('H:m').':00.000+06:00';
    }
    public function store(Request $request)
    {
         $this->validate($request,[
            'name'=>'required|max:190',
            'start_date'=>'required|max:190',
            'end_date'=>'required|max:190',
            'status'=>'required|max:1',
          ]);

        try {

            Holiday::create($request->except('_token'));
            return response()->json(['Saved Successfully'],201);

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
    }

    public function show($id)
    {
        return response()->json(Holiday::findOrFail($id),200);
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|max:190',
            'start_date'=>'required|max:190',
            'end_date'=>'required|max:190',
            'status'=>'required|max:1',
          ]);

        try {

            Holiday::findOrFail($request->id)->update($request->except('_token'));
            return response()->json(['Saved Successfully'],201);

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
        
    }
    public function destroy($id)
    {
        try {

            $holiday = Holiday::findOrFail($id)->delete();  
            return response()->json(['Deleted Successfully'],201);

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Table;
use Illuminate\Http\Request;
use Auth;
use Str;
use Image;
use DB;
class TableController extends Controller
{
   var $path = 'admin.table';
    var $prifix = 'admin.tables';
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view($this->path.'.index',['tables'=>Table::latest()->get()]);       
    }
    public function get(Request $request)
    {
        $status= $request->status !=null ? $request->status : null;
        //$paginate = $request->paginate=!null ? $request->paginate : 200;
        $paginate = 999;
       return response()->json(Table::when($status==1, function ($query) use ($status) {
            $query->where('status',1);
        })->paginate($paginate),200);
    }
    public function getById($id)
    {
       return response()->json(Table::findOrFail($id),200);
    }
    public function create ()
    {
        return view($this->path.'.add');
    }
    public function edit($id)
    {
        return view($this->path.'.edit',['table'=>Table::findOrFail($id)]);
    }
    public function store(Request $request)
    {
         $this->validate($request,[
             'name'=>'required|min:2|max:190',
                'capasity'=>'required|min:1|max:190|numeric',
             'image' => 'mimes:jpeg,jpg,png,gif|nullable|max:1024', // max 1mb
          ]);
          
        try {
            $request['status'] = $request->status;
            $table = Table::create($request->except('_token','image'));

            if ($request->file('image')) {
                $photoUrl = 'image'.time().'.png';
                $path = public_path().'/uploads/images/tables/';
                $url = '/uploads/images/tables/';
                Image::make(file_get_contents($request->image))->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$photoUrl);
                $table->image =  $url.$photoUrl;
                $table->save();             
            }
            
             notify()->success('Saved Successfully');
             
            if ($request->submit =='s&c') {
                return redirect(route($this->prifix.'.create'));
            }else{
                return redirect(route($this->prifix.'.index'));
            }

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            //return $err_message;
            notify()->error($err_message);
            return back();
        }
    }

    public function show($id)
    {
        return response()->json(Table::findOrFail($id));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required|min:2|max:190',
            'capasity'=>'required|min:1|max:190|numeric',
            'image' => 'mimes:jpeg,jpg,png,gif|nullable|max:1024', // max 1mb
        ]);

        try {
            $request['status'] = $request->status;
            $table = Table::findOrFail($id)->update($request->except('_token','image'));
            $table = Table::findOrFail($id);
            if ($request->file('image')) {
               // return $request->image;
                // if ($table->image and file_exists(public_path().$table->image)) {
                //     unlink(public_path().$table->image);
                // }
               
                $photoUrl = 'image'.time().'.png';
                $path = public_path().'/uploads/images/tables/';
                $url = '/uploads/images/tables/';
                Image::make(file_get_contents($request->image))->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$photoUrl);
                $table->image =  $url.$photoUrl;
                $table->save();                 
            }

            notify()->success('Updated Successfully'); 
            
            if ($request->submit =='s&c') {
                return redirect(route($this->prifix.'.edit',$id));
            }else{
                return redirect(route($this->prifix.'.index'));
            }   
         return back();
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            notify()->error($err_message);
            return back();
        }
    }
    public function destroy($id)
    {        
        try {
            
            $purchase = Table::findOrFail($id)->delete();  
            return response()->json(['Deleted Successfully'],201);

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
    }
}

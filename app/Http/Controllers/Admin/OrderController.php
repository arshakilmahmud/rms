<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Ledger;
use Carbon\Carbon;
use DB;
use Auth;
class OrderController extends Controller
{

    var $path = 'admin.order';
    var $prifix = 'admin.orders';

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view($this->path.'.index',['collapsedMenu'=>true,]);       
    }
    
    public function get(Request $request)
    {
        $pre_balance = $this->previousBalance($request);
        $balence = $pre_balance;
        $total = 0;
        $data = Order::WithFilters($request)->with('ledger','customer')
        ->paginate(9999999999);
        $xdata = Order::WithFilters($request)->with('ledger')
        ->get();
        $total = $xdata->where('status',1)->sum('total');//->where('discount_type','!=','guest')
        // $total_discount = $xdata->where('status',1)->where('discount_type','!=','flat')->sum('total_discount');
         $total_discount = $xdata->where('status',1)->sum('total_discount');

        $receivable =  $xdata->where('status',1)->sum('grand_total');

        $total_collected = $xdata->where('status',1)->sum('collected');
        $total_due = $receivable - $total_collected;
        $balence = $pre_balance +  $total;

       return response()->json(['details'=>[
                                    'total'=>$total,
                                    'total_discount'=>$total_discount,
                                    'total_collected'=>$total_collected,
                                    'receivable'=>$receivable,
                                    'total_due'=>$total_due,
                                    'pre_balance'=>$pre_balance,
                                    'balence'=>$balence
                                ],
                                'data'=>$data
                            ],200);
    }
    public function show($id)
    {
        return view($this->path.'.view',['order'=>$order = Order::with('items','user','customer')->find($id)]);
    }
    public function destroy($id)
    {
        try {
            $data = Order::findOrFail($id);
            
            DB::table('customer_accounts')->where('ledgerable_type','App\Models\Order')
                    ->where('ledgerable_id',$data->id)->delete();
            $data->delete();
            return response()->json(['data'=>'Successfully Delted'],202);
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());            
            return response()->json(['data'=>$err_message,'status'=>'error','print'=>'false']);
        }
    }


    
    public function previousBalance($request)
    {
        $range = $this->range($request);
        $data = Order::whereDate('created_at', '<', $range[0])->where('status',1)->get();
        $pre_balance = 0;
        foreach ($data as $key => $row) {            
            if ($row->amount>0) {
               $pre_balance = $pre_balance + $row->amount;
            }
        }
        return $pre_balance;
        
    }



    public function month($request)
    {
        $month = $request->month;
        $date  = $month ?  $month : Carbon::now()->toDateTimeString();
        $dateFrom =  Carbon::parse($date)->startOfMonth()->toDateTimeString();
        $dateTo = Carbon::parse($date)->endOfMonth()->toDateTimeString();
        return [$dateFrom,$dateTo];
    }
    
    public function range($request)
    {
        if ($request->month==null) {
            $start_date = $request->start_date !=null ? Carbon::parse($request->start_date)->startOfDay()->toDateTimeString() : Carbon::now()->parse()->startOfDay()->toDateTimeString();
            $end_date = $request->end_date !=null ? Carbon::parse($request->end_date)->endOfDay()->toDateTimeString() : Carbon::now()->parse()->endOfDay()->toDateTimeString();   
            return [$start_date,$end_date];
        }else{
            return $this->month($request);
        }
        
    }

    public function status(Request $request, $id)
    {        
        try {
            $data = Order::findOrFail($id);
            $data->status = $request->status;

            Ledger::where('ledgerable_type','App\Models\Order')
            ->where('ledgerable_id',$data->id)
            ->first()
            ->update([
                'status'=>$request->status,
                'approved_by'=>Auth::id(),
            ]);

            $data->save();
            return response()->json(['data'=>$data,'message'=>'Successfully Saved'],202);  
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());            
            return response()->json(['data'=>$err_message,'status'=>'error']);
        }
    }

}

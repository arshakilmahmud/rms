<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Asset;
use App\Models\AssetCategory;
use App\Models\AssetPurchase;
use App\Models\AssetPurchaseItems;
use Auth;
use Str;
use Image;
use DB;

class AssetPurchaseController extends Controller
{
    
   
    var $path = 'admin.asset_purchase';
    var $prifix = 'admin.asset-purchases';
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view($this->path.'.index',['purchases'=>AssetPurchase::latest()->get(),'collapsedMenu'=>true]);       
    }
    public function get(Request $request)
    {
        $status= $request->status !=null ? $request->status : null;
        $paginate = $request->paginate=!null ? $request->paginate : 20;
       return response()->json(AssetPurchase::latest()->when($status==1, function ($query) use ($status) {
            $query->where('status',1);
        })->with('supplier','user')->withCount('items')->paginate(20));
    }
    public function getById($id)
    {
       return response()->json(AssetPurchase::with('supplier','user','items')->withCount('items')->findOrFail($id));
    }
    public function create ()
    {
        return view($this->path.'.add',['collapsedMenu'=>true]);
    }
    public function edit($id)
    {
        return view($this->path.'.edit',['purchase'=>AssetPurchase::findOrFail($id),'collapsedMenu'=>true]);
    }

    public function invoice_id()
    {
        $latestOrder = AssetPurchase::orderBy('created_at','DESC')->first();
        $lastOrderId = ($latestOrder !=null) ? $latestOrder->id : 0;
        return '#'.Str::padLeft($lastOrderId + 1, 8, "0", STR_PAD_LEFT);
    }
    public function sub_total($request)
    {
        $sub_total = 0;
        foreach ($request->products as $key => $value) {
            $total = $value['price'] * $value['quantity'];
            $sub_total += $total;
        }
        return $sub_total;
    }
    public function discount($request)
    {
        $total_discount = 0;
        foreach ($request->products as $key => $value) {
            $discount = $value['discount'] * $value['quantity'];
            $total_discount += $discount;
        }
        return $total_discount;
    }
    public function total($request)
    {
        $grand_total = 0;
        foreach ($request->products as $key => $value) {
            $total = ($value['price'] - $value['discount']) * $value['quantity'];
            $grand_total += $total;
        }
        return $grand_total;
    }
    public function store(Request $request)
    {
         $this->validate($request,[
            'date'=>'required|max:190',
            'supplier_id'=>'required|max:190',
            "products"    => "required|array|min:1",
            "paid"    => "nullable",
            "products.*"  => "required|array",
            'products.*.id' => 'required|max:255',
            'products.*.price' => 'required|max:255',
            'products.*.quantity' => 'required|max:255',
          ]);
          
        try {
            //$request['status'] = $request->status;
            $purchase = AssetPurchase::create([
                'invoice_id'=>$this->invoice_id(),
                'supplier_id'=>$request->supplier_id,
                'sub_total' => $this->sub_total($request),
                'discount' => $this->discount($request),
                'total' => $this->total($request),
                'paid' => $request->paid!=null? $request->paid  : 0,
                'due' => $this->total($request) - $request->paid,
                'date' => $request->date,
            ]);
            foreach ($request->products as $key => $row) {
                AssetPurchaseItems::create([
                    'asset_purchase_id'=>$purchase->id,
                    'item_id'=>$row['id'],
                    'price'=>$row['price'],
                    'quantity'=>$row['quantity'],
                    'total'=>$row['quantity'] * $row['price'],
                    'previous_stock'=>0,
                    'current_stock'=>0,
                ]);
            }
             return response()->json(['Saved Successfully'],201);

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
    }

    public function show($id)
    {
        return response()->json(AssetPurchase::findOrFail($id));
    }

    public function update(Request $request, $id)
    {
        
         $this->validate($request,[
            'date'=>'required|max:190',
            'supplier_id'=>'required|max:190',
            "products"    => "required|array|min:1",
            "paid"    => "nullable",
            "products.*"  => "required|array",
            'products.*.id' => 'required|max:255',
            'products.*.price' => 'required|max:255',
            'products.*.quantity' => 'required|max:255',
          ]);
          //return $request;
        try {
            //$request['status'] = $request->status;
            $purchase = AssetPurchase::where('id',$id)->first()->update([
                'supplier_id'=>$request->supplier_id,
                'sub_total' => $this->sub_total($request),
                'discount' => $this->discount($request),
                'total' => $this->total($request),
                'paid' => $request->paid!=null? $request->paid  : 0,
                'due' => $this->total($request) - $request->paid,
                'date' => $request->date,
            ]);
            $purchase = AssetPurchase::where('id',$id)->first();

            DB::table('asset_purchase_items')->where('asset_purchase_id',$id)->delete();
            foreach ($request->products as $key => $row) {
                AssetPurchaseItems::create([
                    'asset_purchase_id'=>$purchase->id,
                    'item_id'=>$row['id'],
                    'price'=>$row['price'],
                    'quantity'=>$row['quantity'],
                    'total'=>$row['quantity'] * $row['price'],
                    'previous_stock'=>0,
                    'current_stock'=>0,
                ]);
            }
             return response()->json(['Updated Successfully'],201);

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
        
    }
    public function destroy($id)
    {
        try {
            $purchase = AssetPurchase::findOrFail($id)->delete();  
            return response()->json(['Deleted Successfully'],201);

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
           return response()->json([$err_message],500);
        }
    }
}

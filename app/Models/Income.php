<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Income extends Model
{
    use HasFactory;    

    protected $guarded = ['id'];
    
    public function ledgers()
    {
        return $this->morphMany('App\Models\Income', 'ledgerable');
    }

    public function ledger()
    {
        return $this->hasOne(Ledger::class,'ledgerable_id')->where('ledgerable_type','App\Models\Income');
    }

    protected static function boot() {
        parent::boot();

        static::creating(function ($data) {
            $data->date = Carbon::parse($data->date)->startOfDay()->format('Y-m-d h:i:s');
            $data->user_id = isset(auth()->user()->id) ? auth()->user()->id  : null;
            $data->status = 1;
        });
        static::created(function ($data) {
            Ledger::create([
                'ledgerable_type'=>'App\Models\Income',
                'ledgerable_id'=>$data->id,
                'reason'=>$data->category->slug,
                'date'=>Carbon::parse($data->date)->startOfDay()->format('Y-m-d h:i:s'),
                'debit'=>1,
                'credit'=>0,
                'amount'=>$data->amount,
                'status'=>1,
            ]);
        });
        static::updating(function($data)
        {
            $data->date = Carbon::parse($data->date)->startOfDay()->format('Y-m-d h:i:s');
            $data->user_id = isset(auth()->user()->id) ? auth()->user()->id  : $data->user_id;
            $data->status = 1;
        });
        static::updated(function($data)
        {
            Ledger::where('ledgerable_type','App\Models\Income')
            ->where('ledgerable_id',$data->id)
            ->first()
            ->update([
                'reason'=>$data->category->slug,
                'date'=>Carbon::parse($data->date)->startOfDay()->format('Y-m-d h:i:s'),
                'amount'=>$data->amount,
                'status'=>1,
            ]);
        });
        static::deleting(function ($data)
        {
            $data->ledger->delete();
        });
    }



    public function scopeWithFilters($query,$request)
    {

        $status = $request->status !=null ? $request->status : null;
        $sort = $request->sort !=null ? $request->sort : 1;
        $name = $request->name !=null ? $request->name : null;
        $type = $request->type !=null ? $request->type : null;

        $start_date = $request->start_date !=null ? Carbon::parse($request->start_date)->startOfDay()->toDateTimeString() : null;
        $end_date = $request->end_date !=null ? Carbon::parse($request->end_date)->endOfDay()->toDateTimeString() : null;
        $month = $request->month !=null ? $this->month($request->month) : null;

        return $query->when($status!=null, function ($query) use ($status) {

            $query->whereHas('ledger', function ($query) use ($status) {
               $query->where('status',$status);
            });

        })->when($start_date!=null and $end_date!=null, function ($query) use ($start_date,$end_date) {

            $query->whereBetween('date', [$start_date,$end_date]);

        })->when($month!=null, function ($query) use ($month) {

            $query->whereBetween('date', $month);

        })->when($name!=null, function ($query) use ($name) {
            $query->where('name', 'LIKE', "%{$name}%");
        })->when($name!=null, function ($query) use ($name) {
            $query->orWhere('id', 'LIKE', "%{$name}%");
        })->when($type!=null, function ($query) use ($type) {
            $query->where('income_category_id', $type);
        })->when($sort!=null, function ($query) use ($sort) {
            if ($sort==1) {
                $query->orderBy('date','desc')->orderBy('created_at','desc');
            }else{
                $query->orderBy('date','asc')->orderBy('created_at','asc');
            }
        });
    }


    public function month($month)
    {
        $date  = $month ?  $month : Carbon::now()->toDateTimeString();
        $dateFrom =  Carbon::parse($date)->startOfMonth()->toDateTimeString();
        $dateTo = Carbon::parse($date)->endOfMonth()->toDateTimeString();
        return [$dateFrom,$dateTo];
    }

    public function category()
    {
        return $this->belongsTo(IncomeCategory::class,'income_category_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}

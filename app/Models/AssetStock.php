<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class AssetStock extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected static function boot() {
        parent::boot();

        static::creating(function ($type) {
            $type->date = Carbon::parse($type->date)->startOfDay()->format('Y-m-d h:i:s');
            $type->user_id = isset(auth()->user()->id) ? auth()->user()->id  : null;
        });
        static::updating(function($type)
        {
            $type->date = Carbon::parse($type->date)->startOfDay()->format('Y-m-d h:i:s');
            $type->user_id = isset(auth()->user()->id) ? auth()->user()->id  : $type->user_id;
        });
        static::deleting(function ($type)
        {
            // if ($type->image and file_exists(public_path().$type->image)) {
            //     unlink(public_path().$type->image);
            // }
            //$type->attachments()->delete();
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function item()
    {
        return $this->belongsTo(Asset::class,'asset_id');
    }


    public function scopeWithFilters($query, $request)
    {        
        $sort = $request->sort !=null ? $request->sort : 1;
        $type = $request->type !=null ? $request->type : 1;

        $start_date = $request->start_date !=null ? Carbon::parse($request->start_date)->startOfDay()->toDateTimeString() : null;
        $end_date = $request->end_date !=null ? Carbon::parse($request->end_date)->endOfDay()->toDateTimeString() : null;
        $month = $request->month !=null ? $this->month($request->month) : null;

        return $query->when($sort!=null, function ($query) use ($sort) {
            if ($sort) {
                $query->latest();
            }else{
                $query->oldest();
            }            
        })->when($type!=null and $type!='' , function ($query) use ($type) {
           if ($type=='in') {
               $query->where('in',1);
            }
           if ($type=='out') {
               $query->where('out',1);
            }
           if ($type=='weast') {
               $query->where('weast',1);
            }
        })->when($start_date!=null and $end_date!=null, function ($query) use ($start_date,$end_date) {

            $query->whereBetween('date', [$start_date,$end_date]);

        })->when($month!=null, function ($query) use ($month) {

            $query->whereBetween('date', $month);

        });
    }


    public function month($month)
    {
        $date  = $month ?  $month : Carbon::now()->toDateTimeString();
        $dateFrom =  Carbon::parse($date)->startOfMonth()->toDateTimeString();
        $dateTo = Carbon::parse($date)->endOfMonth()->toDateTimeString();
        return [$dateFrom,$dateTo];
    }
}

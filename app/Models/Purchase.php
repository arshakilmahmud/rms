<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Ledger;
use Carbon\Carbon;
class Purchase extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    
    public function ledgers()
    {
        return $this->morphMany('App\Models\Purchase', 'ledgerable');
    }

    public function ledger()
    {
        return $this->hasOne(Ledger::class,'ledgerable_id')->where('ledgerable_type','App\Models\Purchase');
    }

    protected static function boot() {
        parent::boot();

        static::creating(function ($data) {
            $data->user_id = isset(auth()->user()->id) ? auth()->user()->id  : null;
        });

        static::created(function ($data) {
            Ledger::create([
                'ledgerable_type'=>'App\Models\Purchase',
                'ledgerable_id'=>$data->id,
                'reason'=>'purchase',
                'date'=>Carbon::parse($data->date)->startOfDay()->format('Y-m-d h:i:s'),
                'debit'=>0,
                'credit'=>1,
                'amount'=>$data->paid,
            ]);
        });
        static::updating(function($data)
        {
            $data->user_id = isset(auth()->user()->id) ? auth()->user()->id  : $data->user_id;
        });
        static::updated(function($data)
        {
            Ledger::where('ledgerable_type','App\Models\Purchase')
            ->where('ledgerable_id',$data->id)
            ->first()
            ->update([
                'date'=>Carbon::parse($data->date)->startOfDay()->format('Y-m-d h:i:s'),
                'amount'=>$data->paid,
            ]);
            
        });
        static::deleting(function ($data)
        {            
            if($data->items){
                $data->items()->delete();
            }
            if($data->ledger){
              $data->ledger()->delete();  
            }            
        });
    }


    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function stocks()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function supplier()
    {
        return $this->belongsTo(Supplier::class,'supplier_id');
    }
    public function items()
    {
        return $this->hasMany(PurchaseItem::class,'purchase_id')->with('item');
    }

    public function scopeWithFilters($query,$request)
    {
        $status = $request->status !=null ? $request->status : null;
        $sort = $request->sort !=null ? $request->sort : 1;
        $name = $request->name !=null ? $request->name : null;

        $start_date = $request->start_date !=null ? Carbon::parse($request->start_date)->startOfDay()->toDateTimeString() : null;
        $end_date = $request->end_date !=null ? Carbon::parse($request->end_date)->endOfDay()->toDateTimeString() : null;
        $month = $request->month !=null ? $this->month($request->month) : null;

        return $query->when($status!=null, function ($query) use ($status) {

            $query->whereHas('ledger', function ($query) use ($status) {
               $query->where('status',$status);
            });

        })->when($status!=null, function ($query) use ($status) {
            
            $query->whereHas('ledger', function ($query) use ($status) {
               $query->where('status',$status);
            });

        })->when($start_date!=null and $end_date!=null, function ($query) use ($start_date,$end_date) {

            $query->whereBetween('date', [$start_date,$end_date]);

        })->when($month!=null, function ($query) use ($month) {

            $query->whereBetween('date', $month);

        })->when($name!=null, function ($query) use ($name) {
            $query->whereHas('supplier', function ($query) use ($status) {
               $query->where('name', 'LIKE', "%{$name}%");
            });            
        })->when($name!=null, function ($query) use ($name) {
            $query->orWhere('id', 'LIKE', "%{$name}%");
        })->when($name!=null, function ($query) use ($name) {
            $query->orWhere('invoice_id', 'LIKE', "%{$name}%");
        })->when($sort!=null, function ($query) use ($sort) {
            if ($sort==1) {
                $query->orderBy('date','desc')->orderBy('created_at','desc');
            }else{
                $query->orderBy('date','asc')->orderBy('created_at','asc');
            }
        });
    }


    public function month($month)
    {
        $date  = $month ?  $month : Carbon::now()->toDateTimeString();
        $dateFrom =  Carbon::parse($date)->startOfMonth()->toDateTimeString();
        $dateTo = Carbon::parse($date)->endOfMonth()->toDateTimeString();
        return [$dateFrom,$dateTo];
    }
}

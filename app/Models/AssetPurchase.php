<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Ledger;
use Carbon\Carbon;
class AssetPurchase extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    
    public function ledgers()
    {
        return $this->morphMany('App\Models\AssetPurchase', 'ledgerable');
    }

    protected static function boot() {
        parent::boot();

        static::creating(function ($data) {
            $data->user_id = isset(auth()->user()->id) ? auth()->user()->id  : null;
            
        });

        static::created(function ($data) {
            $data->user_id = isset(auth()->user()->id) ? auth()->user()->id  : null;
            Ledger::create([
                'ledgerable_type'=>'App\Models\AssetPurchase',
                'ledgerable_id'=>$data->id,
                'reason'=>'purchase',
                'date'=>Carbon::parse($data->date)->startOfDay()->format('Y-m-d h:i:s'),
                'debit'=>0,
                'credit'=>1,
                'amount'=>$data->paid,
            ]);
        });
        static::updating(function($data)
        {
            $data->user_id = isset(auth()->user()->id) ? auth()->user()->id  : $data->user_id;
        });
        static::updated(function($data)
        {
            Ledger::where('ledgerable_type','App\Models\AssetPurchase')
            ->where('ledgerable_id',$data->id)
            ->first()
            ->update([
                'date'=>Carbon::parse($data->date)->startOfDay()->format('Y-m-d h:i:s'),
                'debit'=>0,
                'credit'=>1,
                'amount'=>$data->paid,
            ]);
            
        });
        static::deleting(function ($data)
        {
            // if ($data->image and file_exists(public_path().$data->image)) {
            //     unlink(public_path().$data->image);
            // }
            $data->items()->delete();
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function supplier()
    {
        return $this->belongsTo(Supplier::class,'supplier_id');
    }
    public function items()
    {
        return $this->hasMany(AssetPurchaseItems::class,'asset_purchase_id')->with('item');
    }
}

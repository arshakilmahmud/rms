<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected static function boot() {
        parent::boot();

        static::creating(function ($data) {
            $data->user_id = isset(auth()->user()->id) ? auth()->user()->id  : null;
        });
        static::updating(function($data)
        {
            $data->user_id = isset(auth()->user()->id) ? auth()->user()->id  : $data->user_id;
        });
        static::deleting(function ($data)
        {
            if ($data->image and file_exists(public_path().$data->image)) {
                unlink(public_path().$data->image);
            }
            //$data->items()->delete();
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function accounts()
    {
        return $this->hasMany(CustomerAccount::class,'customer_id');
    }

    public function scopeWithFilters($query, $request)
    {
        $name = $request->name;
        $sort = $request->sort;
        return $query->when($name!=null, function ($query) use ($name) {
            $query->where('name', 'LIKE', "%{$name}%");
            $query->orWhere('phone', 'LIKE', "%{$name}%");
            $query->orWhere('email', 'LIKE', "%{$name}%");
            $query->orWhere('address', 'LIKE', "%{$name}%");
        })->when($sort!=null, function ($query) use ($sort) {
            if ($sort) {
                $query->orderBy('name','desc');
            }else{
                $query->orderBy('name');
            }
        });
    }
}

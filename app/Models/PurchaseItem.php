<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;
use Carbon\Carbon;
class PurchaseItem extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected static function boot() {
        parent::boot();

        static::creating(function ($data) {
            $data->user_id = isset(auth()->user()->id) ? auth()->user()->id  : null;
        });
        static::created(function ($data) {
            Stock::create([
                'perches_item_id'=>$data->id,
                'item_id'=>$data->item_id,
                'reason'=>'purchase',
                'date'=>Carbon::parse($data->date)->startOfDay()->format('Y-m-d h:i:s'),
                'quantity'=>$data->quantity,
                'in'=>1,
                'out'=>0,
                'weast'=>0,
                'user_id'=>Auth::id(),
                'status'=>1,
            ]);
        });
        static::updating(function($data){
            $data->user_id = isset(auth()->user()->id) ? auth()->user()->id  : $data->user_id;
        });
        static::updated(function($data){
            $stock =  Stock::where('perches_item_id',$data->id)
            ->where('item_id',$data->item_id)
            ->first();
            if($stock){
               Stock::where('perches_item_id',$data->perches_item_id)
                ->where('item_id',$data->item_id)
                ->first()->update([
                    'quantity'=>$data->quantity,
                ]); 
            }            
        });
        static::deleting(function ($data){
            if($data->stock){
                $data->stock()->delete();
            }
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function stock()
    {
        return $this->hasMany(Stock::class,'perches_item_id');
    }
    public function item()
    {
        return $this->belongsTo(Item::class,'item_id')->with('unit');
    }
}

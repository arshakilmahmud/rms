<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Ledger extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function ledgerable()
    {
        return $this->morphTo();
    }

    public function scopeWithFilters($query, $request)
    {
        //$status = $request->status !=null ? $request->status : null;
        $status = 1;
        $sort = $request->sort !=null ? $request->sort : 1;
        $reason = $request->reason !=null ? $request->reason : null;
        $start_date = $request->start_date !=null ? Carbon::parse($request->start_date)->startOfDay()->toDateTimeString() : null;
        $end_date = $request->end_date !=null ? Carbon::parse($request->end_date)->endOfDay()->toDateTimeString() : null;
        $month = $request->month !=null ? $this->month($request->month) : null;

        return $query->when($status!=null, function ($query) use ($status) {

            $query->whereHas('ledgerable', function ($query) use ($status) {
               $query->where('status',$status);
            });
            $query->where('status',$status);

        })->when($reason!='' and $reason!=null, function ($query) use ($reason) {

            $query->where('reason',$reason);

        })->when($start_date!=null and $end_date!=null, function ($query) use ($start_date,$end_date) {

            $query->whereBetween('date', [$start_date,$end_date]);

        })->when($month!=null, function ($query) use ($month) {

            $query->whereBetween('date', $month);

        })->when($sort!=null, function ($query) use ($sort) {
            if ($sort==1) {
                $query->orderBy('date','desc')->orderBy('created_at','desc');
            }else{
                $query->orderBy('date','asc')->orderBy('created_at','asc');
            }
        });
    }


    public function month($month)
    {
        $date  = $month ?  $month : Carbon::now()->toDateTimeString();
        $dateFrom =  Carbon::parse($date)->startOfMonth()->toDateTimeString();
        $dateTo = Carbon::parse($date)->endOfMonth()->toDateTimeString();
        return [$dateFrom,$dateTo];
    }
}

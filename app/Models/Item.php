<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Str;
class Item extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected static function boot() {
        parent::boot();

        static::creating(function ($type) {
            $type->slug = Str::slug($type->name);
            $type->user_id = isset(auth()->user()->id) ? auth()->user()->id  : null;
        });
        static::updating(function($type)
        {
            $type->user_id = isset(auth()->user()->id) ? auth()->user()->id  : $type->user_id;
        });
        static::deleting(function ($type)
        {
            if ($type->image and file_exists(public_path().$type->image)) {
                unlink(public_path().$type->image);
            }
            //$type->attachments()->delete();
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function type()
    {
        return $this->belongsTo(Type::class,'type_id');
    }
    public function unit()
    {
        return $this->belongsTo(Unit::class,'unit_id');
    }
    public function stocks()
    {
        return $this->hasMany(Stock::class);
    }

    public function scopeWithFilters($query,$request)
    {
        $status = $request->status !=null ? $request->status : null;
        $sort = $request->sort !=null ? $request->sort : 1;
        $name = $request->name !=null ? $request->name : null;
        $type = $request->type !=null ? $request->type : null;
        return $query->when($status!=null, function ($query) use ($status) {
                $query->where('status',$status);
            })->when($name!=null, function ($query) use ($name) {
                $query->where('name', 'LIKE', "%{$name}%");
            })->when($type!=null or $type !='', function ($query) use ($type) {
                $query->where('type_id',$type);
            })->when($sort!=null, function ($query) use ($sort) {
                if ($sort) {
                    $query->orderBY('name');
                }else{
                    $query->orderBY('name');
                }            
            });
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Str;
class IncomeCategory extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected static function boot() {
        parent::boot();

        static::creating(function ($data) {
            $data->slug = Str::slug($data->name);
            $data->user_id = isset(auth()->user()->id) ? auth()->user()->id  : null;            
        });
        static::updating(function($data)
        {
            $data->slug = Str::slug($data->name);
            $data->user_id = isset(auth()->user()->id) ? auth()->user()->id  : $data->user_id;
        });
        static::deleting(function ($data)
        {
            // if ($data->image and file_exists(public_path().$data->image)) {
            //     unlink(public_path().$data->image);
            // }
            //$data->attachments()->delete();
        });
    }
}

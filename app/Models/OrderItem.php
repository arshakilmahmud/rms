<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class OrderItem extends Model
{
    use HasFactory;

    public function order()
    {
         return $this->belongsTo(Order::class);
    }
    public function product()
    {
         return $this->belongsTo(Product::class);
    }


    public function scopeWithFilters($query,$request)
    {

        $type = $request->type !=null ? $request->type : null;
        $sort = $request->sort !=null ? $request->sort : 1;
        $name = $request->name !=null ? $request->name : null;
        $start_date = $request->start_date !=null ? Carbon::parse($request->start_date)->startOfDay()->toDateTimeString() : null;
        $end_date = $request->end_date !=null ? Carbon::parse($request->end_date)->endOfDay()->toDateTimeString() : null;
        $month = $request->month !=null ? $this->month($request->month) : null;
        return $query->when($start_date!=null and $end_date!=null, function ($query) use ($start_date,$end_date) {
            
             $query->whereBetween('date', [$start_date,$end_date]);

               })->when($month!=null, function ($query) use ($month) {
                
                 $query->whereBetween('date', $month);

               })->when($name!=null, function ($query) use ($name) {
                    $query->whereHas('product', function ($query) use ($name) {
                         $query->where('name', 'LIKE', "%{$name}%");
                    });
               })->when($type!=null, function ($query) use ($type) {
                    $query->whereHas('product', function ($query) use ($type) {
                         $query->where('type',$type);
                    });                 
               })->when($name!=null, function ($query) use ($name) {
                 $query->orWhere('id', 'LIKE', "%{$name}%");
               })->when($name!=null, function ($query) use ($name) {
                 $query->orWhere('order_code', 'LIKE', "%{$name}%");
               })->when($sort!=null, function ($query) use ($sort) {
                    if ($sort==1) {
                        $query->orderBy('date','desc');
                    }else{
                        $query->orderBy('date','asc');
                    }   
               });
    }


    public function scopeWithOrder($query,$request,$product_id)
    {

        $start_date = $request->start_date !=null ? Carbon::parse($request->start_date)->startOfDay()->toDateTimeString() : null;
        $end_date = $request->end_date !=null ? Carbon::parse($request->end_date)->endOfDay()->toDateTimeString() : null;
        $month = $request->month !=null ? $this->month($request->month) : null;

        return $query->when($start_date!=null and $end_date!=null, function ($query) use ($start_date,$end_date) {

            $query->with('order')->whereHas('product', function ($query) {
                         $query->where('status','!=',2);
                    })->whereBetween('date', [$start_date,$end_date]);
               })->when($month!=null, function ($query) use ($month) {
                 $query->whereBetween('date', $month);
               });
    }


    public function month($month)
    {
        $date  = $month ?  $month : Carbon::now()->toDateTimeString();
        $dateFrom =  Carbon::parse($date)->startOfMonth()->toDateTimeString();
        $dateTo = Carbon::parse($date)->endOfMonth()->toDateTimeString();
        return [$dateFrom,$dateTo];
    }
}

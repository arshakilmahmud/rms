<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Leave extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    protected static function boot() {
        parent::boot();

        static::creating(function ($type) {
            $type->start_date = Carbon::parse($type->start_date)->format('Y-m-d h:i:s');
            $type->end_date = Carbon::parse($type->end_date)->format('Y-m-d h:i:s');
            $type->user_id = isset(auth()->user()->id) ? auth()->user()->id  : null;
        });
        static::updating(function($type)
        {
            $type->start_date = Carbon::parse($type->start_date)->format('Y-m-d h:i:s');
            $type->end_date = Carbon::parse($type->end_date)->format('Y-m-d h:i:s');
            $type->user_id = isset(auth()->user()->id) ? auth()->user()->id  : $type->user_id;
        });
        static::deleting(function ($type)
        {
         
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function staff()
    {
        return $this->belongsTo(Staff::class,'staff_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\Ledger;

class Payroll extends Model
{
    use HasFactory;
    

    protected $guarded = ['id'];
    
    public function ledgers()
    {
        return $this->morphMany('App\Models\Payroll', 'ledgerable');
    }

    protected static function boot() {
        parent::boot();

        static::creating(function ($data) {
            $data->date = Carbon::parse($data->date)->startOfDay()->format('Y-m-d h:i:s');
            $data->user_id = isset(auth()->user()->id) ? auth()->user()->id  : null;
            $data->status = 1;
        });
        static::created(function ($data) {
            Ledger::create([
                'ledgerable_type'=>'App\Models\Payroll',
                'ledgerable_id'=>$data->id,
                'reason'=>'salary',
                'date'=>Carbon::parse($data->date)->startOfDay()->format('Y-m-d h:i:s'),
                'debit'=>0,
                'credit'=>1,
                'amount'=>$data->total_paid,
                'status'=>1,
            ]);
        });
        static::updating(function($data)
        {
            $data->date = Carbon::parse($data->date)->startOfDay()->format('Y-m-d h:i:s');
            $data->user_id = isset(auth()->user()->id) ? auth()->user()->id  : $data->user_id;
            $data->status = 1;
        });
        static::updated(function($data)
        {
            Ledger::where('ledgerable_type','App\Models\Payroll')
            ->where('ledgerable_id',$data->id)
            ->first()
            ->update([
                'date'=>Carbon::parse($data->date)->startOfDay()->format('Y-m-d h:i:s'),
                'amount'=>$data->total_paid,
                'status'=>1,
            ]);
        });
        static::deleting(function ($data)
        {
            // if ($data->image and file_exists(public_path().$data->image)) {
            //     unlink(public_path().$data->image);
            // }
            //$data->attachments()->delete();
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function staff()
    {
        return $this->belongsTo(Staff::class,'staff_id')->with('type');
    }
}

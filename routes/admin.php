<?php

use Illuminate\Support\Facades\Route;


Route::middleware('auth')->group(function(){

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashbord', 'HomeController@index')->name('dashbord');
Route::get('/', 'HomeController@index');
	
	Route::prefix('/categories')->name('categories.')->group(function(){
		Route::get('/', 'CategoryController@index')->name('index');
		Route::get('/get', 'CategoryController@get')->name('get');
		Route::get('/create', 'CategoryController@create')->name('create');
		Route::post('/store', 'CategoryController@store')->name('store');
		Route::get('show/{id}', 'CategoryController@show')->name('show');
		Route::get('/{id}/edit', 'CategoryController@edit')->name('edit');
		Route::post('update/{id}', 'CategoryController@update')->name('update');
		Route::post('destroy/{id}', 'CategoryController@destroy')->name('destroy');
	});
	Route::prefix('/products')->name('products.')->group(function(){
		Route::get('/', 'ProductController@index')->name('index');
		Route::get('/get', 'ProductController@get')->name('get');
		Route::get('/getitem/{id}', 'ProductController@getitem')->name('getitem');
		Route::get('/create', 'ProductController@create')->name('create');
		Route::post('/store', 'ProductController@store')->name('store');
		Route::get('show/{id}', 'ProductController@show')->name('show');
		Route::get('/edit/{id}', 'ProductController@edit')->name('edit');
		Route::post('update/{id}', 'ProductController@update')->name('update');
		Route::post('destroy/{id}', 'ProductController@destroy')->name('destroy');
	});

	Route::prefix('/customers')->name('customers.')->group(function(){
		Route::get('/', 'CustomerController@index')->name('index');
		Route::get('/get', 'CustomerController@get')->name('get');
		Route::get('/get-with-code', 'CustomerController@getWithCode')->name('get-with-code');
		Route::get('/getitem/{id}', 'CustomerController@getById')->name('getitem');
		Route::get('/reports/{id}', 'CustomerController@reports')->name('reports');
		Route::get('/create', 'CustomerController@create')->name('create');
		Route::post('/store', 'CustomerController@store')->name('store');
		Route::get('show/{id}', 'CustomerController@show')->name('show');
		Route::get('/edit/{id}', 'CustomerController@edit')->name('edit');
		Route::post('update/{id}', 'CustomerController@update')->name('update');
		Route::post('cash', 'CustomerController@cash')->name('cash');
		Route::post('destroy/{id}', 'CustomerController@destroy')->name('destroy');
	});

	Route::prefix('/pos')->name('pos.')->group(function(){
		Route::get('/', 'PosController@index')->name('index');
		Route::get('/list', 'PosController@index')->name('list');
		Route::get('/get', 'PosController@get')->name('get');
		Route::get('/search-product', 'PosController@searchProduct')->name('search-product');
		Route::get('/create', 'PosController@create')->name('create');
		Route::post('/store', 'PosController@store')->name('store');
		Route::get('show/{id}', 'PosController@show')->name('show');
		Route::get('/{id}/edit', 'PosController@edit')->name('edit');
		Route::post('update/{id}', 'PosController@update')->name('update');
		Route::post('destroy/{id}', 'PosController@destroy')->name('destroy');
		Route::get('/print/{id?}', 'PosController@print')->name('print');
		Route::get('/grandtotal', 'PosController@grand_total')->name('grandtotal');
		Route::get('/discount', 'PosController@discount')->name('discount');
		Route::get('/total', 'PosController@total')->name('total');
		Route::post('/store-billing', 'PosController@storeBilling')->name('store-billing');
		Route::post('/clear', 'PosController@clear')->name('clear');
	});

	Route::prefix('/orders')->name('orders.')->group(function(){
		Route::get('/', 'OrderController@index')->name('index');
		Route::get('/get', 'OrderController@get')->name('get');
		Route::get('/getitem/{id}', 'OrderController@getitem')->name('getitem');
		Route::get('/show/{id}', 'OrderController@show')->name('show');
		Route::get('/edit/{id}', 'PosController@edit')->name('edit');
		Route::post('update/{id}', 'PosController@update')->name('update');
		Route::post('destroy/{id}', 'OrderController@destroy')->name('destroy');		
		Route::post('status/{id}', 'OrderController@status')->name('status');
	});
	
	Route::prefix('/types')->name('types.')->group(function(){
		Route::get('/', 'TypeController@index')->name('index');
		Route::get('/get', 'TypeController@get')->name('get');
		Route::get('/getitem/{id}', 'TypeController@getitem')->name('getitem');
		Route::get('/create', 'TypeController@create')->name('create');
		Route::post('/store', 'TypeController@store')->name('store');
		Route::get('show/{id}', 'TypeController@show')->name('show');
		Route::get('/edit/{id}', 'TypeController@edit')->name('edit');
		Route::post('update/{id}', 'TypeController@update')->name('update');
		Route::post('destroy/{id}', 'TypeController@destroy')->name('destroy');
	});
	
	Route::prefix('/units')->name('units.')->group(function(){
		Route::get('/', 'UnitsController@index')->name('index');
		Route::get('/get', 'UnitsController@get')->name('get');
		Route::get('/getitem/{id}', 'UnitsController@getitem')->name('getitem');
		Route::get('/create', 'UnitsController@create')->name('create');
		Route::post('/store', 'UnitsController@store')->name('store');
		Route::get('show/{id}', 'UnitsController@show')->name('show');
		Route::get('/edit/{id}', 'UnitsController@edit')->name('edit');
		Route::post('update/{id}', 'UnitsController@update')->name('update');
		Route::post('destroy/{id}', 'UnitsController@destroy')->name('destroy');
	});
	
	Route::prefix('/items')->name('items.')->group(function(){
		Route::get('/', 'ItemController@index')->name('index');
		Route::get('/get', 'ItemController@get')->name('get');
		Route::get('/getitem/{id}', 'ItemController@getitem')->name('getitem');
		Route::get('/create', 'ItemController@create')->name('create');
		Route::post('/store', 'ItemController@store')->name('store');
		Route::get('show/{id}', 'ItemController@show')->name('show');
		Route::get('/edit/{id}', 'ItemController@edit')->name('edit');
		Route::post('update/{id}', 'ItemController@update')->name('update');
		Route::post('destroy/{id}', 'ItemController@destroy')->name('destroy');
	});
	
	Route::prefix('/tables')->name('tables.')->group(function(){
		Route::get('/', 'TableController@index')->name('index');
		Route::get('/get', 'TableController@get')->name('get');
		Route::get('/getitem/{id}', 'TableController@getitem')->name('getitem');
		Route::get('/create', 'TableController@create')->name('create');
		Route::post('/store', 'TableController@store')->name('store');
		Route::get('show/{id}', 'TableController@show')->name('show');
		Route::get('/edit/{id}', 'TableController@edit')->name('edit');
		Route::post('update/{id}', 'TableController@update')->name('update');
		Route::post('destroy/{id}', 'TableController@destroy')->name('destroy');
	});
	
	
	Route::prefix('/suppliers')->name('suppliers.')->group(function(){
		Route::get('/', 'SupplierController@index')->name('index');
		Route::get('/get', 'SupplierController@get')->name('get');
		Route::get('/getitem/{id}', 'SupplierController@getitem')->name('getitem');
		Route::get('/create', 'SupplierController@create')->name('create');
		Route::post('/store', 'SupplierController@store')->name('store');
		Route::get('show/{id}', 'SupplierController@show')->name('show');
		Route::get('/edit/{id}', 'SupplierController@edit')->name('edit');
		Route::post('update/{id}', 'SupplierController@update')->name('update');
		Route::post('destroy/{id}', 'SupplierController@destroy')->name('destroy');
	});
	
	Route::prefix('/purchases')->name('purchases.')->group(function(){
		Route::get('/', 'PurchaseController@index')->name('index');
		Route::get('/get', 'PurchaseController@get')->name('get');
		Route::get('/getitem/{id}', 'PurchaseController@getById')->name('getitem');
		Route::get('/create', 'PurchaseController@create')->name('create');
		Route::post('/store', 'PurchaseController@store')->name('store');
		Route::get('show/{id}', 'PurchaseController@show')->name('show');
		Route::get('/edit/{id}', 'PurchaseController@edit')->name('edit');
		Route::post('update/{id}', 'PurchaseController@update')->name('update');
		Route::post('destroy/{id}', 'PurchaseController@destroy')->name('destroy');
		Route::post('status/{id}', 'PurchaseController@status')->name('status');
	});
	
	Route::prefix('/stocks')->name('stocks.')->group(function(){
		Route::get('/', 'StockController@index')->name('index');
		Route::get('/get', 'StockController@get')->name('get');
		Route::get('/getitem/{id}', 'StockController@getById')->name('getitem');
		Route::get('/create', 'StockController@create')->name('create');
		Route::post('/store', 'StockController@store')->name('store');
		Route::get('show/{id}', 'StockController@show')->name('show');
		Route::get('/edit/{id}', 'StockController@edit')->name('edit');
		Route::post('update/{id}', 'StockController@update')->name('update');
		Route::post('destroy/{id}', 'StockController@destroy')->name('destroy');
	});
	
	Route::prefix('/stafftypes')->name('staff-types.')->group(function(){
		Route::get('/', 'StaffTypeController@index')->name('index');
		Route::get('/get', 'StaffTypeController@get')->name('get');
		Route::get('/getitem/{id}', 'StaffTypeController@getitem')->name('getitem');
		Route::get('/create', 'StaffTypeController@create')->name('create');
		Route::post('/store', 'StaffTypeController@store')->name('store');
		Route::get('show/{id}', 'StaffTypeController@show')->name('show');
		Route::get('/edit/{id}', 'StaffTypeController@edit')->name('edit');
		Route::post('update/{id}', 'StaffTypeController@update')->name('update');
		Route::post('destroy/{id}', 'StaffTypeController@destroy')->name('destroy');
	});
	
	Route::prefix('/staff')->name('staff.')->group(function(){
		Route::get('/', 'StaffController@index')->name('index');
		Route::get('/get', 'StaffController@get')->name('get');
		Route::get('/getitem/{id}', 'StaffController@getitem')->name('getitem');
		Route::get('/getattendance', 'StaffController@getAttendance')->name('getattendance');
		Route::get('/create', 'StaffController@create')->name('create');
		Route::post('/store', 'StaffController@store')->name('store');
		Route::get('show/{id}', 'StaffController@show')->name('show');
		Route::get('/edit/{id}', 'StaffController@edit')->name('edit');
		Route::post('update/{id}', 'StaffController@update')->name('update');
		Route::post('destroy/{id}', 'StaffController@destroy')->name('destroy');
	});
	
	Route::prefix('/asset-categories')->name('asset-categories.')->group(function(){
		Route::get('/', 'AssetCategoryController@index')->name('index');
		Route::get('/get', 'AssetCategoryController@get')->name('get');
		Route::get('/getitem/{id}', 'AssetCategoryController@getitem')->name('getitem');
		Route::get('/create', 'AssetCategoryController@create')->name('create');
		Route::post('/store', 'AssetCategoryController@store')->name('store');
		Route::get('show/{id}', 'AssetCategoryController@show')->name('show');
		Route::get('/edit/{id}', 'AssetCategoryController@edit')->name('edit');
		Route::post('update/{id}', 'AssetCategoryController@update')->name('update');
		Route::post('destroy/{id}', 'AssetCategoryController@destroy')->name('destroy');
	});
	
	Route::prefix('/assets')->name('assets.')->group(function(){
		Route::get('/', 'AssetController@index')->name('index');
		Route::get('/get', 'AssetController@get')->name('get');
		Route::get('/filter', 'AssetController@filter')->name('filter');
		Route::get('/getitem/{id}', 'AssetController@getitem')->name('getitem');
		Route::get('/create', 'AssetController@create')->name('create');
		Route::post('/store', 'AssetController@store')->name('store');
		Route::get('show/{id}', 'AssetController@show')->name('show');
		Route::get('/edit/{id}', 'AssetController@edit')->name('edit');
		Route::post('update/{id}', 'AssetController@update')->name('update');
		Route::post('destroy/{id}', 'AssetController@destroy')->name('destroy');
		Route::post('stock', 'AssetController@stock')->name('stock');
		Route::post('stock-destroy/{id}', 'AssetController@stockDestroy')->name('stock-destroy');
		Route::get('stocks/{id}', 'AssetController@stocks')->name('stocks');
	});
	
	Route::prefix('/asset-purchases')->name('asset-purchases.')->group(function(){
		Route::get('/', 'AssetPurchaseController@index')->name('index');
		Route::get('/get', 'AssetPurchaseController@get')->name('get');
		Route::get('/getitem/{id}', 'AssetPurchaseController@getById')->name('getitem');
		Route::get('/create', 'AssetPurchaseController@create')->name('create');
		Route::post('/store', 'AssetPurchaseController@store')->name('store');
		Route::get('show/{id}', 'AssetPurchaseController@show')->name('show');
		Route::get('/edit/{id}', 'AssetPurchaseController@edit')->name('edit');
		Route::post('update/{id}', 'AssetPurchaseController@update')->name('update');
		Route::post('destroy/{id}', 'AssetPurchaseController@destroy')->name('destroy');
	});
	
	Route::prefix('/payrolls')->name('payrolls.')->group(function(){
		Route::get('/', 'PayrollController@index')->name('index');
		Route::get('/get', 'PayrollController@get')->name('get');
		Route::get('/getitem/{id}', 'PayrollController@getById')->name('getitem');
		Route::get('/create', 'PayrollController@create')->name('create');
		Route::post('/store', 'PayrollController@store')->name('store');
		Route::get('show/{id}', 'PayrollController@show')->name('show');
		Route::get('/edit/{id}', 'PayrollController@edit')->name('edit');
		Route::post('update/{id}', 'PayrollController@update')->name('update');
		Route::post('destroy/{id}', 'PayrollController@destroy')->name('destroy');
		Route::post('status/{id}', 'PayrollController@status')->name('status');
	});
	
	Route::prefix('/attendances')->name('attendances.')->group(function(){
		Route::get('/', 'AttendanceController@index')->name('index');
		Route::get('/get', 'AttendanceController@get')->name('get');
		Route::get('/getitem/{id}', 'AttendanceController@getById')->name('getitem');
		Route::get('/create', 'AttendanceController@create')->name('create');
		Route::post('/store', 'AttendanceController@store')->name('store');
		Route::get('show/{id}', 'AttendanceController@show')->name('show');
		Route::get('/edit/{id}', 'AttendanceController@edit')->name('edit');
		Route::post('update', 'AttendanceController@update')->name('update');
		Route::post('destroy/{id}', 'AttendanceController@destroy')->name('destroy');
	});
	
	Route::prefix('/leaves')->name('leaves.')->group(function(){
		Route::get('/', 'LeaveController@index')->name('index');
		Route::get('/get', 'LeaveController@get')->name('get');
		Route::get('/getitem/{id}', 'LeaveController@getById')->name('getitem');
		Route::get('/create', 'LeaveController@create')->name('create');
		Route::post('/store', 'LeaveController@store')->name('store');
		Route::get('show/{id}', 'LeaveController@show')->name('show');
		Route::get('/edit/{id}', 'LeaveController@edit')->name('edit');
		Route::post('update', 'LeaveController@update')->name('update');
		Route::post('destroy/{id}', 'LeaveController@destroy')->name('destroy');
	});
	
	Route::prefix('/holidays')->name('holidays.')->group(function(){
		Route::get('/', 'HolidayController@index')->name('index');
		Route::get('/get', 'HolidayController@get')->name('get');
		Route::get('/getitem/{id}', 'HolidayController@getById')->name('getitem');
		Route::get('/create', 'HolidayController@create')->name('create');
		Route::post('/store', 'HolidayController@store')->name('store');
		Route::get('show/{id}', 'HolidayController@show')->name('show');
		Route::get('/edit/{id}', 'HolidayController@edit')->name('edit');
		Route::post('update', 'HolidayController@update')->name('update');
		Route::post('destroy/{id}', 'HolidayController@destroy')->name('destroy');
	});
	
	Route::prefix('/reports')->name('reports.')->group(function(){
		Route::get('/', 'ReportController@index')->name('index');
		Route::get('/orders', 'ReportController@orders')->name('orders');
		Route::get('/orders/get', 'ReportController@ordersGet')->name('orders-get');
		Route::get('/products', 'ReportController@products')->name('products');
		Route::get('/products/get', 'ReportController@productsGet')->name('products-get');
		Route::get('/credits', 'ReportController@credits')->name('credits');
		Route::get('/credits/get', 'ReportController@creditsGet')->name('credits-get');
		Route::get('/customers', 'ReportController@customers')->name('customers');
		Route::get('/customers/get', 'ReportController@customersGet')->name('customers-get');
		Route::get('/items', 'ReportController@items')->name('items');
		Route::get('/items/get', 'ReportController@itemsGet')->name('items-get');
		Route::get('/get', 'ReportController@get')->name('get');
		Route::get('/getitem/{id}', 'ReportController@getById')->name('getitem');
		Route::get('show/{id}', 'ReportController@show')->name('show');
		Route::post('update', 'ReportController@update')->name('update');
		Route::post('destroy/{id}', 'ReportController@destroy')->name('destroy');
	});

	Route::prefix('/expence-categories')->name('expence-categories.')->group(function(){
		Route::get('/', 'ExpenceCategoryController@index')->name('index');
		Route::get('/create', 'ExpenceCategoryController@create')->name('create');
		Route::post('/store', 'ExpenceCategoryController@store')->name('store');
		Route::get('show/{id}', 'ExpenceCategoryController@show')->name('show');
		Route::get('/edit/{id}', 'ExpenceCategoryController@edit')->name('edit');
		Route::post('update/{id}', 'ExpenceCategoryController@update')->name('update');
		Route::post('destroy/{id}', 'ExpenceCategoryController@destroy')->name('destroy');
	});

	Route::prefix('/income-categories')->name('income-categories.')->group(function(){
		Route::get('/', 'IncomeCategoryController@index')->name('index');
		Route::get('/create', 'IncomeCategoryController@create')->name('create');
		Route::post('/store', 'IncomeCategoryController@store')->name('store');
		Route::get('show/{id}', 'IncomeCategoryController@show')->name('show');
		Route::get('/edit/{id}', 'IncomeCategoryController@edit')->name('edit');
		Route::post('update/{id}', 'IncomeCategoryController@update')->name('update');
		Route::post('destroy/{id}', 'IncomeCategoryController@destroy')->name('destroy');
	});
	
	Route::prefix('/incomes')->name('incomes.')->group(function(){
		Route::get('/', 'IncomeController@index')->name('index');
		Route::get('/get', 'IncomeController@get')->name('get');
		Route::get('/getitem/{id}', 'IncomeController@getById')->name('getitem');
		Route::get('/create', 'IncomeController@create')->name('create');
		Route::post('/store', 'IncomeController@store')->name('store');
		Route::get('show/{id}', 'IncomeController@show')->name('show');
		Route::get('/edit/{id}', 'IncomeController@edit')->name('edit');
		Route::post('update/{id}', 'IncomeController@update')->name('update');
		Route::post('destroy/{id}', 'IncomeController@destroy')->name('destroy');		
		Route::post('status/{id}', 'IncomeController@status')->name('status');
	});
	
	Route::prefix('/expences')->name('expences.')->group(function(){
		Route::get('/', 'ExpenceController@index')->name('index');
		Route::get('/get', 'ExpenceController@get')->name('get');
		Route::get('/getitem/{id}', 'ExpenceController@getById')->name('getitem');
		Route::get('/create', 'ExpenceController@create')->name('create');
		Route::post('/store', 'ExpenceController@store')->name('store');
		Route::get('show/{id}', 'ExpenceController@show')->name('show');
		Route::get('/edit/{id}', 'ExpenceController@edit')->name('edit');
		Route::post('update/{id}', 'ExpenceController@update')->name('update');
		Route::post('destroy/{id}', 'ExpenceController@destroy')->name('destroy');		
		Route::post('status/{id}', 'ExpenceController@status')->name('status');
	});

	
	
	Route::prefix('/web-infos')->name('web.')->group(function(){
		Route::get('/', 'WebInfoController@index')->name('index');
		Route::post('update', 'WebInfoController@update')->name('update');
	});
	Route::prefix('/profile')->name('profile.')->group(function(){
		Route::get('/', 'AccountController@index')->name('index');
		Route::get('/edit', 'AccountController@edit')->name('edit');
		Route::post('update', 'AccountController@update')->name('update');
		Route::post('change-password', 'AccountController@change_password')->name('change-password');
	});
	Route::prefix('/backups')->name('backups.')->group(function(){
		Route::get('/', 'BackupController@index')->name('index');
		Route::get('/create', 'BackupController@create')->name('create');
		Route::get('/edit', 'BackupController@edit')->name('edit');
		Route::get('download/{slg}', 'BackupController@download')->name('download');
		Route::post('destroy/{slg}', 'BackupController@destroy')->name('destroy');
	});

	Route::prefix('/admins')->name('admins.')->group(function(){
		Route::get('/', 'AdminController@index')->name('index')->middleware('permission:admins-index');
		Route::get('/get-admins', 'AdminController@getAdmins')->name('get-admins')->middleware('permission:admins-create');
		Route::get('/create', 'AdminController@create')->name('create')->middleware('permission:admins-index');
		Route::post('/store', 'AdminController@store')->name('store')->middleware('permission:admins-create');
		Route::get('show/{id}', 'AdminController@show')->name('show')->middleware('permission:admins-show');
		Route::get('get-role/{id}', 'AdminController@getRole')->name('get-role')->middleware('permission:admins-show');
		Route::get('/edit/{id}', 'AdminController@edit')->name('edit')->middleware('permission:admins-update');
		Route::post('update/{id}', 'AdminController@update')->name('update')->middleware('permission:admins-update');
		Route::post('destroy/{id}', 'AdminController@destroy')->name('destroy')->middleware('permission:admins-destroy');
	});

	Route::prefix('/roles')->name('roles.')->group(function(){
		Route::get('/', 'RoleController@index')->name('index')->middleware('permission:roles-index');
		Route::get('/get-roles', 'RoleController@getRoles')->name('get-roles')->middleware('permission:roles-index');
		Route::get('/create', 'RoleController@create')->name('create')->middleware('permission:roles-create');
		Route::post('/store', 'RoleController@store')->name('store')->middleware('permission:roles-create');
		Route::get('show/{id}', 'RoleController@show')->name('show')->middleware('permission:roles-show');
		Route::get('/edit/{id}', 'RoleController@edit')->name('edit')->middleware('permission:roles-update');
		Route::post('update/{id}', 'RoleController@update')->name('update')->middleware('permission:roles-update');
		Route::post('destroy/{id}', 'RoleController@destroy')->name('destroy')->middleware('permission:roles-destroy');
	});

});
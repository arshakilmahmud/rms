@extends('admin.layouts.master')
@section('content')

<div class="br-pageheader">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
    <a class="breadcrumb-stafftype" href="{{ route('home') }}">{{ config('app.name', 'Laravel') }}</a>
    <a class="breadcrumb-stafftype text-capitalize" href="{{ route('admin.assets.index') }}">Asset</a>
    <span class="breadcrumb-stafftype active">Add</span>
  </nav>
</div>
<div class="br-pagetitle">
  <i class="fa fa-industry" aria-hidden="true"></i>
  <div>
    <h4 class="text-capitalize">Asset  Add</h4>
    <p class="mg-b-0"></p>
  </div>
</div>

<div class="br-pagebody">
  <div class="br-section-wrapper">
    <div class="row">
      <div class="col-6">
            
        <h6 class="br-section-label text-capitalize">Asset  Add</h6>
        <p class="br-section-text"></p>
      </div>
      <div class="col-6">
        <a href="{{ route('admin.assets.index') }}" class="btn btn-primary float-right text-capitalize">Asset  List</a>
      </div>
    </div>
    <form action="{{ route('admin.assets.store') }}" method="POST" enctype="multipart/form-data">
      @csrf
    <div class="form-layout form-layout-1">
      <div class="row mg-b-25">
        <div class="col-lg-6">
          <div class="form-group">
              <label class="form-control-label">Name: <span class="tx-danger">*</span></label>
            <input class="form-control" type="text" name="name" value="{{ old('name') }}" placeholder="Enter Name" required>  
            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>
        </div>
        <div class="col-lg-6">
          <div class="form-group">
              <label class="form-control-label">Category: <span class="tx-danger">*</span></label>
            <select name="asset_category_id" id="asset_category_id" class="form-control">
              <option value="">Select Category</option>
              @foreach($asset_categories  as $category)
              <option value="{{ $category->id }}" {{ old('asset_category_id') == $category->id ? 'selected': null }} >{{ $category->name }}</option>
              @endforeach
            </select>
            @error('asset_category_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>
        </div><!-- col-4 -->
        <div class="col-lg-6">
          <div class="form-group">
              <label class="form-control-label">Unit: <span class="tx-danger">*</span></label>
            <select name="unit_id" id="unit_id" class="form-control">
              <option value="">Select Unit</option>
              @foreach($units  as $unit)
              <option value="{{ $unit->id }}" {{ old('unit_id') == $unit->id ? 'selected': null }} >{{ $unit->name }}</option>
              @endforeach
            </select>
            @error('unit_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>
        </div><!-- col-4 -->
        <div class="col-lg-6">
          <div class="form-group row">
            <div class="col-6">
              <label class="form-control-label">Image: </label>
            <input class="" type="file" name="image" value="" placeholder="Enter Image"  accept="image/x-png,image/gif,image/jpeg"  onchange="priviewImg(event)">  
            @error('image')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            </div>
            <div class="col-6">
              <img id="preview-img" src="{{ asset('uploads/blank.png') }}" class="rounded" width="80px" />
            </div>
            
          </div>
        </div><!-- col-4 -->
        <div class="col-lg-6">
          <div class="form-group">
            <label class="form-control-label">Status: </label>
            <input class=""  type="checkbox" {{ old('status') == 1 ? 'checked' : null }} name="status" value="1">
            @error('status')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>
        </div><!-- col-4 -->
      </div>

      <div class="form-layout-footer">
        <button class="btn btn-secondary" type="reset">Reset</button>
        <button class="btn btn-outline-success" name="submit" value="s&c" type="submit">Save and Continue</button>
        <button class="btn btn-info" type="submit">Save</button>
      </div>
    </div>
    </form>
  </div>
</div>
@endsection

@push('title','Asset  Add')
@push('js')


@endpush
@push('css')

@endpush
@extends('admin.layouts.master')
@section('content')

<div class="br-pageheader">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
    <a class="breadcrumb-stafftype" href="{{ route('home') }}">{{ config('app.name', 'Laravel') }}</a>
    <a class="breadcrumb-stafftype text-capitalize" href="{{ route('admin.assets.index') }}">staff types</a>
    <span class="breadcrumb-stafftype active">List</span>
  </nav>
</div>

<div class="br-pagetitle">
  <i class="fa fa-industry" aria-hidden="true"></i>
  <div>
    <h4 class="text-capitalize">Assets</h4>
    <p class="mg-b-0"></p>
  </div>
</div>
<assets></assets>
@endsection

@push('title','Asset List')
@push('js')


@endpush

@push('css')

@endpush
@extends('admin.layouts.master')
@section('content')

<div class="br-pageheader">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
    <a class="breadcrumb-asset" href="{{ route('home') }}">{{ config('app.name', 'Laravel') }}</a>
    <a class="breadcrumb-asset text-capitalize" href="{{ route('admin.assets.index') }}">Assets</a>
    <span class="breadcrumb-asset active">View</span>
  </nav>
</div><!-- br-pageheader -->
<assets-view :data="{{ $asset }}"></assets-view>

@endsection

@push('title','Asset View')
@push('js')


@endpush
@push('css')

@endpush
@extends('admin.layouts.master')
@section('content')

<div class="br-pageheader">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
    <a class="breadcrumb-item" href="{{ route('home') }}">{{ config('app.name', 'Laravel') }}</a>
    <a class="breadcrumb-item text-capitalize" href="{{ route('admin.customers.index') }}">customers</a>
    <span class="breadcrumb-item active">View</span>
  </nav>
</div><!-- br-pageheader -->

<customer :data="{{ $customer }}" :reasons="{{ $reasons }}"></customer>

@endsection

@push('title','Customer View')
@push('js')


@endpush
@push('css')

@endpush
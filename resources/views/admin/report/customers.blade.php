@extends('admin.layouts.master')
@section('content')

<div class="br-pageheader">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
    <a class="breadcrumb-item" href="{{ route('home') }}">{{ config('app.name', 'Laravel') }}</a>
    <a class="breadcrumb-item text-capitalize" href="{{ route('admin.reports.index') }}">Report</a>
    <span class="breadcrumb-item active">List</span>
  </nav>
</div>



<report-customer></report-customer>
@endsection

@push('title','Items Reports List')
@push('js')


@endpush

@push('css')

@endpush
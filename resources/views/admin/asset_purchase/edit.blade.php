@extends('admin.layouts.master')
@section('content')

<div class="br-pageheader">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
    <a class="breadcrumb-item" href="{{ route('home') }}">{{ config('app.name', 'Laravel') }}</a>
    <a class="breadcrumb-item text-capitalize" href="{{ route('admin.asset-purchases.index') }}">purchases</a>
    <span class="breadcrumb-item active">Edit</span>
  </nav>
</div>
<div class="br-pagetitle">
  <i class="fa fa-industry" aria-hidden="true"></i>
  <div>
    <h4 class=" text-capitalize">Asset Purchases Edit</h4>
    <p class="mg-b-0"></p>
  </div>
</div><!-- d-flex -->

<asset-purchase-edit :data="{{ $purchase }}"></asset-purchase-edit>
@endsection

@push('title','stock Edit')
@push('js')


@endpush
@push('css')

@endpush
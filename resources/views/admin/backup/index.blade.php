@extends('admin.layouts.master')
@section('content')

<div class="br-pageheader">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
    <a class="breadcrumb-stafftype" href="{{ route('home') }}">{{ config('app.name', 'Laravel') }}</a>
    <a class="breadcrumb-stafftype text-capitalize" href="{{ route('admin.backups.index') }}">staff types</a>
    <span class="breadcrumb-stafftype active">List</span>
  </nav>
</div>

<div class="br-pagetitle">
  <i class="fa fa-industry" aria-hidden="true"></i>
  <div>
    <h4 class="text-capitalize">backup List</h4>
    <p class="mg-b-0"></p>
  </div>
</div>

<div class="br-pagebody">
  <div class="br-section-wrapper">
    <div class="row">
      <div class="col-6">
            
        <h6 class="br-section-label text-capitalize">backup List</h6>
        <p class="br-section-text"></p>
      </div>
      <div class="col-6">
        <a href="{{ route('admin.backups.create') }}" class="btn btn-primary float-right">Add New</a>
      </div>
    </div>

    <div class="bd bd-gray-300 rounded table-responsive">
      <table class="table mg-b-0">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Date</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          @isset($backups)
          @foreach($backups as $key => $row)
            @php
            $filename = storage_path('app/Laravel/'.$row[0]);
            if (file_exists($filename)) {

                $time = \Carbon\Carbon::parse(filemtime($filename));
                $size = File::size($filename);
                //$time = filemtime($filename);

                //$result = $time->gt($last);
                // if ($result) {
                //     $last = $time;
                //     $lastid = $key;
                // }
                //var_dump($result);
            }
            @endphp
          <tr>
            <th scope="row">{{ $key+1 }}</th>
            <th >{{ $row[0] }} <br>
              {{ number_format($size  / 1048576,2) }} Mb
            </th>
            <td>{{ $time->diffForHumans() }}</td>
            <td class="text-center">
              <a href="{{ route('admin.backups.download',$row[0]) }}"class="btn btn-outline-secondary btn-sm ml-2" >Download</a>
              <a href="javascript:viod()" class="btn btn-outline-danger btn-sm ml-2" onclick="archiveFunction('{{ $row[0] }}');">Delete</a>
              <form id="delete-form{{ $row[0] }}" action="{{ route('admin.backups.destroy',$row[0]) }}" method="POST">@csrf </form>
            </td>
          </tr>
          @endforeach
          @endif
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

@push('title','Backup List')
@push('js')


@endpush

@push('css')

@endpush
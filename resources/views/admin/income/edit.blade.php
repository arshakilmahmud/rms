@extends('admin.layouts.master')
@section('content')

<div class="br-pageheader">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
    <a class="breadcrumb-item" href="{{ route('home') }}">{{ config('app.name', 'Laravel') }}</a>
    <a class="breadcrumb-item" href="{{ route('admin.incomes.index') }}">Incomes</a>
    <span class="breadcrumb-item active">Edit</span>
  </nav>
</div><!-- br-pageheader -->
<div class="br-pagetitle">
  <i class="fa fa-industry" aria-hidden="true"></i>
  <div>
    <h4>Income Edit</h4>
    <p class="mg-b-0"></p>
  </div>
</div><!-- d-flex -->

<div class="br-pagebody">
  <div class="br-section-wrapper">
    <div class="row">
      <dib class="col-6">
            
        <h6 class="br-section-label">Income Edit</h6>
        <p class="br-section-text"></p>
      </dib>
      <dib class="col-6">
        <a href="{{ route('admin.incomes.index') }}" class="btn btn-primary float-right">Income List</a>
      </dib>
    </div>
    <form action="{{ route('admin.incomes.update',$income->id) }}" method="POST" enctype="multipart/form-data">
      @csrf
    <div class="form-layout form-layout-1">
      <div class="row mg-b-25">
        <div class="col-lg-12">
          <div class="form-group">
              <label class="form-control-label">Name: <span class="tx-danger">*</span></label>
            <input class="form-control" type="text" name="name" value="{{ $income->name }}" placeholder="Enter Name" required>  
            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            
          </div>
        </div><!-- col-4 -->
        <div class="col-lg-4">
          <div class="form-group">
              <label class="form-control-label">Categorie: <span class="tx-danger">*</span></label>
              <select name="income_category_id" id="income_category_id" class="form-control" required>
                <option value="">Select Categorie</option>
                @foreach($categories as $category)
                <option value="{{ $category->id }}" {{ $income->income_category_id== $category->id ? 'selected': null }}>{{ $category->name }}</option>
                @endforeach
              </select>
            @error('income_category_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            
          </div>
        </div><!-- col-4 -->
        <div class="col-lg-3">
          @php
          $date = Carbon\Carbon::parse($income->date)->format('Y-m-d') 
          @endphp
          <div class="form-group ">
              <label class="form-control-label">Date: <span class="tx-danger">*</span></label>
            <input class="form-control" type="date" name="date" value="{{ $date }}" placeholder="Enter Date"  required>  
            @error('date')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            
          </div>
        </div><!-- col-4 -->
        <div class="col-lg-3">
          <div class="form-group ">
              <label class="form-control-label">Amount: <span class="tx-danger">*</span></label>
            <input class="form-control" type="number" name="amount" value="{{ $income->amount }}" placeholder="Enter Amount" step="any" required>  
            @error('amount')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            
          </div>
        </div><!-- col-4 -->
        <div class="col-lg-12">
          <div class="form-group ">
              <label class="form-control-label">Description: </label>
            <textarea name="description" id="description" class="form-control" cols="30" rows="4">{!! $income->description !!}</textarea>
            @error('description')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            
          </div>
        </div><!-- col-4 -->
        <div class="col-lg-12">
          <div class="form-group row">
            <div class="col-6">
              <label class="form-control-label">Image: <span class="tx-danger">*</span></label>
            <input class="" type="file" name="image" value="" placeholder="Enter Image"  accept="image/x-png,image/gif,image/jpeg" >  
            @error('image')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            </div>
            @if($category->image!=null)
            <div class="col-6">
              <img src="{{ asset($income->image) }}" alt="" width="80px" class="rounded">
            </div>
            @endif
            
          </div>
        </div><!-- col-4 -->
        {{-- <div class="col-lg-6">
          <div class="form-group">
            <label class="form-control-label">Status: </label>
            <input class=""  type="checkbox" name="status" @if($income->status==1) checked @endif value="1">
            @error('status')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>
        </div><!-- col-4 --> --}}
      </div><!-- row -->

      <div class="form-layout-footer">
        <button class="btn btn-secondary" type="reset">Reset</button>
        <button class="btn btn-outline-success" name="submit" value="s&c" type="submit">Save and Continue</button>
        <button class="btn btn-info" type="submit">Save</button>
      </div><!-- form-layout-footer -->
    </div><!-- form-layout -->
    </form>
  </div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->
@endsection

@push('title','Income Edit')
@push('js')


@endpush
@push('css')

@endpush
<div class="br-logo"><a href="{{ route('index') }}">
<img src="{{ asset($web_info->logo) }}" width="80%"  alt="">
</a></div>
<div class="br-sideleft sideleft-scrollbar">
  <label class="sidebar-label pd-x-10 mg-t-20 op-3">Navigation</label>
  <ul class="br-sideleft-menu">
    <li class="br-menu-item">
      <a href="{{ route('admin.home') }}" class="br-menu-link {{ (request()->is('admin/dashbord')) ? 'active' : '' }}">
        <i class="menu-item-icon icon ion-ios-home-outline tx-24"></i>
        <span class="menu-item-label">Dashboard</span>
      </a>
    </li>

    <li class="br-menu-item">
      <a href="{{ route('admin.pos.index') }}" class="br-menu-link {{ (request()->is('admin/pos*')) ? 'active' : '' }}">
        <i class="fa fa-print" aria-hidden="true"></i>
        <span class="menu-item-label">Pos</span>
      </a>
    </li>

    <li class="br-menu-item">
      <a href="{{ route('admin.orders.index') }}" class="br-menu-link {{ (request()->is('admin/orders*')) ? 'active' : '' }}">
        <i class="fas fa-box"></i>
        <span class="menu-item-label">Orders</span>
      </a>
    </li>
    
    <li class="br-menu-item">
      <a href="{{ route('admin.purchases.index') }}" class="br-menu-link {{ (request()->is('admin/purchases*')) ? 'active' : '' }}">
        <i class="fa fa-archive" aria-hidden="true"></i>
        <span class="menu-item-label">Purchase</span>
      </a>
    </li>
    
    <li class="br-menu-item">
      <a href="{{ route('admin.stocks.index') }}" class="br-menu-link {{ (request()->is('admin/stocks*')) ? 'active' : '' }}">
        <i class="fa fa-archive" aria-hidden="true"></i>
        <span class="menu-item-label">Stock</span>
      </a>
    </li>

    <li class="br-menu-item">
      <a href="#" class="br-menu-link with-sub 
      {{ (request()->is('admin/reports*')) ? 'active show-sub' : null }}
      ">
        <i class="menu-item-icon icon ion-ios-color-filter-outline tx-24"></i>
        <span class="menu-item-label">Reports</span>
      </a><!-- br-menu-link -->
      <ul class="br-menu-sub">
        <li class="sub-item">
          <a href="{{ route('admin.reports.index') }}" class="sub-link {{ (request()->is('admin/reports')) ? 'active' : '' }}">Cash Ledger</a>
        </li>
        <li class="sub-item">
          <a href="{{ route('admin.reports.credits') }}" class="sub-link {{ (request()->is('admin/reports/credits')) ? 'active' : '' }}">Credits</a>
        </li>
        <li class="sub-item">
          <a href="{{ route('admin.reports.products') }}" class="sub-link {{ (request()->is('admin/reports/products')) ? 'active' : '' }}">Products</a>
        </li>
        <li class="sub-item">
          <a href="{{ route('admin.reports.customers') }}" class="sub-link {{ (request()->is('admin/reports/customers')) ? 'active' : '' }}">Customers</a>
        </li>
        <li class="sub-item">
          <a href="{{ route('admin.reports.orders') }}" class="sub-link {{ (request()->is('admin/reports/orders')) ? 'active' : '' }}">Orders</a>
        </li>
      </ul>
    </li>

    <li class="br-menu-item">
      <a href="#" class="br-menu-link with-sub 
      {{ (request()->is('admin/payrolls*')) ? 'active show-sub' : null }}
      {{ (request()->is('admin/expences*')) ? 'active show-sub' : null }}
      {{ (request()->is('admin/incomes*')) ? 'active show-sub' : null }}
      ">
        <i class="menu-item-icon icon ion-ios-color-filter-outline tx-24"></i>
        <span class="menu-item-label">Accounts</span>
      </a><!-- br-menu-link -->
      <ul class="br-menu-sub">        
        <li class="sub-item">
          <a href="{{ route('admin.incomes.index') }}" class="sub-link {{ (request()->is('admin/incomes*')) ? 'active' : '' }}">Income</a>
        <li class="sub-item">
          <a href="{{ route('admin.expences.index') }}" class="sub-link {{ (request()->is('admin/expences*')) ? 'active' : '' }}">Expence</a>
        </li>
        <li class="sub-item">
          <a href="{{ route('admin.payrolls.index') }}" class="sub-link {{ (request()->is('admin/payrolls*')) ? 'active' : '' }}">Payrolls</a>
        </li>
      </ul>
    </li>
    
    <li class="br-menu-item">
      <a href="#" class="br-menu-link with-sub {{ (request()->is('admin/categories*')) ? 'active show-sub"' : null }}{{ (request()->is('admin/products*')) ? 'active show-sub' : null }}">
        <i class="menu-item-icon icon ion-ios-color-filter-outline tx-24"></i>
        <span class="menu-item-label">Menu Items Catalogue</span>
      </a><!-- br-menu-link -->
      <ul class="br-menu-sub">
        <li class="sub-item"><a href="{{ route('admin.categories.index') }}" class="sub-link {{ (request()->is('admin/categories*')) ? 'active' : '' }}">Categories</a></li>
        <li class="sub-item"><a href="{{ route('admin.products.index') }}" class="sub-link {{ (request()->is('admin/products*')) ? 'active' : '' }}">Menu Items</a></li>
      </ul>
    </li>
    
    <li class="br-menu-item">
      <a href="#" class="br-menu-link with-sub 
      {{ (request()->is('admin/types*')) ? 'active show-sub' : null }}
      {{ (request()->is('admin/items*')) ? 'active show-sub' : null }}">
        <i class="menu-item-icon icon ion-ios-color-filter-outline tx-24"></i>
        <span class="menu-item-label">Purchases Items Catalogue</span>
      </a><!-- br-menu-link -->
      <ul class="br-menu-sub">
        <li class="sub-item"><a href="{{ route('admin.types.index') }}" class="sub-link {{ (request()->is('admin/types*')) ? 'active' : '' }}">Items Types</a></li>
        <li class="sub-item"><a href="{{ route('admin.items.index') }}" class="sub-link {{ (request()->is('admin/items*')) ? 'active' : '' }}">Items</a></li>
      </ul>
    </li>

    <li class="br-menu-item">
      <a href="#" class="br-menu-link with-sub 
      {{ (request()->is('admin/units*')) ? 'active show-sub' : null }}
      {{ (request()->is('admin/expence-categories*')) ? 'active show-sub' : null }}
      {{ (request()->is('admin/income-categories*')) ? 'active show-sub' : null }}
      ">
        <i class="menu-item-icon icon ion-ios-color-filter-outline tx-24"></i>
        <span class="menu-item-label">Catalogue</span>
      </a><!-- br-menu-link -->
      <ul class="br-menu-sub">
        <li class="sub-item"><a href="{{ route('admin.units.index') }}" class="sub-link {{ (request()->is('admin/units*')) ? 'active' : '' }}">Units</a></li>
        <li class="sub-item"><a href="{{ route('admin.tables.index') }}" class="sub-link {{ (request()->is('admin/tables*')) ? 'active' : '' }}">Tables</a></li>
        <li class="sub-item">
          <a href="{{ route('admin.expence-categories.index') }}" class="sub-link {{ (request()->is('admin/expence-categories*')) ? 'active' : '' }}">Expence Categories</a>
        </li>
        <li class="sub-item">
          <a href="{{ route('admin.income-categories.index') }}" class="sub-link {{ (request()->is('admin/income-categories*')) ? 'active' : '' }}">Income Categories</a>
        </li>
      </ul>
    </li>

    <li class="br-menu-item">
      <a href="#" class="br-menu-link with-sub 
      {{ (request()->is('admin/stafftypes*')) ? 'active show-sub' : null }}
      {{ (request()->is('admin/staff*')) ? 'active show-sub' : null }}
      {{ (request()->is('admin/attendances*')) ? 'active show-sub' : null }}
      {{ (request()->is('admin/leaves*')) ? 'active show-sub' : null }}
      {{ (request()->is('admin/holidays*')) ? 'active show-sub' : null }}">
        <i class="menu-item-icon icon ion-ios-color-filter-outline tx-24"></i>
        <span class="menu-item-label">Staff</span>
      </a><!-- br-menu-link -->
      <ul class="br-menu-sub">
        <li class="sub-item">
          <a href="{{ route('admin.attendances.index') }}" class="sub-link {{ (request()->is('admin/attendances*')) ? 'active' : '' }}">Attendances</a>
        </li>
        <li class="sub-item">
          <a href="{{ route('admin.leaves.index') }}" class="sub-link {{ (request()->is('admin/leaves*')) ? 'active' : '' }}">Leaves</a>
        </li>
        <li class="sub-item">
          <a href="{{ route('admin.holidays.index') }}" class="sub-link {{ (request()->is('admin/holidays*')) ? 'active' : '' }}">Holidays</a>
        </li>
        <li class="sub-item"><a href="{{ route('admin.staff-types.index') }}" class="sub-link {{ (request()->is('admin/stafftypes*')) ? 'active' : '' }}">Staff Types</a></li>
        <li class="sub-item"><a href="{{ route('admin.staff.index') }}" class="sub-link {{ (request()->is('admin/admins*')) ? 'active' : '' }}">Staff Menage</a></li>
      </ul>
    </li>

    <li class="br-menu-item">
      <a href="{{ route('admin.customers.index') }}" class="br-menu-link {{ (request()->is('admin/customers*')) ? 'active' : '' }}">
        <i class="fa fa-users" aria-hidden="true"></i>
        <span class="menu-item-label">Customers</span>
      </a>
    </li>

    <li class="br-menu-item">
      <a href="{{ route('admin.suppliers.index') }}" class="br-menu-link {{ (request()->is('admin/suppliers*')) ? 'active' : '' }}">
        <i class="fa fa-users" aria-hidden="true"></i>
        <span class="menu-item-label">Suppliers</span>
      </a>
    </li>

    <li class="br-menu-item">
      <a href="#" class="br-menu-link with-sub {{ (request()->is('admin/asset-categories*')) ? 'active show-sub' : null }}{{ (request()->is('admin/assets*')) ? 'active show-sub' : null }}{{ (request()->is('admin/asset-purchases*')) ? 'active show-sub' : null }}">
        <i class="fa fa-cube"></i>
        <span class="menu-item-label">Asset</span>
      </a><!-- br-menu-link -->
      <ul class="br-menu-sub">
        <li class="sub-item"><a href="{{ route('admin.asset-categories.index') }}" class="sub-link {{ (request()->is('admin/asset-categories*')) ? 'active' : '' }}">Asset Categories</a></li>
        <li class="sub-item"><a href="{{ route('admin.assets.index') }}" class="sub-link {{ (request()->is('admin/assets*')) ? 'active' : '' }}">Asset Menage</a></li>
       {{--  <li class="sub-item"><a href="{{ route('admin.asset-purchases.index') }}" class="sub-link {{ (request()->is('admin/asset-purchases*')) ? 'active' : '' }}">Asset Purchases</a></li> --}}
      </ul>
    </li>
    
    <li class="br-menu-item">
      <a href="{{ route('admin.admins.index') }}" class="br-menu-link {{ (request()->is('admin/admins*')) ? 'active' : '' }}">
        <i class="fa fa-gavel" aria-hidden="true"></i>
        <span class="menu-item-label">Admins</span>
      </a>
    </li>
    
    <li class="br-menu-item">
      <a href="{{ route('admin.roles.index') }}" class="br-menu-link {{ (request()->is('admin/roles*')) ? 'active' : '' }}">
        <i class="fa fa-unlock-alt" aria-hidden="true"></i>
        <span class="menu-item-label">Role</span>
      </a>
    </li>

    <li class="br-menu-item">
      <a href="{{ route('admin.web.index') }}" class="br-menu-link {{ (request()->is('admin/web*')) ? 'active' : '' }}">
        <i class="fa fa-globe" aria-hidden="true"></i>
        <span class="menu-item-label">Web Info</span>
      </a>
    </li>

    {{-- <li class="br-menu-item">
      <a href="{{ route('admin.backups.index') }}" class="br-menu-link {{ (request()->is('admin/backups*')) ? 'active' : '' }}">
        <i class="fa fa-globe" aria-hidden="true"></i>
        <span class="menu-item-label">Backups</span>
      </a>
    </li> --}}
  </ul>
  <br>
</div>
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;
Vue.prototype.$role = document.querySelector("meta[name='role']").getAttribute('content');
Vue.prototype.$eventBus = new Vue(); 

Vue.use(require('vue-resource'));
//select
import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css';
Vue.component('v-select', vSelect)
//end select

import 'sweetalert2/dist/sweetalert2.min.css';
import VueSweetalert2 from 'vue-sweetalert2';
Vue.use(VueSweetalert2);

import Toaster from 'v-toaster'
import 'v-toaster/dist/v-toaster.css'
Vue.use(Toaster, {timeout: 5000});

Vue.use(require('vue-moment'));

Vue.component('pagination', require('laravel-vue-pagination').default);

import { Datetime } from 'vue-datetime';
import 'vue-datetime/dist/vue-datetime.css'
Vue.component('datetime', Datetime);

import CKEditor from 'ckeditor4-vue';

Vue.use( CKEditor );


Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('cart-list', require('./components/admin/pos/CartList.vue').default);
Vue.component('order-edit', require('./components/admin/pos/OrderEdit.vue').default);
Vue.component('pos-product-list', require('./components/admin/pos/PosProductList.vue').default);

Vue.component('orders-list', require('./components/admin/order/Order.vue').default);
Vue.component('orders-view', require('./components/admin/order/OrderView.vue').default);
Vue.component('products-list', require('./components/admin/product/ProductList.vue').default);

// Vue.component('stock-view', require('./components/admin/order/OrderView.vue').default);
// Vue.component('stock-list', require('./components/admin/product/ProductList.vue').default);
Vue.component('stock-in', require('./components/admin/purchase/StockIn.vue').default);
Vue.component('stock-edit', require('./components/admin/purchase/StockEdit.vue').default);
Vue.component('purchase-list', require('./components/admin/purchase/Purchase.vue').default);


Vue.component('stocks', require('./components/admin/stocks/Stocks.vue').default);
Vue.component('stocks-view', require('./components/admin/stocks/StockView.vue').default);

Vue.component('assets', require('./components/admin/assets/Assets.vue').default);
Vue.component('assets-view', require('./components/admin/assets/assetView.vue').default);
//Asset Purchases
// Vue.component('asset-purchase-new', require('./components/admin/asset-purchase/PurchaseNew.vue').default);
// Vue.component('asset-purchase-edit', require('./components/admin/asset-purchase/PurchaseEdit.vue').default);
// Vue.component('asset-purchase-list', require('./components/admin/asset-purchase/PurchaseList.vue').default);

//role component
Vue.component('admin-role-list', require('./components/admin/role/RoleList.vue').default);
Vue.component('customer-list', require('./components/admin/customer/CustomerList.vue').default);
Vue.component('customer', require('./components/admin/customer/Customer.vue').default);
Vue.component('table-list', require('./components/admin/table/TableList.vue').default);
//Vue.component('asset-category-list', require('./components/admin/table/AssetCategoryList.vue').default);

//attendance component
Vue.component('attendance', require('./components/admin/attendance/AttendanceList.vue').default);
//leave Component
Vue.component('leave-list', require('./components/admin/leave/LeaveList.vue').default);
Vue.component('leave-edit', require('./components/admin/leave/EditLeave.vue').default);


//payroll component
Vue.component('payroll-list', require('./components/admin/payroll/PayrollList.vue').default);
//Report
Vue.component('report', require('./components/admin/report/Report.vue').default);
Vue.component('report-credit', require('./components/admin/report-credit/Credit.vue').default);
Vue.component('report-order', require('./components/admin/report-order/Order.vue').default);
Vue.component('report-product', require('./components/admin/report-product/Product.vue').default);
Vue.component('report-customer', require('./components/admin/report-customer/Customer.vue').default);

Vue.component('income', require('./components/admin/income/Income.vue').default);
Vue.component('expence', require('./components/admin/expence/expence.vue').default);
//Vue.component('payroll-edit', require('./components/admin/payroll/PayrollEdit.vue').default);
//holiday Component
Vue.component('holiday', require('./components/admin/holiday/Holiday.vue').default);
Vue.component('holiday-edit', require('./components/admin/holiday/EditHoliday.vue').default);

//admin component
Vue.component('admin-admin-list', require('./components/admin/admin/AdminList.vue').default);
Vue.component('admin-admin-add', require('./components/admin/admin/AdminAdd.vue').default);
Vue.component('admin-admin-edit', require('./components/admin/admin/AdminEdit.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const app = new Vue({
    el: '#app',
});

const MyPlugin = {
  install(Vue, options) {
    Vue.prototype.ifErrors = (error) => {
        let errors = error.response.data.errors;
        //console.log(errors)
        app.$swal({
          title: 'warning!',
          text: error.response.data.message,
          icon: 'warning',
        });
        app.$toaster.error(error.response.data.message)
        for (const [key, value] of Object.entries(errors)) {
          app.$toaster.warning(value[0])
        }
    }
  },
}
Vue.use(MyPlugin)
<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Income;
use App\Models\Expence;
use App\Models\Ledger;
use App\Models\Payroll;
use App\Models\Purchase;
use App\Models\Stock;
use Carbon\Carbon;

class ClearData extends Seeder
{ 

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data  = Purchase::with('items')->get();
        foreach ($data as $key => $row) {
            $row->date = Carbon::parse($row->date)->startOfDay()->format('Y-m-d h:i:s');
            $row->save();
            foreach ($row->items as $key => $item) {
                $item->date = Carbon::parse($row->date)->startOfDay()->format('Y-m-d h:i:s');
                $item->save();
            }
        }
        $data  = Stock::with('perches_item')->get();
        foreach ($data as $key => $row) {
            $row->date = Carbon::parse($row->perches_item->date)->startOfDay()->format('Y-m-d h:i:s');
            $row->save();
        }
        /*$data  = Ledger::where('ledgerable_type','App\Models\Expence')->with('ledgerable')->get();
        foreach ($data as $key => $row) {
            $row->amount =$row->ledgerable->amount;
            $row->credit =1;
            $row->debit =0;
            $row->status =1;
            $row->save();
        }*/
         //date
        // $data  = Order::all();
        // foreach ($data as $key => $row) {
        //     if($row->payment_type == 'cradit'){
        //         $row->payment_type =  'credit';
        //     }
        //     $row->status =  1;
        //     $row->save();
        // }
        // $data  = Income::all();
        // foreach ($data as $key => $row) {
            
        //     $row->status =  1;
        //     $row->save();
        // }
           
        // $data  = Expence::all();
        // foreach ($data as $key => $row) {
        //      $row->status =  1;
        //     $row->save();
        // }
        // $data  = Ledger::all();
        // foreach ($data as $key => $row) {
        //     if($row->ledgerable_type !='App\Models\Purchase'){
        //         $row->status =  1;
        //         $row->save();
        //     }
        // }
        // $data  = Payroll::all();
        // foreach ($data as $key => $row) {
        //      $row->status =  1;
        //     $row->save();
        // }
        // DB::table('purchases')->delete();
        // DB::table('purchase_items')->delete();
        // DB::table('stocks')->delete();


        // DB::table('ledgers')->delete();

        // DB::table('incomes')->delete();

        // DB::table('expences')->delete();

        // DB::table('holidays')->delete();

        // DB::table('payrolls')->delete();

        // DB::table('orders')->delete();

        // DB::table('order_items')->delete();

        // DB::table('leaves')->delete();
        
        // DB::table('attendances')->delete();
    }
}

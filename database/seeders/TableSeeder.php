<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Table;
use DB;
class TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tables')->delete();
        for ($i=1; $i < 20 ; $i++) { 
            Table::create(['name'=>'Table '.$i,'status'=>1]);
        }
    }
}

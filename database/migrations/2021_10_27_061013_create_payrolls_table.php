<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayrollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payrolls', function (Blueprint $table) {
            $table->id();
            $table->integer('staff_id');
            $table->decimal('allawence', 15, 2)->nullable();
            $table->decimal('bonus', 15, 2)->nullable();
            $table->decimal('advance', 15, 2)->nullable();
            $table->decimal('advance_deduction', 15, 2)->nullable();
            $table->decimal('absent_deduction', 15, 2)->nullable();
            $table->decimal('other_deduction', 15, 2)->nullable();
            $table->decimal('basic_salary', 15, 2)->nullable();
            $table->decimal('total_paid', 15, 2)->nullable();
            $table->string('date');
            $table->text('description')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payrolls');
    }
}

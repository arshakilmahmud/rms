<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('stocks');
        Schema::create('stocks', function (Blueprint $table) {
            $table->id();
            $table->integer('perches_item_id')->nullable();
            $table->integer('item_id');
            $table->tinyInteger('in')->nullable();
            $table->tinyInteger('weast')->nullable();
            $table->tinyInteger('out')->nullable();
            $table->string('quantity')->nullable();
            $table->string('date')->nullable();
            $table->string('reason')->nullable();
            $table->integer('user_id')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
    }
}

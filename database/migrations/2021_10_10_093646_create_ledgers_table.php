<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLedgersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ledgers', function (Blueprint $table) {
            $table->id();
            $table->morphs('ledgerable');
            $table->date('date');
            $table->text('reason');
            $table->boolean('credit')->default(0);
            $table->boolean('debit')->default(0);
            $table->float('amount', 8, 2);
            $table->float('current_balance', 8, 2)->nullable();
            $table->integer('approved_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ledgers');
    }
}

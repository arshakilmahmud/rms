<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddcustomerIdTOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {            
            $table->dropColumn('payment_status');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->foreign('asset_id')->references('id')->on('customers')->onDelete('cascade')->onUpdate('cascade');
            $table->string('payment_type')->nullable()->after('grand_total');
            $table->string('payment_status')->nullable()->after('grand_total');
            $table->string('date')->nullable()->after('grand_total');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('customer_id');
            $table->dropColumn('payment_type');
            $table->dropColumn('date');
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetPurchaseItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_purchase_items', function (Blueprint $table) {
            $table->id();
            $table->integer('asset_purchase_id');
            $table->string('type')->nullable();
            $table->integer('item_id')->nullable();
            $table->float('price',20,2)->nullable();
            $table->float('quantity',20,3)->nullable();
            $table->float('total',20,2)->nullable();
            $table->float('previous_stock',20,3)->nullable();
            $table->float('current_stock',20,3)->nullable();
            $table->integer('user_id')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_purchase_items');
    }
}

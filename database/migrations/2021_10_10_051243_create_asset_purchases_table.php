<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_purchases', function (Blueprint $table) {
            $table->id();
            $table->string('invoice_id');
            $table->integer('supplier_id')->nullable();
            $table->float('sub_total',20,3)->nullable();
            $table->float('total',20,3)->nullable();
            $table->float('discount',20,3)->nullable();
            $table->float('paid',20,3)->nullable();
            $table->float('due',20,3)->nullable();
            $table->timestamp('date')->nullable();
            $table->integer('user_id')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_purchases');
    }
}

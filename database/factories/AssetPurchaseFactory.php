<?php

namespace Database\Factories;

use App\Models\AssetPurchase;
use Illuminate\Database\Eloquent\Factories\Factory;

class AssetPurchaseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AssetPurchase::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}

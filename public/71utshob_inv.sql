-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 26, 2021 at 11:38 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `71utshob_inv`
--

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE `assets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asset_category_id` int(11) DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assets`
--

INSERT INTO `assets` (`id`, `name`, `asset_category_id`, `slug`, `image`, `description`, `user_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, 'spoon', 14, 'spoon', NULL, NULL, 1, 1, '2021-10-10 01:16:10', '2021-10-10 01:16:10', NULL),
(5, 'pork', 14, 'pork', NULL, NULL, 1, 1, '2021-10-10 01:16:37', '2021-10-10 01:16:37', NULL),
(6, 'plate', 10, 'plate', NULL, NULL, 1, 1, '2021-10-10 01:17:09', '2021-10-10 01:17:09', NULL),
(7, 'cup', 10, 'cup', NULL, NULL, 1, 1, '2021-10-10 01:17:26', '2021-10-10 01:17:26', NULL),
(8, 'dish', 10, 'dish', NULL, NULL, 1, 1, '2021-10-10 01:18:03', '2021-10-10 01:18:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `asset_categories`
--

CREATE TABLE `asset_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `asset_categories`
--

INSERT INTO `asset_categories` (`id`, `name`, `slug`, `image`, `description`, `user_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(6, 'Furniture', 'furniture', NULL, NULL, 1, 1, '2021-10-10 00:24:31', '2021-10-10 00:24:31', NULL),
(7, 'Gadget', 'gadget', NULL, NULL, 1, 1, '2021-10-10 00:24:56', '2021-10-10 00:24:56', NULL),
(8, 'Electronics', 'electronics', NULL, NULL, 1, 1, '2021-10-10 00:25:37', '2021-10-10 00:25:37', NULL),
(9, 'Kitchen Accessories', 'kitchen-accessories', NULL, NULL, 1, 1, '2021-10-10 00:27:48', '2021-10-10 00:27:48', NULL),
(10, 'Crockeries', 'crocaries', NULL, NULL, 1, 1, '2021-10-10 00:29:28', '2021-10-10 00:58:37', NULL),
(11, 'Computer equipment and software', 'computer-equipment-and-software', NULL, NULL, 1, 1, '2021-10-10 00:34:28', '2021-10-10 00:34:28', NULL),
(12, 'Leasehold improvements', 'leasehold-improvements', NULL, NULL, 1, 1, '2021-10-10 00:34:50', '2021-10-10 00:34:50', NULL),
(13, 'Intangible assets', 'intangible-assets', NULL, NULL, 1, 1, '2021-10-10 00:34:58', '2021-10-10 00:34:58', NULL),
(14, 'Cutlery', 'cutlery', NULL, NULL, 1, 1, '2021-10-10 00:56:56', '2021-10-10 00:57:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `asset_purchases`
--

CREATE TABLE `asset_purchases` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `sub_total` double(20,3) DEFAULT NULL,
  `total` double(20,3) DEFAULT NULL,
  `discount` double(20,3) DEFAULT NULL,
  `paid` double(20,3) DEFAULT NULL,
  `due` double(20,3) DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `asset_purchase_items`
--

CREATE TABLE `asset_purchase_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `asset_purchase_id` int(11) NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `price` double(20,2) DEFAULT NULL,
  `quantity` double(20,3) DEFAULT NULL,
  `total` double(20,2) DEFAULT NULL,
  `previous_stock` double(20,3) DEFAULT NULL,
  `current_stock` double(20,3) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attendances`
--

CREATE TABLE `attendances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `staff_id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `approve_remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valid` tinyint(4) NOT NULL DEFAULT 1,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `show_individual` tinyint(1) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `icon`, `image`, `text_1`, `text_2`, `status`, `show_individual`, `parent_id`, `created_at`, `updated_at`) VALUES
(41, 'Salad', 'salad', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2021-06-20 18:30:24', '2021-06-20 18:30:24'),
(42, 'Noodles', 'noodlespasta', NULL, NULL, 'Noodles', NULL, 1, NULL, NULL, '2021-06-20 18:30:25', '2021-10-31 18:34:41'),
(43, 'Soup', 'soup', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2021-06-20 18:30:28', '2021-06-20 18:30:28'),
(44, 'Rice', 'rice', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2021-06-20 18:30:32', '2021-06-20 18:30:32'),
(45, 'Appetizers', 'appetizers', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2021-06-20 18:30:34', '2021-06-20 18:30:34'),
(47, 'Beef', 'beef', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2021-06-20 18:30:44', '2021-06-20 18:30:44'),
(48, 'Fish/Prawn', 'fishprawn', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2021-06-20 18:30:45', '2021-06-20 18:30:45'),
(49, 'Mutton', 'mutton', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2021-06-20 18:30:52', '2021-06-20 18:30:52'),
(50, 'Chicken', 'chicken', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2021-06-20 18:30:52', '2021-06-20 18:30:52'),
(51, 'Fast Food', 'first-food', NULL, NULL, 'Fast Food', NULL, 1, NULL, NULL, '2021-06-20 18:31:05', '2021-10-30 19:05:52'),
(52, 'Vegetable', 'vegetable', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2021-06-20 18:31:10', '2021-06-20 18:31:10'),
(54, 'Beef Burger', 'burger', NULL, NULL, 'Beef Burger', NULL, 1, NULL, NULL, '2021-10-29 22:11:54', '2021-10-30 19:06:32'),
(56, 'Coffee', 'coffee', NULL, NULL, 'Coffee', NULL, 1, NULL, NULL, '2021-10-29 22:25:46', '2021-10-29 22:25:46'),
(57, 'Juice', 'juice', NULL, NULL, 'Juice', NULL, 1, NULL, NULL, '2021-10-29 22:26:05', '2021-10-29 22:26:05'),
(58, 'Lassi', 'lassi', NULL, NULL, 'Lassi', NULL, 1, NULL, NULL, '2021-10-29 22:26:17', '2021-10-29 22:26:17'),
(59, 'Chicken Burger', 'chicken-burger', NULL, NULL, 'Chicken Burger', NULL, 1, NULL, NULL, '2021-10-29 22:58:53', '2021-10-29 22:58:53'),
(60, 'Pizza', 'pizza', NULL, NULL, 'Pizza', NULL, 1, NULL, NULL, '2021-10-29 23:10:34', '2021-10-29 23:10:34'),
(61, 'Chicken/Wings', 'chickenwings', NULL, NULL, 'Chicken/Wings', NULL, 1, NULL, NULL, '2021-10-30 17:30:55', '2021-10-30 17:30:55'),
(62, 'Pasta', 'pasta', NULL, NULL, 'Pasta', NULL, 1, NULL, NULL, '2021-10-30 17:53:49', '2021-10-30 17:53:49'),
(63, 'Sandwich', 'sandwich', NULL, NULL, 'Sandwich', NULL, 1, NULL, NULL, '2021-10-30 17:54:05', '2021-10-30 17:54:05'),
(64, 'Beverages', 'beverages', NULL, NULL, 'Beverages', NULL, 1, NULL, NULL, '2021-10-31 18:49:27', '2021-10-31 18:49:27'),
(65, 'Chinese Food', 'chinese-food', NULL, NULL, 'Chinese Food', NULL, 1, NULL, NULL, '2021-10-31 19:28:53', '2021-10-31 19:29:07'),
(66, 'Set Menu', 'set-menu', NULL, NULL, 'Set Menu', NULL, 1, NULL, NULL, '2021-11-07 20:34:38', '2021-11-07 20:34:38'),
(67, 'Residential', 'residential', NULL, NULL, 'Residential', NULL, 1, NULL, NULL, '2021-11-09 23:36:59', '2021-11-09 23:36:59');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `occupation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `image`, `email`, `phone`, `occupation`, `address`, `user_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Akram Hossain', NULL, 'jhfuyikg@gmail.com', '01851774229', NULL, 'Shibchar, Madaripur.', 27, 1, '2021-11-07 02:39:16', '2021-11-12 03:23:41', NULL),
(2, 'Habib Sheikh', NULL, 'habibsheikh@gmail.com', '01839776647', NULL, 'Shibchar, Madaripur', 27, 1, '2021-11-07 02:48:41', '2021-11-07 02:48:41', NULL),
(3, 'Nadim Uddin', NULL, 'nadimuddin@gmail.com', '01994094525', NULL, 'Shibchar, Madaripur', 27, 1, '2021-11-07 02:51:25', '2021-11-07 19:11:04', NULL),
(4, 'Rakibul Hasan', NULL, 'rakibulhasan@gmail.com', '01575083036', NULL, 'Shibchar, Madaripur', 27, 1, '2021-11-07 03:02:30', '2021-11-07 03:02:30', NULL),
(5, 'Aman Ullah', NULL, 'amanullah@gmail.com', '01965432762', NULL, 'Shibchar, Madaripur', 27, 1, '2021-11-07 03:04:20', '2021-11-07 19:10:34', NULL),
(6, 'Abdul jabbar', NULL, 'abduljabbar@gmail.com', '01986578898', NULL, 'Paschar, Madaripur', 27, 1, '2021-11-12 03:26:38', '2021-11-12 03:26:38', NULL),
(7, 'Md. Rakib', NULL, 'rakib@gmail.com', '01977338489', NULL, '71 Road, Shibchar, Madaripur', 27, 1, '2021-11-12 03:28:48', '2021-11-12 03:28:48', NULL),
(8, 'Mamun Sheikh', NULL, 'mamunsheikh@gmail.com', '01678976589', NULL, 'Shibchar, Madaripur', 27, 1, '2021-11-12 03:31:13', '2021-11-12 03:31:13', NULL),
(9, 'Halim Khan', NULL, 'halimkhan@gmail.com', '01883748798', NULL, 'Paschar, Shibchar', 27, 1, '2021-11-12 03:32:54', '2021-11-12 03:32:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `expences`
--

CREATE TABLE `expences` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `expence_category_id` int(11) NOT NULL,
  `amount` double(20,2) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `expence_categories`
--

CREATE TABLE `expence_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `expence_categories`
--

INSERT INTO `expence_categories` (`id`, `name`, `slug`, `user_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Abdul Duncan', 'abdul-duncan', 1, 1, '2021-12-05 04:23:26', '2021-12-05 04:23:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `end_date` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `incomes`
--

CREATE TABLE `incomes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `income_category_id` int(11) NOT NULL,
  `amount` double(20,2) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `income_categories`
--

CREATE TABLE `income_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `income_categories`
--

INSERT INTO `income_categories` (`id`, `name`, `slug`, `user_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Generator Theke Aye', 'generator-theke-aye', 1, 1, '2021-12-21 03:44:46', '2021-12-21 03:44:46', NULL),
(2, 'xyz', 'xyz', 1, 1, '2021-12-22 06:29:21', '2021-12-22 06:29:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_id` bigint(20) UNSIGNED DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_id` bigint(20) UNSIGNED DEFAULT NULL,
  `purchase_price` double(20,2) NOT NULL DEFAULT 0.00,
  `selling_price` double(20,2) NOT NULL DEFAULT 0.00,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `type_id`, `slug`, `sku`, `image`, `description`, `unit_id`, `purchase_price`, `selling_price`, `user_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'পেপে', 3, 'pepe', NULL, NULL, NULL, 3, 20.00, 0.00, 27, 1, '2021-11-02 18:38:55', '2021-11-02 18:38:55', NULL),
(2, 'চিচিংগা', 3, 'cicingga', NULL, NULL, NULL, 3, 48.00, 0.00, 27, 1, '2021-11-02 18:45:45', '2021-11-02 18:45:45', NULL),
(3, 'জালি', 3, 'jali', NULL, NULL, NULL, 8, 35.00, 0.00, 27, 1, '2021-11-02 18:48:43', '2021-11-02 18:48:43', NULL),
(4, 'গাজর', 3, 'gajr', NULL, NULL, NULL, 3, 140.00, 0.00, 27, 1, '2021-11-02 18:50:17', '2021-11-02 18:50:17', NULL),
(5, 'বরবটি', 3, 'brbti', NULL, NULL, NULL, 3, 40.00, 0.00, 27, 1, '2021-11-02 18:54:03', '2021-11-02 18:54:03', NULL),
(6, 'কাচামরিচ', 3, 'kacamric', NULL, NULL, NULL, 3, 150.00, 0.00, 27, 1, '2021-11-02 18:54:54', '2021-11-02 18:54:54', NULL),
(7, 'শসা', 3, 'ssa', NULL, NULL, NULL, 3, 50.00, 0.00, 27, 1, '2021-11-02 18:55:58', '2021-11-02 18:55:58', NULL),
(8, 'আদা', 3, 'ada', NULL, NULL, NULL, 3, 120.00, 0.00, 27, 1, '2021-11-02 18:57:15', '2021-11-02 18:57:15', NULL),
(9, 'টমেটো', 3, 'tmeto', NULL, NULL, NULL, 3, 100.00, 0.00, 27, 1, '2021-11-02 19:01:51', '2021-11-02 19:01:51', NULL),
(10, 'লেবু', 3, 'lebu', NULL, NULL, NULL, 8, 5.00, 0.00, 27, 1, '2021-11-02 19:02:43', '2021-11-02 19:02:43', NULL),
(11, 'বাধাকপি', 3, 'badhakpi', NULL, NULL, NULL, 3, 45.00, 0.00, 27, 1, '2021-11-02 19:30:02', '2021-11-02 19:30:02', NULL),
(12, 'চাইনিজ পাতা', 3, 'cainij-pata', NULL, NULL, NULL, 3, 20.00, 0.00, 27, 1, '2021-11-02 19:31:20', '2021-11-02 19:31:20', NULL),
(13, 'পিয়াজ', 3, 'piyaj', NULL, NULL, NULL, 3, 60.00, 0.00, 27, 1, '2021-11-02 19:36:59', '2021-11-02 19:36:59', NULL),
(14, 'আলু', 3, 'alu', NULL, NULL, NULL, 3, 20.00, 0.00, 27, 1, '2021-11-02 19:40:31', '2021-11-02 19:40:31', NULL),
(15, 'রসুন', 3, 'rsun', NULL, NULL, NULL, 3, 120.00, 0.00, 27, 1, '2021-11-02 19:41:12', '2021-11-02 19:41:12', NULL),
(16, 'ক্যাপছিক্যাম', 3, 'kzapchikzam', NULL, NULL, NULL, 3, 50.00, 0.00, 27, 1, '2021-11-02 19:44:02', '2021-11-02 19:44:02', NULL),
(17, 'কাজুবাদাম', 4, 'kajubadam', NULL, NULL, NULL, 2, 100.00, 0.00, 27, 1, '2021-11-02 19:45:41', '2021-11-02 19:45:41', NULL),
(18, 'বাসমতি', 14, 'basmti', NULL, NULL, NULL, 10, 1500.00, 0.00, 27, 1, '2021-11-02 19:50:13', '2021-11-02 20:56:32', NULL),
(19, 'পাকিস্তানি বাসমতি', 14, 'pakistani-basmti-caul', NULL, NULL, NULL, 3, 250.00, 0.00, 27, 1, '2021-11-02 20:08:45', '2021-11-02 20:56:07', NULL),
(20, 'কোকাকোলা', 9, 'kokakola', NULL, NULL, NULL, 11, 350.00, 0.00, 27, 1, '2021-11-02 20:29:06', '2021-11-02 20:29:06', NULL),
(21, 'ফ্যান্টা', 9, 'fzanta', NULL, NULL, NULL, 11, 400.00, 0.00, 27, 1, '2021-11-02 20:30:15', '2021-11-02 20:30:15', NULL),
(22, 'স্প্রাইট', 9, 'sprait', NULL, NULL, NULL, 11, 400.00, 0.00, 27, 1, '2021-11-02 20:31:55', '2021-11-02 20:31:55', NULL),
(23, 'স্প্রাইট জিরো', 9, 'sprait-jiro', NULL, NULL, NULL, 11, 350.00, 0.00, 27, 1, '2021-11-02 20:33:55', '2021-11-02 20:33:55', NULL),
(24, 'কোকাকোলা জিরো', 9, 'kokakola-jirea', NULL, NULL, NULL, 11, 300.00, 0.00, 27, 1, '2021-11-02 20:34:52', '2021-11-02 20:34:52', NULL),
(25, 'বসুন্ধরা গ্যাস', 10, 'bsundhra-gzas', NULL, NULL, NULL, 3, 1200.00, 0.00, 27, 1, '2021-11-02 20:46:56', '2021-11-02 20:46:56', NULL),
(26, 'বয়লার মুরগীর ডিম', 7, 'bylar-murgeer-dim', NULL, NULL, NULL, 6, 100.00, 0.00, 27, 1, '2021-11-02 21:05:50', '2021-11-02 21:05:50', NULL),
(27, 'হাঁসের ডিম', 7, 'hannser-dim', NULL, NULL, NULL, 6, 130.00, 0.00, 27, 1, '2021-11-02 21:08:06', '2021-11-02 21:08:06', NULL),
(28, 'দেশৗ মুরগীর ডিম', 7, 'des-murgeer-dim', NULL, NULL, NULL, 6, 120.00, 0.00, 27, 1, '2021-11-02 21:09:41', '2021-11-02 21:09:41', NULL),
(29, 'বয়লার মুরগী', 6, 'bylar-murgee', NULL, NULL, NULL, 3, 140.00, 0.00, 27, 1, '2021-11-02 21:11:50', '2021-11-02 21:11:50', NULL),
(30, 'দেশী মুরগী', 6, 'desee-murgee', NULL, NULL, NULL, 3, 400.00, 0.00, 27, 1, '2021-11-02 21:13:15', '2021-11-02 21:13:15', NULL),
(31, 'লেয়ার মুরগী', 6, 'leyar-murgee', NULL, NULL, NULL, 3, 250.00, 0.00, 27, 1, '2021-11-02 21:15:05', '2021-11-02 21:15:05', NULL),
(32, 'হাঁস', 6, 'hanns', NULL, NULL, NULL, 3, 500.00, 0.00, 27, 1, '2021-11-02 21:16:34', '2021-11-02 21:16:34', NULL),
(33, 'গরু', 6, 'gru', NULL, NULL, NULL, 3, 600.00, 0.00, 27, 1, '2021-11-02 21:17:15', '2021-11-02 21:17:15', NULL),
(34, 'খাসি', 6, 'khasi', NULL, NULL, NULL, 3, 700.00, 0.00, 27, 1, '2021-11-02 21:18:01', '2021-11-02 21:18:01', NULL),
(35, 'সিম', 3, 'sim', NULL, NULL, NULL, 3, 100.00, 0.00, 27, 1, '2021-11-03 00:12:26', '2021-11-03 00:12:26', NULL),
(36, 'চিনি', 4, 'cini', NULL, NULL, NULL, 3, 75.00, 0.00, 27, 1, '2021-11-03 00:24:03', '2021-11-03 00:24:03', NULL),
(37, 'দেশি ডাল', 4, 'desi-dal', NULL, NULL, NULL, 3, 150.00, 0.00, 27, 1, '2021-11-03 00:24:57', '2021-11-03 00:24:57', NULL),
(38, 'সাবান গুড়া', 4, 'saban-gura', NULL, NULL, NULL, 3, 80.00, 0.00, 27, 1, '2021-11-03 00:28:06', '2021-11-03 00:28:06', NULL),
(39, 'বেগুন', 3, 'begun', NULL, NULL, NULL, 3, 50.00, 0.00, 27, 1, '2021-11-03 00:33:56', '2021-11-03 00:33:56', NULL),
(40, 'ধনের পাতা', 3, 'dhner-pata', NULL, NULL, NULL, 2, 20.00, 0.00, 27, 1, '2021-11-03 00:38:50', '2021-11-03 00:38:50', NULL),
(41, 'লাউ', 3, 'lau', NULL, NULL, NULL, 8, 40.00, 0.00, 27, 1, '2021-11-03 00:39:58', '2021-11-03 00:39:58', NULL),
(42, 'পিয়াজ পাতা', 3, 'piyaj-pata', NULL, NULL, NULL, 3, 200.00, 0.00, 27, 1, '2021-11-03 00:51:11', '2021-11-03 00:51:11', NULL),
(43, 'চিলি সস', 15, 'cili-ss', NULL, NULL, NULL, 12, 2600.00, 0.00, 27, 1, '2021-11-03 01:00:45', '2021-11-03 01:00:45', NULL),
(44, 'ফিস সস', 15, 'fis-ss', NULL, NULL, NULL, 8, 200.00, 0.00, 27, 1, '2021-11-03 01:03:37', '2021-11-03 01:03:37', NULL),
(45, 'ওয়েস্টার সস', 15, 'westar-ss', NULL, NULL, NULL, 12, 1200.00, 0.00, 27, 1, '2021-11-03 01:05:36', '2021-11-03 01:05:36', NULL),
(46, 'সয়া সস', 15, 'sya-ss', NULL, NULL, NULL, 12, 1300.00, 0.00, 27, 1, '2021-11-03 01:07:02', '2021-11-03 01:07:02', NULL),
(47, 'মাশরুম', 15, 'masrum', NULL, NULL, NULL, 8, 80.00, 0.00, 27, 1, '2021-11-03 01:07:57', '2021-11-03 01:07:57', NULL),
(48, 'ম্যাগী সস', 15, 'mzagee-ss', NULL, NULL, NULL, 8, 180.00, 0.00, 27, 1, '2021-11-03 01:10:39', '2021-11-03 01:10:39', NULL),
(49, 'কর্নফ্লাওয়ার', 15, 'krnflawar', NULL, NULL, NULL, 3, 100.00, 0.00, 27, 1, '2021-11-03 01:12:37', '2021-11-03 01:12:37', NULL),
(50, 'টেস্টিং সল্ট', 15, 'testing-slt', NULL, NULL, NULL, 12, 5000.00, 0.00, 27, 1, '2021-11-03 01:16:11', '2021-11-03 01:16:11', NULL),
(51, 'কারি পাওডার', 15, 'kari-paoodar', NULL, NULL, NULL, 8, 120.00, 0.00, 27, 1, '2021-11-03 01:18:10', '2021-11-03 01:18:10', NULL),
(52, 'টমেটো সস', 15, 'tmeto-ss', NULL, NULL, NULL, 8, 520.00, 0.00, 27, 1, '2021-11-03 01:38:20', '2021-11-03 01:38:20', NULL),
(53, 'গোল মরিচ', 4, 'gol-mric', NULL, NULL, NULL, 3, 800.00, 0.00, 27, 1, '2021-11-03 01:39:18', '2021-11-03 01:44:07', NULL),
(54, 'চিনা বাদাম', 4, 'cina-badam', NULL, NULL, NULL, 3, 120.00, 0.00, 27, 1, '2021-11-03 01:41:14', '2021-11-03 01:46:06', NULL),
(55, 'জয়ফল', 4, 'jyfl', NULL, NULL, NULL, 8, 10.00, 0.00, 27, 1, '2021-11-03 01:47:15', '2021-11-03 01:47:15', NULL),
(56, 'দারচিনি', 4, 'darcini', NULL, NULL, NULL, 3, 400.00, 0.00, 27, 1, '2021-11-03 01:53:55', '2021-11-03 01:53:55', NULL),
(57, 'এলাচ', 4, 'elac', NULL, NULL, NULL, 2, 280.00, 0.00, 27, 1, '2021-11-03 01:56:15', '2021-11-03 01:56:15', NULL),
(58, 'লং', 4, 'lng', NULL, NULL, NULL, 2, 110.00, 0.00, 27, 1, '2021-11-03 01:58:33', '2021-11-03 01:58:33', NULL),
(59, 'জিরা', 4, 'jira', NULL, NULL, NULL, 3, 400.00, 0.00, 27, 1, '2021-11-03 01:59:18', '2021-11-03 01:59:18', NULL),
(60, 'বার্গার বর্ন', 15, 'bargar-brn', NULL, NULL, NULL, 8, 20.00, 0.00, 27, 1, '2021-11-03 02:00:35', '2021-11-03 02:00:35', NULL),
(61, 'ফ্রেঞ্জফ্রাই', 15, 'frenjfrai', NULL, NULL, NULL, 8, 600.00, 0.00, 27, 1, '2021-11-03 02:08:28', '2021-11-03 02:08:28', NULL),
(62, 'চিজ', 15, 'cij', NULL, NULL, NULL, 8, 1100.00, 0.00, 27, 1, '2021-11-03 02:10:03', '2021-11-03 02:10:03', NULL),
(63, 'ব্লাক অলিভ', 15, 'blak-oliv', NULL, NULL, NULL, 3, 150.00, 0.00, 27, 1, '2021-11-03 02:16:50', '2021-11-03 02:16:50', NULL),
(64, 'পাতা সস', 15, 'pata-ss', NULL, NULL, NULL, 3, 1000.00, 0.00, 27, 1, '2021-11-03 02:18:08', '2021-11-03 02:18:08', NULL),
(65, 'পাস্তা', 15, 'pasta', NULL, NULL, NULL, 8, 200.00, 0.00, 27, 1, '2021-11-03 02:19:59', '2021-11-03 02:19:59', NULL),
(66, 'আইড় মাছ', 5, 'air-mach', NULL, NULL, NULL, 3, 500.00, 0.00, 27, 1, '2021-11-03 02:35:19', '2021-11-03 02:35:19', NULL),
(67, 'দেশী মোরগ', 6, 'desee-morg', NULL, NULL, NULL, 3, 600.00, 0.00, 27, 1, '2021-11-03 02:37:26', '2021-11-03 02:37:26', NULL),
(68, 'কাতল মাছ', 5, 'katl-mach', NULL, NULL, NULL, 3, 500.00, 0.00, 27, 1, '2021-11-03 02:38:14', '2021-11-03 02:38:14', NULL),
(69, 'টাকি মাছ', 5, 'taki-mach', NULL, NULL, NULL, 3, 200.00, 0.00, 27, 1, '2021-11-03 02:39:05', '2021-11-03 02:39:05', NULL),
(70, 'ছোট চিংড়ি', 5, 'chot-cingri', NULL, NULL, NULL, 3, 100.00, 0.00, 27, 1, '2021-11-03 02:40:39', '2021-11-03 02:40:39', NULL),
(71, 'বেলে মাছ', 5, 'bele-mach', NULL, NULL, NULL, 3, 1000.00, 0.00, 27, 1, '2021-11-03 02:41:46', '2021-11-03 02:41:46', NULL),
(72, 'টিসু', 12, 'tisu', NULL, NULL, NULL, 12, 500.00, 0.00, 27, 1, '2021-11-03 23:59:03', '2021-11-03 23:59:03', NULL),
(73, 'ইলিশ মাছ', 5, 'ilis-mach', NULL, NULL, NULL, 3, 1000.00, 0.00, 27, 1, '2021-11-04 00:01:41', '2021-11-04 00:01:41', NULL),
(74, 'পোলার চাউল', 14, 'polar-caul', NULL, NULL, NULL, 10, 2000.00, 0.00, 27, 1, '2021-11-04 00:03:37', '2021-11-04 00:03:37', NULL),
(75, 'রুই মাছ', 5, 'rui-mach', NULL, NULL, NULL, 3, 500.00, 0.00, 27, 1, '2021-11-04 00:05:04', '2021-11-04 00:05:04', NULL),
(76, 'রুপ চাঁদা', 5, 'rup-cannda', NULL, NULL, NULL, 3, 1000.00, 0.00, 27, 1, '2021-11-04 00:06:35', '2021-11-04 00:06:35', NULL),
(77, 'বড় চিংড়ি', 5, 'br-cingri', NULL, NULL, NULL, 3, 1200.00, 0.00, 27, 1, '2021-11-04 01:01:19', '2021-11-04 01:01:19', NULL),
(78, 'বোয়াল মাছ', 5, 'boyal-mach', NULL, NULL, NULL, 3, 700.00, 0.00, 27, 1, '2021-11-04 01:04:02', '2021-11-04 01:04:02', NULL),
(79, 'শোল মাছ', 5, 'sol-mach', NULL, NULL, NULL, 3, 300.00, 0.00, 27, 1, '2021-11-04 01:06:02', '2021-11-04 01:06:02', NULL),
(80, 'পাবদা মাছ', 5, 'pabda-mach', NULL, NULL, NULL, 3, 400.00, 0.00, 27, 1, '2021-11-04 02:20:13', '2021-11-04 02:20:13', NULL),
(81, 'পোলার আইসক্রিম', 16, 'polar-aiskrim', NULL, NULL, NULL, 12, 500.00, 0.00, 27, 1, '2021-11-04 23:24:14', '2021-11-04 23:24:14', NULL),
(82, 'ইগলু আইসক্রিম', 16, 'iglu-aiskrim', NULL, NULL, NULL, 12, 700.00, 0.00, 27, 1, '2021-11-05 00:12:48', '2021-11-05 00:12:48', NULL),
(83, 'ভ্যানিলা আইসক্রিম', 16, 'vzanila-aiskrim', NULL, NULL, NULL, 12, 700.00, 0.00, 27, 1, '2021-11-05 00:14:59', '2021-11-05 00:14:59', NULL),
(84, 'ছোট পানি', 9, 'chot-pani', NULL, NULL, NULL, 11, 500.00, 0.00, 27, 1, '2021-11-05 00:22:16', '2021-11-05 00:22:16', NULL),
(85, 'বড় পানি', 9, 'br-pani', NULL, NULL, NULL, 12, 600.00, 0.00, 27, 1, '2021-11-05 00:23:30', '2021-11-05 00:23:30', NULL),
(86, 'কাছকি মাছ', 5, 'kachki-mach', NULL, NULL, NULL, 3, 200.00, 0.00, 27, 1, '2021-11-05 00:26:12', '2021-11-05 00:26:12', NULL),
(87, 'শিং মাছ', 5, 'sing-mach', NULL, NULL, NULL, 3, 600.00, 0.00, 27, 1, '2021-11-05 00:28:20', '2021-11-05 00:28:20', NULL),
(88, 'টেংরা মাছ', 5, 'tengra-mach', NULL, NULL, NULL, 3, 600.00, 0.00, 27, 1, '2021-11-05 00:34:03', '2021-11-05 00:34:03', NULL),
(89, 'বার-বি-কিউ সস', 15, 'bar-bi-kiu-ss', NULL, NULL, NULL, 8, 240.00, 0.00, 27, 1, '2021-11-05 00:58:17', '2021-11-05 00:58:17', NULL),
(90, 'স্ট্রোবেরী  সিরাপ', 15, 'stroberee-sirap', NULL, NULL, NULL, 8, 340.00, 0.00, 27, 1, '2021-11-05 00:59:43', '2021-11-05 00:59:43', NULL),
(91, 'অরেঞ্জ জেলী', 15, 'orenj-jelee', NULL, NULL, NULL, 8, 185.00, 0.00, 27, 1, '2021-11-05 01:01:15', '2021-11-05 01:01:15', NULL),
(92, 'বনর্লেছ (চিকেন)', 15, 'bnrlech-ciken', NULL, NULL, NULL, 3, 300.00, 0.00, 27, 1, '2021-11-05 01:03:02', '2021-11-05 01:03:02', NULL),
(93, 'বনর্লেছ (বিফ)', 15, 'bnrlech-bif', NULL, NULL, NULL, 3, 400.00, 0.00, 27, 1, '2021-11-05 01:04:57', '2021-11-05 01:04:57', NULL),
(94, 'অরিও বিস্কুট', 15, 'orioo-biskut', NULL, NULL, NULL, 8, 230.00, 0.00, 27, 1, '2021-11-05 01:06:37', '2021-11-05 01:06:37', NULL),
(95, 'ফয়েল পেপার', 15, 'fyel-pepar', NULL, NULL, NULL, 8, 100.00, 0.00, 27, 1, '2021-11-05 01:20:06', '2021-11-05 01:20:06', NULL),
(96, 'গালির্ফ পাাউডার', 15, 'galirf-paaudar', NULL, NULL, NULL, 2, 100.00, 0.00, 27, 1, '2021-11-05 01:22:24', '2021-11-05 01:22:24', NULL),
(97, 'সুইটবল', 15, 'suitbl', NULL, NULL, NULL, 2, 85.00, 0.00, 27, 1, '2021-11-05 01:23:28', '2021-11-05 01:23:28', NULL),
(98, 'বাটার', 15, 'batar', NULL, NULL, NULL, 8, 100.00, 0.00, 27, 1, '2021-11-05 01:24:39', '2021-11-05 01:24:39', NULL),
(99, 'স্লাইস চিজ', 15, 'slais-cij', NULL, NULL, NULL, 8, 800.00, 0.00, 27, 1, '2021-11-05 01:26:13', '2021-11-05 01:26:13', NULL),
(100, 'অরিগানো', 15, 'origano', NULL, NULL, NULL, 2, 100.00, 0.00, 27, 1, '2021-11-05 01:27:13', '2021-11-05 01:27:13', NULL),
(101, 'সচেজ', 15, 'scej', NULL, NULL, NULL, 8, 100.00, 0.00, 27, 1, '2021-11-05 01:28:18', '2021-11-05 01:28:18', NULL),
(102, 'মেথী পাতা', 3, 'methee-pata', NULL, NULL, NULL, 8, 50.00, 0.00, 27, 1, '2021-11-05 01:29:06', '2021-11-05 01:29:06', NULL),
(103, 'ড্যামস্টিক', 15, 'dzamstik', NULL, NULL, NULL, 3, 200.00, 0.00, 27, 1, '2021-11-05 01:30:22', '2021-11-05 01:30:22', NULL),
(104, 'চিলি', 15, 'cili', NULL, NULL, NULL, 8, 500.00, 0.00, 27, 1, '2021-11-05 01:31:38', '2021-11-05 01:31:38', NULL),
(105, 'বার্গার বন', 15, 'bargar-bn', NULL, NULL, NULL, 8, 200.00, 0.00, 27, 1, '2021-11-05 01:33:38', '2021-11-05 01:33:38', NULL),
(106, 'সাব স্যান্ডুইচ', 15, 'sab-szanduic', NULL, NULL, NULL, 8, 200.00, 0.00, 27, 1, '2021-11-05 01:36:52', '2021-11-05 01:36:52', NULL),
(107, 'স্যান্ডুইচ বন', 15, 'szanduic-bn', NULL, NULL, NULL, 8, 150.00, 0.00, 27, 1, '2021-11-05 01:37:43', '2021-11-05 01:37:43', NULL),
(108, 'পলি', 4, 'pli', NULL, NULL, NULL, 3, 100.00, 0.00, 27, 1, '2021-11-05 01:38:42', '2021-11-05 01:38:42', NULL),
(109, 'কমলা', 17, 'kmla', NULL, NULL, NULL, 3, 200.00, 0.00, 27, 1, '2021-11-05 19:36:32', '2021-11-05 19:36:32', NULL),
(110, 'আপেল', 17, 'apel', NULL, NULL, NULL, 3, 150.00, 0.00, 27, 1, '2021-11-05 19:37:59', '2021-11-05 19:37:59', NULL),
(111, 'আনার (ডালিম)', 17, 'anar-dalim', NULL, NULL, NULL, 3, 350.00, 0.00, 27, 1, '2021-11-05 19:39:24', '2021-11-05 19:39:24', NULL),
(112, 'আনারস', 17, 'anars', NULL, NULL, NULL, 8, 50.00, 0.00, 27, 1, '2021-11-05 19:40:06', '2021-11-05 19:40:06', NULL),
(113, 'আঙ্গুর', 17, 'anggur', NULL, NULL, NULL, 3, 250.00, 0.00, 27, 1, '2021-11-05 19:41:33', '2021-11-07 01:47:46', NULL),
(114, 'আম', 17, 'am', NULL, NULL, NULL, 3, 100.00, 0.00, 27, 1, '2021-11-05 19:42:47', '2021-11-05 19:42:47', NULL),
(115, 'কলা', 17, 'kla', NULL, NULL, NULL, 13, 30.00, 0.00, 27, 1, '2021-11-05 19:46:39', '2021-11-05 19:46:39', NULL),
(116, 'দধি', 18, 'ddhi', NULL, NULL, NULL, 3, 250.00, 0.00, 27, 1, '2021-11-07 00:37:46', '2021-11-07 00:37:46', NULL),
(117, 'রসগোল্লা', 18, 'rsgolla', NULL, NULL, NULL, 3, 150.00, 0.00, 27, 1, '2021-11-07 00:48:28', '2021-11-07 00:48:28', NULL),
(118, 'কালোজাম', 18, 'kalojam', NULL, NULL, NULL, 3, 150.00, 0.00, 27, 1, '2021-11-07 00:51:35', '2021-11-07 00:51:35', NULL),
(119, 'ছানা', 18, 'chana', NULL, NULL, NULL, 3, 500.00, 0.00, 27, 1, '2021-11-07 00:52:59', '2021-11-07 00:52:59', NULL),
(121, 'চমচম', 18, 'cmcm', NULL, NULL, NULL, 3, 150.00, 0.00, 27, 1, '2021-11-07 00:56:59', '2021-11-07 00:56:59', NULL),
(122, 'শুকনা মরিচ', 3, 'sukna-mric', NULL, NULL, NULL, 3, 200.00, 0.00, 27, 1, '2021-11-07 00:59:19', '2021-11-07 00:59:19', NULL),
(123, 'মাখন', 18, 'makhn', NULL, NULL, NULL, 3, 400.00, 0.00, 27, 1, '2021-11-07 01:01:25', '2021-11-07 01:01:25', NULL),
(124, 'ঘি', 18, 'ghi', NULL, NULL, NULL, 3, 1200.00, 0.00, 27, 1, '2021-11-07 01:17:52', '2021-11-07 01:17:52', NULL),
(125, 'লেটুস পাতা', 15, 'letus-pata', NULL, NULL, NULL, 8, 20.00, 0.00, 27, 1, '2021-11-07 01:30:05', '2021-11-07 01:30:05', NULL),
(126, 'শষা', 3, 'ssha', NULL, NULL, NULL, 3, 80.00, 0.00, 27, 1, '2021-11-07 01:32:56', '2021-11-07 01:32:56', NULL),
(127, 'পাতা কপি', 3, 'pata-kpi', NULL, NULL, NULL, 3, 20.00, 0.00, 27, 1, '2021-11-07 01:38:33', '2021-11-07 01:38:33', NULL),
(128, 'কুমড়া', 3, 'kumra', NULL, NULL, NULL, 3, 30.00, 0.00, 27, 1, '2021-11-07 01:39:19', '2021-11-07 01:39:19', NULL),
(129, 'কালো আঙ্গুর', 17, 'kalo-anggur', NULL, NULL, NULL, 3, 200.00, 0.00, 27, 1, '2021-11-07 01:46:04', '2021-11-07 01:47:25', NULL),
(130, 'লাল শাক', 3, 'lal-sak', NULL, NULL, NULL, 3, 30.00, 0.00, 27, 1, '2021-11-07 02:03:57', '2021-11-07 02:03:57', NULL),
(131, 'পালন শাক', 3, 'paln-sak', NULL, NULL, NULL, 3, 40.00, 0.00, 27, 1, '2021-11-07 02:04:55', '2021-11-07 02:04:55', NULL),
(132, 'কাচ কলা', 3, 'kac-kla', NULL, NULL, NULL, 3, 30.00, 0.00, 27, 1, '2021-11-08 17:12:30', '2021-11-08 17:12:30', NULL),
(133, 'পেস্তা বাদাম', 4, 'pesta-badam', NULL, NULL, NULL, 3, 1000.00, 0.00, 27, 1, '2021-11-08 17:15:39', '2021-11-08 17:15:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `leaves`
--

CREATE TABLE `leaves` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `staff_id` bigint(20) NOT NULL,
  `reason` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `end_date` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ledgers`
--

CREATE TABLE `ledgers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ledgerable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ledgerable_id` bigint(20) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `credit` tinyint(1) NOT NULL DEFAULT 0,
  `debit` tinyint(1) NOT NULL DEFAULT 0,
  `amount` double(8,2) NOT NULL,
  `current_balance` double(8,2) DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_03_31_065158_create_web_infos_table', 1),
(5, '2021_05_23_105157_create_categories_table', 1),
(6, '2021_05_24_062924_create_products_table', 1),
(7, '2021_06_13_102442_product_category', 2),
(35, '2021_06_17_100051_create_orders_table', 3),
(36, '2021_06_20_045956_create_order_items_table', 3),
(38, '2021_06_20_070023_web_info', 4),
(39, '2021_06_17_100149_create_oder_items_table', 5),
(40, '2021_06_23_085244_create_permission_tables', 5),
(41, '2021_09_01_074752_add_kot_to_order', 6),
(42, '2021_06_26_065927_creat_product', 7),
(43, '2021_06_26_070008_creat_cetagory', 7),
(44, '2021_06_26_083111_products_categories', 7),
(45, '2021_09_02_042319_create_types_table', 7),
(52, '2021_09_02_051303_create_items_table', 8),
(53, '2021_09_02_061532_create_units_table', 8),
(54, '2021_09_04_033816_create_suppliers_table', 8),
(62, '2021_09_05_083037_create_staff_types_table', 11),
(70, '2021_09_04_060725_create_stock_items_table', 16),
(71, '2021_10_03_040351_create_purchases_table', 17),
(73, '2021_10_03_041041_create_purchase_items_table', 18),
(75, '2021_10_04_042801_create_customers_table', 19),
(77, '2021_10_05_072431_create_tables_table', 20),
(78, '2021_10_06_051039_create_asset_categories_table', 21),
(79, '2021_10_06_061639_create_assets_table', 22),
(80, '2021_10_10_051243_create_asset_purchases_table', 23),
(81, '2021_10_10_051654_create_asset_purchase_items_table', 23),
(83, '2021_09_05_081808_create_staff_table', 25),
(84, '2018_09_12_99999_create_backupverify_table', 26),
(86, '2021_10_13_085755_create_attendances_table', 27),
(90, '2021_10_24_041118_create_leaves_table', 28),
(91, '2021_10_24_041248_create_holidays_table', 29),
(94, '2021_10_27_061013_create_payrolls_table', 30),
(95, '2021_12_02_095854_create_income_categories_table', 31),
(96, '2021_12_02_100029_create_expence_categories_table', 31),
(103, '2021_12_05_045153_create_incomes_table', 32),
(104, '2021_12_05_092821_create_expences_table', 33),
(110, '2021_09_04_060422_create_stocks_table', 34),
(112, '2021_12_23_114317_add_subtotal_in_order_item', 36),
(114, '2021_10_10_093646_create_ledgers_table', 37),
(115, '2021_12_26_154956_add_status_to_ledger', 38);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(3, 'App\\Models\\User', 26);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax` double(10,2) DEFAULT NULL,
  `vat` double(10,2) DEFAULT NULL,
  `total` double(10,2) DEFAULT NULL,
  `discount` double(10,2) DEFAULT NULL,
  `discount_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_discount` double(10,2) DEFAULT NULL,
  `service_charge` double(10,2) DEFAULT NULL,
  `total_item` int(11) DEFAULT NULL,
  `grand_total` double(10,2) DEFAULT NULL,
  `payment_status` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `kot` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collected` double(10,2) DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double(10,2) DEFAULT NULL,
  `subtotal` double(10,2) DEFAULT NULL,
  `quantity` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `discount_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payrolls`
--

CREATE TABLE `payrolls` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `staff_id` int(11) NOT NULL,
  `allawence` decimal(15,2) DEFAULT NULL,
  `bonus` decimal(15,2) DEFAULT NULL,
  `advance` decimal(15,2) DEFAULT NULL,
  `advance_deduction` decimal(15,2) DEFAULT NULL,
  `absent_deduction` decimal(15,2) DEFAULT NULL,
  `other_deduction` decimal(15,2) DEFAULT NULL,
  `basic_salary` decimal(15,2) DEFAULT NULL,
  `total_paid` decimal(15,2) DEFAULT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'dashboard', 'web', '2021-08-25 03:57:45', '2021-08-25 03:57:45'),
(2, 'pos', 'web', '2021-08-25 03:57:45', '2021-08-25 03:57:45'),
(3, 'orders', 'web', '2021-08-25 03:57:45', '2021-08-25 03:57:45'),
(4, 'web-infos', 'web', '2021-08-25 03:57:45', '2021-08-25 03:57:45'),
(5, 'categories-index', 'web', '2021-08-25 03:57:45', '2021-08-25 03:57:45'),
(6, 'categories-create', 'web', '2021-08-25 03:57:46', '2021-08-25 03:57:46'),
(7, 'categories-show', 'web', '2021-08-25 03:57:46', '2021-08-25 03:57:46'),
(8, 'categories-update', 'web', '2021-08-25 03:57:46', '2021-08-25 03:57:46'),
(9, 'categories-destroy', 'web', '2021-08-25 03:57:46', '2021-08-25 03:57:46'),
(10, 'products-index', 'web', '2021-08-25 03:57:46', '2021-08-25 03:57:46'),
(11, 'products-create', 'web', '2021-08-25 03:57:46', '2021-08-25 03:57:46'),
(12, 'products-show', 'web', '2021-08-25 03:57:46', '2021-08-25 03:57:46'),
(13, 'products-update', 'web', '2021-08-25 03:57:46', '2021-08-25 03:57:46'),
(14, 'products-destroy', 'web', '2021-08-25 03:57:46', '2021-08-25 03:57:46'),
(15, 'admins-index', 'web', '2021-08-25 03:57:46', '2021-08-25 03:57:46'),
(16, 'admins-create', 'web', '2021-08-25 03:57:46', '2021-08-25 03:57:46'),
(17, 'admins-show', 'web', '2021-08-25 03:57:46', '2021-08-25 03:57:46'),
(18, 'admins-update', 'web', '2021-08-25 03:57:46', '2021-08-25 03:57:46'),
(19, 'admins-destroy', 'web', '2021-08-25 03:57:46', '2021-08-25 03:57:46'),
(20, 'roles-index', 'web', '2021-08-25 03:57:46', '2021-08-25 03:57:46'),
(21, 'roles-create', 'web', '2021-08-25 03:57:46', '2021-08-25 03:57:46'),
(22, 'roles-show', 'web', '2021-08-25 03:57:46', '2021-08-25 03:57:46'),
(23, 'roles-update', 'web', '2021-08-25 03:57:46', '2021-08-25 03:57:46'),
(24, 'roles-destroy', 'web', '2021-08-25 03:57:46', '2021-08-25 03:57:46');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'chinese',
  `slug` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `price` double(8,2) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `type`, `slug`, `image`, `description`, `status`, `price`, `stock`, `user_id`, `created_at`, `updated_at`) VALUES
(55, 'American Chopsy (sweet)', 'chinese', 'cream-pasta', '/uploads/images/products/image1635831614.png', NULL, 1, 300.00, 50, 1, '2021-06-20 18:30:26', '2021-11-01 17:40:14'),
(56, 'Chinese Chopsi (Salted)', 'chinese', 'special-pasta', '/uploads/images/products/image1635831682.png', NULL, 1, 300.00, 50, 1, '2021-06-20 18:30:26', '2021-11-01 17:41:22'),
(59, 'Vegetable Noodles', 'chinese', 'prawn-fried-noodles', '/uploads/images/products/image1635831055.png', NULL, 1, 300.00, 50, 1, '2021-06-20 18:30:27', '2021-11-01 17:30:55'),
(60, 'Chicken Fried Noodles', 'chinese', 'sichuan-fried-noodles', '/uploads/images/products/image1635831984.png', NULL, 1, 330.00, 50, 1, '2021-06-20 18:30:27', '2021-11-01 17:46:24'),
(61, 'Thai Fried Noodles', 'chinese', 'thai-fried-noodles', '/uploads/images/products/image1635831409.png', NULL, 1, 380.00, 50, 1, '2021-06-20 18:30:27', '2021-11-01 17:36:49'),
(62, 'Sichuan Fried Noodles', 'chinese', 'chicken-fried-noodles', '/uploads/images/products/image1635831484.png', NULL, 1, 300.00, 50, 1, '2021-06-20 18:30:27', '2021-11-01 17:38:04'),
(63, 'Prawn Fried Noodles', 'chinese', 'vegetable-noodles', '/uploads/images/products/image1635831543.png', NULL, 1, 350.00, 50, 1, '2021-06-20 18:30:27', '2021-11-01 17:39:03'),
(64, '71 Utsav Special Noodles', 'chinese', 'special-noodles', '/uploads/images/products/image1635830981.png', NULL, 1, 380.00, 50, 1, '2021-06-20 18:30:28', '2021-11-01 17:29:41'),
(75, 'Chicken Fried Rice', 'chinese', 'steamed-rice', '/uploads/images/products/image1635829346.png', NULL, 1, 330.00, 50, 1, '2021-06-20 18:30:32', '2021-11-01 17:02:26'),
(76, 'Prawn Fried Rice', 'chinese', 'sichuan-fried-rice', '/uploads/images/products/0image1622607904.png', NULL, 1, 350.00, 50, 1, '2021-06-20 18:30:32', '2021-11-01 03:16:47'),
(77, 'Vegetable Fried Rice', 'chinese', 'masala-fried-rice', '/uploads/images/products/0image1622607862.png', NULL, 1, 300.00, 50, 1, '2021-06-20 18:30:32', '2021-11-01 03:17:53'),
(78, 'Masala Fried Rice', 'chinese', 'vegetable-fried-rice', '/uploads/images/products/0image1622607818.png', NULL, 1, 300.00, 50, 1, '2021-06-20 18:30:32', '2021-11-01 03:19:22'),
(79, 'Fantasy Rice', 'chinese', 'prawn-fried-rice', '/uploads/images/products/0image1622607754.png', NULL, 1, 300.00, 50, 1, '2021-06-20 18:30:33', '2021-11-01 02:42:30'),
(80, 'Thai Fried Rice', 'chinese', 'chicken-fried-rice', '/uploads/images/products/0image1622607717.png', NULL, 1, 380.00, 50, 1, '2021-06-20 18:30:33', '2021-11-01 02:50:36'),
(81, 'Egg Fried Rice', 'chinese', 'egg-fried-rice', '/uploads/images/products/0image1622607627.png', NULL, 1, 300.00, 50, 1, '2021-06-20 18:30:33', '2021-11-01 02:52:52'),
(101, 'Beef Chili Onion', 'chinese', 'beef-chili-dry', '/uploads/images/products/0image1622606415.png', NULL, 1, 380.00, 50, 1, '2021-06-20 18:30:44', '2021-11-11 23:37:39'),
(102, 'Beef Chili (Dry)', 'chinese', 'beef-chili-onion', '/uploads/images/products/0image1622606380.png', NULL, 1, 380.00, 50, 1, '2021-06-20 18:30:44', '2021-11-11 23:38:14'),
(103, 'Beef Sizzling', 'chinese', 'beef-masala', '/uploads/images/products/0image1622606340.png', NULL, 1, 450.00, 50, 1, '2021-06-20 18:30:44', '2021-11-10 23:59:42'),
(104, 'Red Sanpper Mushroom & Ginger', 'chinese', 'red-sanpper-bbq', '/uploads/images/products/0image1622606284.png', NULL, 1, 500.00, 50, 1, '2021-06-20 18:30:45', '2021-11-02 17:30:51'),
(105, 'Red Sanpper B.B.Q', 'chinese', 'red-sanpper-mushroom-7-ginger', '/uploads/images/products/0image1622606238.png', NULL, 1, 450.00, 50, 1, '2021-06-20 18:30:45', '2021-11-02 17:31:39'),
(106, 'Rupchada Fish Fry', 'chinese', 'red-sanpper-fry', '/uploads/images/products/0image1622543537.png', NULL, 1, 450.00, 50, 1, '2021-06-20 18:30:46', '2021-11-02 17:28:36'),
(107, 'Red Sanpper With Hot Sauce', 'chinese', 'red-sanpper-with-hot-sauce', '/uploads/images/products/0image1622543485.png', NULL, 1, 450.00, 50, 1, '2021-06-20 18:30:46', '2021-06-20 18:30:46'),
(108, 'Red Sanpper Fry', 'chinese', 'rupchada-fish-fry', '/uploads/images/products/0image1622543444.png', NULL, 1, 450.00, 50, 1, '2021-06-20 18:30:46', '2021-11-02 17:29:58'),
(109, 'King Prawn Hot sauce praw', 'chinese', 'prawn-sizzling', '/uploads/images/products/0image1622543413.png', NULL, 1, 600.00, 50, 1, '2021-06-20 18:30:47', '2021-11-02 17:26:51'),
(110, 'Prawn Sizzling', 'chinese', 'king-prawn-hot-sauce', '/uploads/images/products/0image1622543386.png', NULL, 1, 450.00, 50, 1, '2021-06-20 18:30:47', '2021-11-02 17:27:40'),
(111, '71 Utsav Prawn Masala', 'chinese', 'prawn-masala', '/uploads/images/products/0image1622543350.png', NULL, 1, 450.00, 50, 1, '2021-06-20 18:30:49', '2021-11-02 17:25:39'),
(112, 'Mutton Masala', 'chinese', 'mutton-karai-sizzling', '/uploads/images/products/image1635842952.png', NULL, 1, 450.00, 50, 1, '2021-06-20 18:30:51', '2021-11-01 20:49:12'),
(116, 'Chicken Mushroom with Ginger', 'chinese', 'special-foil-chicken-curry', '/uploads/images/products/0image1622542964.png', NULL, 1, 350.00, 50, 1, '2021-06-20 18:30:55', '2021-11-01 19:55:00'),
(118, 'Sweet & Sour Chicken', 'chinese', 'szuan-chicken-curry', '/uploads/images/products/0image1622542834.png', NULL, 1, 350.00, 50, 1, '2021-06-20 18:30:56', '2021-11-01 19:53:58'),
(119, 'Special Foil Chicken Curry', 'chinese', 'lemon-chicken', '/uploads/images/products/0image1622542780.png', NULL, 1, 400.00, 50, 1, '2021-06-20 18:30:57', '2021-11-01 19:53:14'),
(120, 'Chicken Sizzling', 'chinese', 'bangkok-chicken-fry-8-pcs', '/uploads/images/products/0image1622542635.png', NULL, 1, 420.00, 50, 1, '2021-06-20 18:30:58', '2021-11-01 19:51:59'),
(121, 'Lemon Chicken', 'chinese', 'chicken-chili-dry', '/uploads/images/products/0image1622542529.png', NULL, 1, 350.00, 50, 1, '2021-06-20 18:30:59', '2021-11-01 19:49:42'),
(122, 'Szuan Chicken Curry', 'chinese', 'chicken-chili-onion', '/uploads/images/products/0image1622542485.png', NULL, 1, 350.00, 50, 1, '2021-06-20 18:30:59', '2021-11-01 19:50:45'),
(123, 'Chicken Chili (Dry)', 'chinese', 'chicken-masala', '/uploads/images/products/0image1622542432.png', NULL, 1, 350.00, 50, 1, '2021-06-20 18:31:00', '2021-11-01 19:47:27'),
(124, 'Bangkok Chicken Fry (8 pcs)', 'chinese', 'chicken-cutlets', '/uploads/images/products/0image1622542392.png', NULL, 1, 340.00, 50, 1, '2021-06-20 18:31:00', '2021-11-01 19:48:56'),
(125, 'Chicken Chili Onion', 'chinese', 'chicken-lolipop', '/uploads/images/products/0image1622542317.png', NULL, 1, 350.00, 50, 1, '2021-06-20 18:31:01', '2021-11-01 19:46:38'),
(126, 'Chicken B.B.Q (6pcs)', 'chinese', 'chicken-salay', '/uploads/images/products/0image1622542281.png', NULL, 1, 450.00, 50, 1, '2021-06-20 18:31:02', '2021-11-01 19:43:32'),
(127, 'Chicken Cutlets', 'chinese', 'spring-chicken-fry', '/uploads/images/products/0image1622542229.png', NULL, 1, 380.00, 50, 1, '2021-06-20 18:31:02', '2021-11-01 19:44:37'),
(128, 'Chicken Masala', 'chinese', 'chicken-bbq', '/uploads/images/products/0image1622542160.png', NULL, 1, 380.00, 50, 1, '2021-06-20 18:31:02', '2021-11-01 19:45:46'),
(129, '71 Utsav Special Chicken Fry', 'chinese', 'special-sichuan-chicken-fry', '/uploads/images/products/0image1622542128.png', NULL, 1, 420.00, 50, 1, '2021-06-20 18:31:03', '2021-11-01 19:40:27'),
(130, 'Special Sichuan Chicken Fry', 'chinese', 'special-chicken-fry', '/uploads/images/products/0image1622542079.png', NULL, 1, 400.00, 50, 1, '2021-06-20 18:31:03', '2021-11-01 19:42:15'),
(131, 'Cheese Sandwich', 'chinese', 'cheese-sandwich', '/uploads/images/products/0image1622542021.png', NULL, 1, 120.00, 50, 1, '2021-06-20 18:31:04', '2021-06-20 18:31:04'),
(132, 'Club Sandwich', 'chinese', 'club-sandwich', '/uploads/images/products/0image1622541987.png', NULL, 1, NULL, 55, 1, '2021-06-20 18:31:07', '2021-10-17 19:21:39'),
(133, 'Special Pizza 12\"', 'chinese', 'special-pizza-12', '/uploads/images/products/0image1622541958.png', NULL, 1, 590.00, 50, 1, '2021-06-20 18:31:07', '2021-06-20 18:31:07'),
(134, 'Special Pizza 8\"', 'chinese', 'special-pizza-8', '/uploads/images/products/0image1622541925.png', NULL, 1, 450.00, 50, 1, '2021-06-20 18:31:08', '2021-06-20 18:31:09'),
(135, 'Crispy Chicken', 'chinese', 'crispy-chicken', '/uploads/images/products/0image1622541886.png', NULL, 1, 60.00, 50, 1, '2021-06-20 18:31:09', '2021-06-20 18:31:09'),
(136, 'Special Burger', 'chinese', 'special-burger', '/uploads/images/products/0image1622541851.png', NULL, 1, 120.00, 50, 1, '2021-06-20 18:31:09', '2021-06-20 18:31:10'),
(137, 'Mixed Vegetables', 'chinese', 'mashroom-vegetable', '/uploads/images/products/0image1622541755.png', NULL, 1, 200.00, 50, 1, '2021-06-20 18:31:10', '2021-11-01 19:34:50'),
(138, 'Beef Vegetable', 'chinese', 'mixed-vegetables', '/uploads/images/products/0image1622541717.png', NULL, 1, 280.00, 50, 1, '2021-06-20 18:31:11', '2021-11-01 19:33:04'),
(139, 'Mashroom Vegetable', 'chinese', 'beef-vegetable', '/uploads/images/products/0image1622541687.png', NULL, 1, 250.00, 50, 1, '2021-06-20 18:31:11', '2021-11-01 19:33:52'),
(140, 'Prawn Vegetable', 'chinese', 'prawn-vegetable', '/uploads/images/products/0image1622541649.png', NULL, 1, 280.00, 50, 1, '2021-06-20 18:31:12', '2021-06-20 18:31:12'),
(141, '71 Utsav Special Vegetable', 'chinese', 'chicken-vegetable', '/uploads/images/products/0image1622541619.png', NULL, 1, 300.00, 50, 1, '2021-06-20 18:31:13', '2021-11-01 19:29:23'),
(142, 'Chicken Vegetable', 'chinese', 'special-vegetable', '/uploads/images/products/0image1622541591.png', NULL, 1, 240.00, 50, 1, '2021-06-20 18:31:13', '2021-11-01 19:30:11'),
(143, '71 Utsav Special Rice', 'chinese', '1634970601ksschi', '/uploads/images/products/image1635829375.png', '<p>rghjhj</p>', 1, 380.00, NULL, 1, '2021-10-22 18:30:01', '2021-11-01 17:02:55'),
(144, '71 Utsav Beef Masala', 'bangla', '163558927671-utsav-special-beef-burger', '/uploads/images/products/image1637301612.png', NULL, 1, 400.00, NULL, 27, '2021-10-29 22:21:16', '2021-12-26 06:27:17'),
(146, 'Beef Burger (Regular)', 'chinese', '1635590960beef-burger-regular', '/uploads/images/products/image1635682846.png', NULL, 1, 180.00, NULL, 27, '2021-10-29 22:49:20', '2021-10-31 00:20:46'),
(147, 'Beef Cheese Burger', 'chinese', '1635591214beef-cheese-burger', '/uploads/images/products/image1635683265.png', NULL, 1, 200.00, NULL, 27, '2021-10-29 22:53:34', '2021-10-31 00:27:45'),
(148, 'Naga Beef Burger', 'chinese', '1635591369naga-beef-burger', '/uploads/images/products/image1635682884.png', NULL, 1, 220.00, NULL, 27, '2021-10-29 22:56:09', '2021-10-31 00:21:24'),
(149, 'B.B.Q Beef Burger', 'chinese', '1635591442bbq-beef-burger', '/uploads/images/products/image1635682901.png', NULL, 1, 220.00, NULL, 27, '2021-10-29 22:57:22', '2021-10-31 00:21:41'),
(150, '71 Utsav Special Burger', 'chinese', '163559167771-utsav-special-burger', '/uploads/images/products/image1635591677.png', NULL, 1, 250.00, NULL, 27, '2021-10-29 23:01:17', '2021-10-29 23:01:17'),
(151, 'Chicken Burger (Regular)', 'chinese', '1635591750chicken-burger-regular', '/uploads/images/products/image1635682134.png', NULL, 1, 120.00, NULL, 27, '2021-10-29 23:02:30', '2021-10-31 00:08:54'),
(152, 'Chicken Crispy Burger', 'chinese', '1635591800chicken-crispy-burger', '/uploads/images/products/image1635682168.png', NULL, 1, 160.00, NULL, 27, '2021-10-29 23:03:20', '2021-10-31 00:09:28'),
(153, 'B.B.Q Chicken Burger', 'chinese', '1635591903bbq-chicken-burger', '/uploads/images/products/image1635682188.png', NULL, 1, 180.00, NULL, 27, '2021-10-29 23:05:03', '2021-10-31 00:09:48'),
(154, 'Naga Chicken Burger', 'chinese', '1635591952naga-chicken-burger', '/uploads/images/products/image1635682218.png', NULL, 1, 170.00, NULL, 27, '2021-10-29 23:05:52', '2021-10-31 00:10:18'),
(155, 'Double Chicken Burger', 'chinese', '1635592006double-chicken-burger', '/uploads/images/products/image1635682235.png', NULL, 1, 220.00, NULL, 27, '2021-10-29 23:06:46', '2021-10-31 00:10:35'),
(156, 'B.B.Q Chicken Cheese Burger', 'chinese', '1635592078bbq-chicken-cheese-burger', '/uploads/images/products/image1635682252.png', NULL, 1, 200.00, NULL, 27, '2021-10-29 23:07:58', '2021-10-31 00:10:52'),
(157, 'Naga Chicken Cheese Burger', 'chinese', '1635592138naga-chicken-cheese-burger', '/uploads/images/products/image1635682268.png', NULL, 1, 190.00, NULL, 27, '2021-10-29 23:08:58', '2021-10-31 00:11:08'),
(158, '71 Utsav Special Pizza(12\")', 'chinese', '163559246371-utsav-special-pizza12', '/uploads/images/products/image1635592463.png', NULL, 1, 980.00, NULL, 27, '2021-10-29 23:14:23', '2021-10-29 23:14:23'),
(159, 'Mexican Pizza(8\")', 'chinese', '1635592573mexican-pizza8', '/uploads/images/products/image1635672568.png', NULL, 1, 480.00, NULL, 27, '2021-10-29 23:16:13', '2021-10-30 21:29:28'),
(160, 'Mexican Pizza(10\")', 'chinese', '1635592621mexican-pizza10', '/uploads/images/products/image1635672614.png', NULL, 1, 600.00, NULL, 27, '2021-10-29 23:17:01', '2021-10-30 21:30:14'),
(161, 'Mexican Pizza(12\")', 'chinese', '1635592665mexican-pizza12', '/uploads/images/products/image1635672661.png', NULL, 1, 700.00, NULL, 27, '2021-10-29 23:17:45', '2021-10-30 21:31:01'),
(162, 'B.B.Q Pizza(8\")', 'chinese', '1635592764bbq-pizza8', '/uploads/images/products/image1635672692.png', NULL, 1, 480.00, NULL, 27, '2021-10-29 23:19:24', '2021-10-30 21:31:32'),
(163, 'B.B.Q Pizza(10\")', 'chinese', '1635592803bbq-pizza10', '/uploads/images/products/image1635672722.png', NULL, 1, 600.00, NULL, 27, '2021-10-29 23:20:03', '2021-10-30 21:32:02'),
(164, 'B.B.Q Pizza(12\")', 'chinese', '1635592868bbq-pizza12', '/uploads/images/products/image1635672766.png', NULL, 1, 700.00, NULL, 27, '2021-10-29 23:21:08', '2021-10-30 21:32:46'),
(165, 'Naga Pizza(8\")', 'chinese', '1635592907bbq-pizza8', '/uploads/images/products/image1635672787.png', NULL, 1, 480.00, NULL, 27, '2021-10-29 23:21:47', '2021-10-30 21:33:07'),
(166, 'Naga Pizza(10\")', 'chinese', '1635592938bbq-pizza10', '/uploads/images/products/image1635672805.png', NULL, 1, 600.00, NULL, 27, '2021-10-29 23:22:18', '2021-10-30 21:33:25'),
(167, 'Naga Pizza(12\")', 'chinese', '1635592973bbq-pizza12', '/uploads/images/products/image1635672825.png', NULL, 1, 700.00, NULL, 27, '2021-10-29 23:22:53', '2021-10-30 21:33:45'),
(168, 'Ciminati Coffee', 'chinese', '1635655473ciminati-coffee', '/uploads/images/products/image1635655773.png', NULL, 1, 100.00, NULL, 27, '2021-10-30 16:44:33', '2021-10-30 16:49:33'),
(169, 'White Mauka Coffee', 'chinese', '1635656004white-mauka-coffee', '/uploads/images/products/image1635656004.png', NULL, 1, 120.00, NULL, 27, '2021-10-30 16:53:24', '2021-10-30 16:53:24'),
(170, 'Coffee Regular (Hot)', 'chinese', '1635656281coffee-regular-hot', '/uploads/images/products/image1635656281.png', NULL, 1, 80.00, NULL, 27, '2021-10-30 16:58:01', '2021-10-30 16:58:01'),
(171, 'Black Coffee', 'chinese', '1635656364black-coffee', '/uploads/images/products/image1635674106.png', NULL, 1, 60.00, NULL, 27, '2021-10-30 16:59:24', '2021-10-30 21:55:06'),
(172, 'Cold Coffee (Chocolate)', 'chinese', '1635656427cold-coffee-chocolate', '/uploads/images/products/image1635656427.png', NULL, 1, 100.00, NULL, 27, '2021-10-30 17:00:27', '2021-10-30 17:00:27'),
(173, 'Vanilla Shake', 'chinese', '1635656602vanilla-shake', '/uploads/images/products/image1635683565.png', NULL, 1, 150.00, NULL, 27, '2021-10-30 17:03:22', '2021-10-31 00:32:45'),
(174, 'Chocolate Shake', 'chinese', '1635656659chocolate-shake', '/uploads/images/products/image1635682411.png', NULL, 1, 150.00, NULL, 27, '2021-10-30 17:04:19', '2021-10-31 00:13:31'),
(175, 'Casonate Shake', 'chinese', '1635656757casonate-shake', '/uploads/images/products/image1635683717.png', NULL, 1, 180.00, NULL, 27, '2021-10-30 17:05:57', '2021-10-31 00:35:17'),
(176, 'Strawberry Shake', 'chinese', '1635656825strawberry-shake', '/uploads/images/products/image1635683815.png', NULL, 1, 180.00, NULL, 27, '2021-10-30 17:07:05', '2021-10-31 00:36:55'),
(177, 'Mango Shake', 'chinese', '1635656924mango-shake', '/uploads/images/products/image1635683916.png', NULL, 1, 150.00, NULL, 27, '2021-10-30 17:08:44', '2021-10-31 00:38:36'),
(178, 'Oreo Shake', 'chinese', '1635656999oreo-shake', '/uploads/images/products/image1635684031.png', NULL, 1, 200.00, NULL, 27, '2021-10-30 17:09:59', '2021-10-31 00:40:31'),
(179, 'Kit-kat Shake', 'chinese', '1635657085kit-kat-shake', '/uploads/images/products/image1635684150.png', NULL, 1, 200.00, NULL, 27, '2021-10-30 17:11:25', '2021-10-31 00:42:30'),
(180, 'Sweet Lassi', 'chinese', '1635657148sweet-lassi', '/uploads/images/products/image1635657148.png', NULL, 1, 100.00, NULL, 27, '2021-10-30 17:12:28', '2021-10-30 17:12:28'),
(181, 'Salt Lassi', 'chinese', '1635657221salt-lassi', '/uploads/images/products/image1635684300.png', NULL, 1, 100.00, NULL, 27, '2021-10-30 17:13:41', '2021-10-31 00:45:00'),
(182, 'Mango Lassi', 'chinese', '1635657294mango-lassi', '/uploads/images/products/image1635657294.png', NULL, 1, 120.00, NULL, 27, '2021-10-30 17:14:54', '2021-10-30 17:14:54'),
(183, 'Faluda', 'chinese', '1635657413faluda', '/uploads/images/products/image1635684431.png', NULL, 1, 180.00, NULL, 27, '2021-10-30 17:16:53', '2021-10-31 00:47:11'),
(184, 'Orange Juice', 'chinese', '1635657489orange-juice', '/uploads/images/products/image1635684861.png', NULL, 1, 120.00, NULL, 27, '2021-10-30 17:18:09', '2021-10-31 00:54:21'),
(185, 'Pomegranate (Anar) Juice', 'chinese', '1635657630pomegranate-anar-juice', '/uploads/images/products/image1635657630.png', NULL, 1, 180.00, NULL, 27, '2021-10-30 17:20:30', '2021-10-30 17:20:30'),
(186, 'Mango Juice', 'chinese', '1635657673mango-juice', '/uploads/images/products/image1635684938.png', NULL, 1, 120.00, NULL, 27, '2021-10-30 17:21:13', '2021-10-31 00:55:38'),
(187, 'Green Mango Juice', 'chinese', '1635657730green-mango-juice', '/uploads/images/products/image1635657730.png', NULL, 1, 120.00, NULL, 27, '2021-10-30 17:22:10', '2021-10-30 17:22:10'),
(188, 'Lemon Mint', 'chinese', '1635657785lemon-mint', '/uploads/images/products/image1635685241.png', NULL, 1, 100.00, NULL, 27, '2021-10-30 17:23:05', '2021-10-31 01:00:41'),
(189, 'Grape Juice', 'chinese', '1635657858grape-juice', '/uploads/images/products/image1635657858.png', NULL, 1, 180.00, NULL, 27, '2021-10-30 17:24:18', '2021-10-30 17:24:18'),
(190, 'Dragon Juice', 'chinese', '1635657908dragon-juice', '/uploads/images/products/image1635685037.png', NULL, 1, 200.00, NULL, 27, '2021-10-30 17:25:08', '2021-10-31 00:57:17'),
(191, 'Garlic Chicken (1pic)', 'chinese', '1635658606garlic-chicken-1pic', '/uploads/images/products/image1635673667.png', NULL, 1, 100.00, NULL, 27, '2021-10-30 17:36:46', '2021-10-30 21:47:47'),
(192, 'Crispy Chicken (1pic)', 'chinese', '1635658662crispy-chicken-1pic', '/uploads/images/products/image1635673900.png', NULL, 1, 80.00, NULL, 27, '2021-10-30 17:37:42', '2021-10-30 21:51:40'),
(193, 'Chicken Drumstick (1pic)', 'chinese', '1635658718chicken-drumstick-1pic', '/uploads/images/products/image1635673435.png', NULL, 1, 100.00, NULL, 27, '2021-10-30 17:38:38', '2021-10-30 21:43:55'),
(194, 'Pop Chicken (10 pcs)', 'chinese', '1635658810pop-chicken-10-pcs', '/uploads/images/products/image1635658810.png', NULL, 1, 250.00, NULL, 27, '2021-10-30 17:40:10', '2021-10-30 17:40:10'),
(195, 'Naga Wings (6 pcs)', 'chinese', '1635658866naga-wings-6-pcs', '/uploads/images/products/image1635658866.png', NULL, 1, 200.00, NULL, 27, '2021-10-30 17:41:06', '2021-10-30 17:41:06'),
(196, 'Buffelo Wings (6 pcs)', 'chinese', '1635658924buffelo-wings-6-pcs', '/uploads/images/products/image1635673781.png', NULL, 1, 280.00, NULL, 27, '2021-10-30 17:42:04', '2021-10-30 21:49:41'),
(197, 'Crispy Hot Wings (6 pcs)', 'chinese', '1635658999crispy-hot-wings-6-pcs', '/uploads/images/products/image1635673573.png', NULL, 1, 220.00, NULL, 27, '2021-10-30 17:43:19', '2021-10-30 21:46:13'),
(198, 'B.B.Q Wings (6 pcs)', 'chinese', '1635659184bbq-wings-6-pcs', '/uploads/images/products/image1635659184.png', NULL, 1, 250.00, NULL, 27, '2021-10-30 17:46:24', '2021-10-30 17:46:24'),
(199, 'Chicken Lollypop (6pcs)', 'chinese', '1635659238chicken-lollypop-6pcs', '/uploads/images/products/image1635659238.png', NULL, 1, 280.00, NULL, 27, '2021-10-30 17:47:18', '2021-10-30 17:47:18'),
(200, 'Oven Bake Pasta', 'chinese', '1635659915oven-bake-pasta', '/uploads/images/products/image1635681232.png', NULL, 1, 300.00, NULL, 27, '2021-10-30 17:58:35', '2021-10-30 23:53:52'),
(201, 'Italian pasta', 'chinese', '1635659956italian-pasta', '/uploads/images/products/image1635659956.png', NULL, 1, 300.00, NULL, 27, '2021-10-30 17:59:16', '2021-10-30 17:59:16'),
(202, 'White Cream Pasta', 'chinese', '1635660003white-cream-pasta', '/uploads/images/products/image1635660003.png', NULL, 1, 200.00, NULL, 27, '2021-10-30 18:00:03', '2021-10-30 18:00:03'),
(203, 'Red Pasta', 'chinese', '1635660037red-pasta', '/uploads/images/products/image1635660037.png', NULL, 1, 200.00, NULL, 27, '2021-10-30 18:00:37', '2021-10-30 18:00:37'),
(204, 'Juicy Nachos Feista', 'chinese', '1635660149juicy-nachos-feista', '/uploads/images/products/image1635660149.png', NULL, 1, 260.00, NULL, 27, '2021-10-30 18:02:29', '2021-10-30 18:02:29'),
(205, 'Potato Masala Wedges (12 pcs)', 'chinese', '1635660398potato-masala-wedges-12-pcs', '/uploads/images/products/image1635660398.png', NULL, 1, 220.00, NULL, 27, '2021-10-30 18:06:38', '2021-10-30 18:06:38'),
(206, 'Chicken Sab Sandwich', 'chinese', '1635660466chicken-sab-sandwich', '/uploads/images/products/image1635680907.png', NULL, 1, 180.00, NULL, 27, '2021-10-30 18:07:46', '2021-10-30 23:48:27'),
(207, 'Chicken Cheese Sandwich', 'chinese', '1635660723chicken-cheese-sandwich', '/uploads/images/products/image1635660723.png', NULL, 1, 220.00, NULL, 27, '2021-10-30 18:12:03', '2021-10-30 18:12:03'),
(208, 'Smoke Cheese Sandwich', 'chinese', '1635660874smoke-cheese-sandwich', '/uploads/images/products/image1635660874.png', NULL, 1, 220.00, NULL, 27, '2021-10-30 18:14:34', '2021-10-30 18:14:34'),
(209, 'Love Sandwich', 'chinese', '1635660923love-sandwich', '/uploads/images/products/image1635660923.png', NULL, 1, 200.00, NULL, 27, '2021-10-30 18:15:23', '2021-10-30 18:15:23'),
(210, 'Club Sandwich', 'chinese', '1635660962club-sandwich', '/uploads/images/products/image1635660962.png', NULL, 1, 220.00, NULL, 27, '2021-10-30 18:16:02', '2021-10-30 18:16:02'),
(211, 'Chicken Sandwich', 'chinese', '1635661003chicken-sandwich', '/uploads/images/products/image1635661003.png', NULL, 1, 100.00, NULL, 27, '2021-10-30 18:16:43', '2021-10-30 18:16:43'),
(212, 'Soft Drinks', 'chinese', '1635661845soft-drinks', '/uploads/images/products/image1635661845.png', NULL, 1, 25.00, NULL, 27, '2021-10-30 18:30:45', '2021-10-30 18:30:45'),
(213, 'Mineral Water (Large)', 'chinese', '1635661885mineral-water', '/uploads/images/products/image1635661886.png', NULL, 1, 30.00, NULL, 27, '2021-10-30 18:31:25', '2021-10-30 18:32:01'),
(214, 'Mineral Water (Small)', 'chinese', '1635662176mineral-water-small', '/uploads/images/products/image1635662176.png', NULL, 1, 20.00, NULL, 27, '2021-10-30 18:36:16', '2021-10-30 18:36:16'),
(215, 'Can Drinks', 'chinese', '1635662225can-drinks', '/uploads/images/products/image1635662225.png', NULL, 1, 50.00, NULL, 27, '2021-10-30 18:37:05', '2021-10-30 18:37:05'),
(216, 'Red Bull Can', 'chinese', '1635662261red-bull-can', '/uploads/images/products/image1635662261.png', NULL, 1, 220.00, NULL, 27, '2021-10-30 18:37:41', '2021-10-30 18:37:41'),
(217, 'Special Awnthon', 'chinese', '1635768550special-awnthon', '/uploads/images/products/image1635768550.png', NULL, 1, 260.00, NULL, 27, '2021-11-01 00:09:10', '2021-11-01 00:09:10'),
(218, 'Fried Awnthon', 'chinese', '1635768623fried-awnthon', '/uploads/images/products/image1635768623.png', NULL, 1, 220.00, NULL, 27, '2021-11-01 00:10:23', '2021-11-01 00:10:23'),
(219, 'Vegetable Pakora', 'chinese', '1635768828vegetable-pakora', '/uploads/images/products/image1635768828.png', NULL, 1, 200.00, NULL, 27, '2021-11-01 00:13:48', '2021-11-01 00:13:48'),
(220, 'Prawn toast', 'chinese', '1635768941prawn-toast', '/uploads/images/products/image1635768942.png', NULL, 1, 260.00, NULL, 27, '2021-11-01 00:15:41', '2021-11-01 00:15:42'),
(221, 'Finger Roll', 'chinese', '1635769015finger-roll', '/uploads/images/products/image1635769015.png', NULL, 1, 240.00, NULL, 27, '2021-11-01 00:16:55', '2021-11-01 00:16:55'),
(222, 'Friench Fry', 'chinese', '1635769106friench-fry', '/uploads/images/products/image1635769106.png', NULL, 1, 150.00, NULL, 27, '2021-11-01 00:18:26', '2021-11-01 00:18:26'),
(223, 'Prawn Fry (10 pcs)', 'chinese', '1635769170prawn-fry-10-pcs', '/uploads/images/products/image1635769170.png', NULL, 1, 320.00, NULL, 27, '2021-11-01 00:19:30', '2021-11-01 00:19:30'),
(225, '71 Utsav Special Thai Soup', 'chinese', '163577449771-utsav-special-thai-soup', '/uploads/images/products/image1635774497.png', NULL, 1, 400.00, NULL, 27, '2021-11-01 01:48:17', '2021-11-01 01:48:17'),
(226, 'Tom Yum Soup (Gai/Goong)', 'chinese', '1635774586tom-yum-soup-gaigoong', '/uploads/images/products/image1635774587.png', NULL, 1, 400.00, NULL, 27, '2021-11-01 01:49:46', '2021-11-01 01:49:47'),
(227, 'Cocktail Soup', 'chinese', '1635774735cocktail-soup', '/uploads/images/products/image1635774735.png', NULL, 1, 400.00, NULL, 27, '2021-11-01 01:52:15', '2021-11-01 01:52:15'),
(228, 'Special Vegetable Soup', 'chinese', '1635774959special-vegetable-soup', '/uploads/images/products/image1635775635.png', NULL, 1, 380.00, NULL, 27, '2021-11-01 01:55:59', '2021-11-01 02:07:15'),
(229, 'Chicken Corn Soup', 'chinese', '1635775033chicken-corn-soup', '/uploads/images/products/image1635775033.png', NULL, 1, 270.00, NULL, 27, '2021-11-01 01:57:13', '2021-11-01 01:57:13'),
(230, 'Hot & Sour Soup', 'chinese', '1635775112hot-sour-soup', '/uploads/images/products/image1635775112.png', NULL, 1, 300.00, NULL, 27, '2021-11-01 01:58:32', '2021-11-01 01:58:32'),
(231, 'Special Corn Soup', 'chinese', '1635775166special-corn-soup', '/uploads/images/products/image1635775166.png', NULL, 1, 330.00, NULL, 27, '2021-11-01 01:59:26', '2021-11-01 01:59:26'),
(232, 'Chicken Onion Soup', 'chinese', '1635775346chicken-onion-soup', '/uploads/images/products/image1635775346.png', NULL, 1, 300.00, NULL, 27, '2021-11-01 02:02:26', '2021-11-01 02:02:26'),
(233, 'Steamed Chicken Soup', 'chinese', '1635775624steamed-chicken-soup', '/uploads/images/products/image1635775624.png', NULL, 1, 350.00, NULL, 27, '2021-11-01 02:07:04', '2021-11-01 02:07:04'),
(234, 'Special Sichuan Soup', 'chinese', '1635775724special-sichuan-soup', '/uploads/images/products/image1635775724.png', NULL, 1, 350.00, NULL, 27, '2021-11-01 02:08:44', '2021-11-01 02:08:44'),
(235, 'Sichuan Fried Rice', 'chinese', '1635829448sichuan-fried-rice', '/uploads/images/products/image1635829448.png', NULL, 1, 300.00, NULL, 27, '2021-11-01 17:04:08', '2021-11-01 17:04:08'),
(236, 'Steamed Rice', 'chinese', '1635829511steamed-rice', '/uploads/images/products/image1635829511.png', NULL, 1, 120.00, NULL, 27, '2021-11-01 17:05:11', '2021-11-01 17:05:11'),
(237, '71 Utsav Special Salad', 'chinese', '163583015671-utsav-special-salad', '/uploads/images/products/image1635830156.png', NULL, 1, 480.00, NULL, 27, '2021-11-01 17:15:56', '2021-11-01 17:15:56'),
(238, 'Chicken with Casonat Salad', 'chinese', '1635830236chicken-with-casonat-salad', '/uploads/images/products/image1635830236.png', NULL, 1, 450.00, NULL, 27, '2021-11-01 17:17:16', '2021-11-01 17:17:16'),
(239, 'Prawn with Casonat Salad', 'chinese', '1635830281prawn-with-casonat-salad', '/uploads/images/products/image1635830281.png', NULL, 1, 500.00, NULL, 27, '2021-11-01 17:18:01', '2021-11-01 17:18:01'),
(240, 'Green salad', 'chinese', '1635830340green-salad', '/uploads/images/products/image1635830340.png', NULL, 1, 80.00, NULL, 27, '2021-11-01 17:19:00', '2021-11-01 17:19:00'),
(241, 'Mutton Karai Sizzling', 'chinese', '1635843006mutton-karai-sizzling', '/uploads/images/products/image1635843006.png', NULL, 1, 480.00, NULL, 27, '2021-11-01 20:50:06', '2021-11-01 20:50:06'),
(242, 'Normal Package', 'chinese', '1636360976fried-rice-chicken-fry-2-piece-mix-vegetable', '/uploads/images/products/image1636360976.png', NULL, 1, 200.00, NULL, 27, '2021-11-07 20:42:56', '2021-11-18 01:02:20'),
(243, 'Special Package', 'chinese', '1636361253fried-rice-chicken-fry-2-piece-mix-vegetable-chicken-masala', '/uploads/images/products/image1636361253.png', NULL, 1, 250.00, NULL, 27, '2021-11-07 20:47:33', '2021-11-18 00:53:15'),
(244, 'Beef Package', 'chinese', '1636361312fried-rice-chicken-fry-2-piece-mix-vegetable-beef-chili-onion', '/uploads/images/products/image1636361312.png', NULL, 1, 280.00, NULL, 27, '2021-11-07 20:48:32', '2021-11-18 00:54:00'),
(245, 'Prawn Package', 'chinese', '1636361556fried-rice-chicken-fry-2-piece-mix-vegetable-prawn-masala', '/uploads/images/products/image1636361556.png', NULL, 1, 300.00, NULL, 27, '2021-11-07 20:52:36', '2021-11-18 01:01:54'),
(246, 'Ice Cream', 'chinese', '1636443122ice-cream', '/uploads/images/products/image1636443268.png', NULL, 1, 150.00, NULL, 27, '2021-11-08 19:32:02', '2021-11-08 19:34:28'),
(247, 'Room-No-301-(Double)', 'chinese', '1636545510room-no-1-double', '/uploads/images/products/image1636545510.png', NULL, 1, 1000.00, NULL, 27, '2021-11-09 23:58:30', '2021-11-23 20:04:58'),
(248, 'Room-No-302-(Double)', 'chinese', '1636545629room-no-2-double', '/uploads/images/products/image1636545629.png', NULL, 1, 1000.00, NULL, 27, '2021-11-10 00:00:29', '2021-11-23 20:05:11'),
(249, 'Room-No-303-(Single)', 'chinese', '1636545677room-no-3-single', '/uploads/images/products/image1636545677.png', NULL, 1, 600.00, NULL, 27, '2021-11-10 00:01:17', '2021-11-23 20:05:24'),
(250, 'Room-No-304-(Single)', 'chinese', '1636545726room-no-4-single', '/uploads/images/products/image1636545726.png', NULL, 1, 600.00, NULL, 27, '2021-11-10 00:02:06', '2021-11-23 20:05:39'),
(251, 'Room-No-305-(Double)', 'chinese', '1636545773room-no-5-double', '/uploads/images/products/image1636545773.png', NULL, 1, 1000.00, NULL, 27, '2021-11-10 00:02:53', '2021-11-23 20:05:52'),
(252, 'Chicken Cheese Burger', 'chinese', '1636728761chicken-cheese-burger', '/uploads/images/products/image1636728762.png', NULL, 1, 160.00, NULL, 27, '2021-11-12 02:52:42', '2021-11-12 02:52:42'),
(253, '71 Utsav Special Beef Burger', 'chinese', '163730301371-utsav-special-beef-burger', '/uploads/images/products/image1637303013.png', NULL, 1, 350.00, NULL, 27, '2021-11-18 18:23:33', '2021-11-18 18:23:33'),
(254, 'Small Coffee', 'fast-food', '1637742154small-coffee', '/uploads/images/products/image1637742154.png', NULL, 1, 30.00, NULL, 27, '2021-11-23 20:22:34', '2021-12-26 10:13:49');

-- --------------------------------------------------------

--
-- Table structure for table `products_categories`
--

CREATE TABLE `products_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products_categories`
--

INSERT INTO `products_categories` (`id`, `category_id`, `product_id`) VALUES
(11, 2, 2),
(12, 1, 2),
(13, 2, 1),
(14, 10, 10),
(15, 11, 10),
(16, 12, 10),
(17, 13, 10),
(18, 15, 10),
(19, 17, 20),
(20, 18, 21),
(21, 18, 22),
(22, 18, 23),
(23, 19, 24),
(24, 19, 25),
(25, 19, 26),
(26, 19, 27),
(27, 19, 28),
(28, 19, 29),
(29, 19, 30),
(30, 19, 31),
(31, 20, 32),
(32, 21, 33),
(33, 21, 34),
(34, 29, 35),
(35, 30, 36),
(36, 30, 37),
(37, 30, 38),
(38, 31, 39),
(39, 31, 40),
(40, 31, 41),
(41, 31, 42),
(42, 31, 43),
(43, 31, 44),
(44, 31, 45),
(45, 31, 46),
(46, 32, 47),
(47, 33, 48),
(48, 33, 49),
(49, 41, 50),
(50, 41, 51),
(51, 41, 52),
(52, 41, 53),
(53, 42, 54),
(56, 42, 57),
(57, 42, 58),
(64, 43, 65),
(65, 43, 66),
(66, 43, 67),
(67, 43, 68),
(68, 43, 69),
(69, 43, 70),
(70, 43, 71),
(71, 43, 72),
(72, 43, 73),
(73, 43, 74),
(81, 45, 82),
(82, 45, 83),
(83, 45, 84),
(84, 45, 85),
(85, 45, 86),
(86, 45, 87),
(87, 45, 88),
(88, 45, 89),
(89, 45, 90),
(90, 45, 91),
(91, 46, 92),
(92, 46, 93),
(93, 46, 94),
(94, 46, 95),
(95, 46, 96),
(96, 46, 97),
(97, 46, 98),
(98, 46, 99),
(99, 46, 100),
(112, 50, 113),
(113, 50, 114),
(114, 50, 115),
(116, 50, 117),
(130, 51, 131),
(132, 51, 133),
(133, 51, 134),
(134, 51, 135),
(135, 51, 136),
(143, 51, 132),
(149, 56, 145),
(150, 51, 145),
(159, 59, 150),
(160, 51, 150),
(175, 60, 158),
(176, 51, 158),
(197, 56, 168),
(198, 51, 168),
(199, 56, 169),
(200, 51, 169),
(201, 56, 170),
(202, 51, 170),
(205, 56, 172),
(206, 51, 172),
(221, 58, 180),
(222, 51, 180),
(225, 58, 182),
(226, 51, 182),
(231, 57, 185),
(232, 51, 185),
(235, 57, 187),
(236, 51, 187),
(239, 57, 189),
(240, 51, 189),
(249, 61, 194),
(250, 51, 194),
(251, 61, 195),
(252, 51, 195),
(257, 61, 198),
(258, 51, 198),
(259, 61, 199),
(260, 51, 199),
(263, 62, 201),
(264, 51, 201),
(265, 62, 202),
(266, 51, 202),
(267, 62, 203),
(268, 51, 203),
(269, 62, 204),
(270, 51, 204),
(271, 62, 205),
(272, 51, 205),
(275, 63, 207),
(276, 51, 207),
(277, 63, 208),
(278, 51, 208),
(279, 63, 209),
(280, 51, 209),
(281, 63, 210),
(282, 51, 210),
(283, 63, 211),
(284, 51, 211),
(298, 60, 159),
(299, 51, 159),
(300, 60, 160),
(301, 51, 160),
(302, 60, 161),
(303, 51, 161),
(304, 60, 162),
(305, 51, 162),
(306, 60, 163),
(307, 51, 163),
(308, 60, 164),
(309, 51, 164),
(310, 60, 165),
(311, 51, 165),
(312, 60, 166),
(313, 51, 166),
(314, 60, 167),
(315, 51, 167),
(316, 61, 193),
(317, 51, 193),
(318, 61, 197),
(319, 51, 197),
(320, 61, 191),
(321, 51, 191),
(322, 61, 196),
(323, 51, 196),
(324, 61, 192),
(325, 51, 192),
(326, 56, 171),
(327, 51, 171),
(328, 63, 206),
(329, 51, 206),
(330, 62, 200),
(331, 51, 200),
(332, 59, 151),
(333, 51, 151),
(334, 59, 152),
(335, 51, 152),
(336, 59, 153),
(337, 51, 153),
(338, 59, 154),
(339, 51, 154),
(340, 59, 155),
(341, 51, 155),
(342, 59, 156),
(343, 51, 156),
(344, 59, 157),
(345, 51, 157),
(346, 56, 174),
(347, 51, 174),
(348, 54, 146),
(349, 51, 146),
(352, 54, 148),
(353, 51, 148),
(354, 54, 149),
(355, 51, 149),
(358, 54, 147),
(359, 51, 147),
(360, 58, 173),
(361, 51, 173),
(362, 58, 175),
(363, 51, 175),
(364, 58, 176),
(365, 51, 176),
(366, 58, 177),
(367, 51, 177),
(368, 58, 178),
(369, 51, 178),
(370, 58, 179),
(371, 51, 179),
(372, 58, 181),
(373, 51, 181),
(374, 58, 183),
(375, 51, 183),
(376, 57, 184),
(377, 51, 184),
(378, 57, 186),
(379, 51, 186),
(380, 57, 190),
(381, 51, 190),
(382, 57, 188),
(383, 51, 188),
(384, 64, 212),
(385, 51, 212),
(386, 64, 213),
(387, 51, 213),
(388, 64, 214),
(389, 51, 214),
(390, 64, 215),
(391, 51, 215),
(392, 64, 216),
(393, 51, 216),
(394, 65, 217),
(395, 45, 217),
(396, 65, 218),
(397, 45, 218),
(398, 65, 219),
(399, 45, 219),
(400, 65, 220),
(401, 45, 220),
(402, 65, 221),
(403, 45, 221),
(404, 65, 222),
(405, 45, 222),
(406, 65, 223),
(407, 45, 223),
(408, 65, 224),
(409, 65, 225),
(410, 43, 225),
(411, 65, 226),
(412, 43, 226),
(413, 65, 227),
(414, 43, 227),
(417, 65, 229),
(418, 43, 229),
(419, 65, 230),
(420, 43, 230),
(421, 65, 231),
(422, 43, 231),
(423, 65, 232),
(424, 43, 232),
(425, 65, 233),
(426, 43, 233),
(427, 65, 228),
(428, 43, 228),
(429, 65, 234),
(430, 43, 234),
(433, 65, 79),
(434, 44, 79),
(435, 65, 80),
(436, 44, 80),
(437, 65, 81),
(438, 44, 81),
(441, 65, 76),
(442, 44, 76),
(443, 65, 77),
(444, 44, 77),
(445, 65, 78),
(446, 44, 78),
(447, 65, 75),
(448, 44, 75),
(449, 65, 143),
(450, 44, 143),
(451, 65, 235),
(452, 44, 235),
(453, 65, 236),
(454, 44, 236),
(455, 65, 237),
(456, 41, 237),
(457, 65, 238),
(458, 41, 238),
(459, 65, 239),
(460, 41, 239),
(461, 65, 240),
(462, 41, 240),
(463, 65, 64),
(464, 42, 64),
(465, 65, 59),
(466, 42, 59),
(469, 65, 61),
(470, 42, 61),
(471, 65, 62),
(472, 42, 62),
(473, 65, 63),
(474, 42, 63),
(475, 65, 55),
(476, 42, 55),
(477, 65, 56),
(478, 42, 56),
(479, 65, 60),
(480, 42, 60),
(481, 65, 141),
(482, 52, 141),
(483, 65, 142),
(484, 52, 142),
(485, 65, 140),
(486, 52, 140),
(487, 65, 138),
(488, 52, 138),
(489, 65, 139),
(490, 52, 139),
(491, 65, 137),
(492, 52, 137),
(493, 65, 129),
(494, 50, 129),
(497, 65, 130),
(498, 50, 130),
(499, 65, 126),
(500, 50, 126),
(501, 65, 127),
(502, 50, 127),
(503, 65, 128),
(504, 50, 128),
(505, 65, 125),
(506, 50, 125),
(507, 65, 123),
(508, 50, 123),
(509, 65, 124),
(510, 50, 124),
(511, 65, 121),
(512, 50, 121),
(513, 65, 122),
(514, 50, 122),
(515, 65, 120),
(516, 50, 120),
(517, 65, 119),
(518, 50, 119),
(519, 65, 118),
(520, 50, 118),
(521, 65, 116),
(522, 50, 116),
(535, 65, 112),
(536, 49, 112),
(537, 65, 241),
(538, 49, 241),
(539, 65, 111),
(540, 48, 111),
(541, 65, 109),
(542, 48, 109),
(543, 65, 110),
(544, 48, 110),
(545, 65, 106),
(546, 48, 106),
(547, 65, 107),
(548, 48, 107),
(549, 65, 108),
(550, 48, 108),
(551, 65, 104),
(552, 48, 104),
(553, 65, 105),
(554, 48, 105),
(571, 58, 246),
(572, 51, 246),
(578, 65, 103),
(579, 47, 103),
(583, 65, 101),
(584, 47, 101),
(585, 65, 102),
(586, 47, 102),
(593, 59, 252),
(594, 51, 252),
(620, 66, 243),
(621, 65, 243),
(622, 66, 244),
(623, 65, 244),
(624, 66, 245),
(625, 65, 245),
(626, 66, 242),
(627, 65, 242),
(654, 54, 253),
(655, 51, 253),
(656, 67, 247),
(657, 67, 248),
(658, 67, 249),
(659, 67, 250),
(660, 67, 251),
(663, 65, 144),
(664, 47, 144),
(665, 56, 254),
(666, 51, 254);

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `sub_total` double(20,3) DEFAULT NULL,
  `total` double(20,3) DEFAULT NULL,
  `discount` double(20,3) DEFAULT NULL,
  `paid` double(20,3) DEFAULT NULL,
  `due` double(20,3) DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_items`
--

CREATE TABLE `purchase_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `price` double(20,2) DEFAULT NULL,
  `quantity` double(20,3) DEFAULT NULL,
  `total` double(20,2) DEFAULT NULL,
  `previous_stock` double(20,3) DEFAULT NULL,
  `current_stock` double(20,3) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'super-admin', 'web', '2021-08-25 03:56:41', '2021-08-25 03:56:41'),
(2, 'admin', 'web', '2021-08-25 03:56:41', '2021-08-25 03:56:41'),
(3, 'moderator', 'web', '2021-08-25 03:56:41', '2021-08-25 03:56:41'),
(4, 'new', 'web', '2021-08-25 03:56:41', '2021-08-25 03:56:41');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 2),
(1, 3),
(2, 2),
(2, 3),
(3, 2),
(4, 2),
(5, 2),
(6, 2),
(7, 2),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 2),
(15, 2),
(16, 2),
(17, 2),
(18, 2),
(19, 2),
(20, 2),
(21, 2),
(22, 2),
(23, 2),
(24, 2);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salary` double(20,2) DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uid_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` timestamp NULL DEFAULT NULL,
  `join_date` timestamp NULL DEFAULT NULL,
  `staf_type_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `name`, `phone`, `email`, `image`, `address`, `salary`, `description`, `uid`, `uid_type`, `dob`, `join_date`, `staf_type_id`, `user_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'SWAPAN CHAKRABORTY', '01839776647', 'chakrabortysawpan@gmail', NULL, 'Village: Kumer Par, Post Office: Polli Kumer Par, Post Code: 7930, Upazila: Shibchar, District: Madaripur', 18000.00, 'He is a very intelligent man.', '7355772257', 'nid', '1983-08-11 12:00:00', '2016-03-13 12:00:00', 1, 27, 1, '2021-10-29 18:47:02', '2021-10-29 19:10:09', NULL),
(3, 'MAHMUDUL HASAN', '01961592206', 'mahmud96.bd@gmail.com', NULL, 'Muksudpur, Gopalgonj', 15000.00, NULL, '19963515833000060', 'nid', '1996-03-02 12:00:00', '2021-10-29 12:00:00', 2, 27, 1, '2021-10-29 19:01:47', '2021-10-29 19:14:40', NULL),
(4, 'MD. AZAHAR HOSSAIN', '01851774229', NULL, NULL, 'Village: Churkhar, Post: Churkhar, Upazila: Mymensingh Sadar, District: Mymensingh', 22000.00, NULL, '6115229621716', 'nid', '1987-05-31 12:00:00', '2016-12-31 12:00:00', 3, 27, 1, '2021-10-29 19:29:55', '2021-10-29 19:29:55', NULL),
(5, 'MD. FARUK SHEIKH', '01994094525', NULL, NULL, 'Village: Kuchiagram, Upazila: Alfadangga, District: Faridpur', 9000.00, NULL, '20032910342009909', 'birth', '2003-01-04 12:00:00', '2017-12-31 12:00:00', 4, 27, 1, '2021-10-29 19:40:22', '2021-10-29 19:40:22', NULL),
(6, 'MD. RABBI HOWlLADER', '01616808069', 'rabbydit@gmail.com', NULL, 'Village: Bahergat, Post: Bahergat, Upazila: Ujirpur, District: Barisal', 12000.00, NULL, '1502665944', 'nid', '1997-03-04 12:00:00', '2021-09-07 12:00:00', 5, 27, 1, '2021-10-29 19:52:28', '2021-11-04 17:56:41', NULL),
(7, 'TAMIM HOSSIAN SHIEKH', '01323638420', NULL, NULL, 'Village: Kuchiagram, Upazila: Alfadangga, District: Faridpur', 7000.00, NULL, '20062910342102694', 'birth', '2006-11-11 12:00:00', '2017-12-31 12:00:00', 6, 27, 1, '2021-10-29 20:01:06', '2021-10-29 20:01:06', NULL),
(8, 'MD. JAHID MALLIK', '01315306886', NULL, NULL, 'Village: Vabanipur, Upazila: Ujirpur, District: Barisal', 5000.00, NULL, '20040619463005475', 'birth', '2004-06-16 12:00:00', '2021-09-30 12:00:00', 7, 27, 1, '2021-10-29 20:48:27', '2021-10-29 20:48:27', NULL),
(9, 'MD. EMARAT HOSSAIN', '01752560501', NULL, NULL, 'Village: Kuchia, Post: Kuchia, Upazila: Alfadangga, District: Faridpur', 10000.00, NULL, '9116233223', 'nid', '1988-02-01 12:00:00', '2010-02-09 12:00:00', 8, 27, 1, '2021-10-29 20:55:39', '2021-10-29 20:55:39', NULL),
(10, 'SAMRAT MIA', '01575083036', NULL, NULL, 'Vill: Choudurikandi, Post: Chordatta Para, Upazila: Shibchar, District: Madaripur', 6000.00, NULL, '3754325441', 'nid', '1998-08-06 12:00:00', '2020-07-31 12:00:00', 10, 27, 1, '2021-10-30 19:38:27', '2021-10-31 03:07:22', NULL),
(11, 'AMIT MONDOL', '01315852024', NULL, NULL, 'Village: Carigau, Post: Hasara, Upazila: Sirinogor, District: Munshiganj', 6000.00, NULL, '7353673804', 'nid', '1997-04-23 12:00:00', '2018-12-31 12:00:00', 9, 27, 1, '2021-10-31 03:18:41', '2021-10-31 03:18:41', NULL),
(12, 'AKASH SHEIKH', '01323637150', NULL, NULL, 'Village: Nolgora, Post: Borhamgonj, Upazila: Shibchar, District: Madaripur', 7000.00, NULL, '7769805859', 'nid', '1993-04-05 12:00:00', '2018-12-31 12:00:00', 11, 27, 1, '2021-10-31 03:28:32', '2021-10-31 03:28:32', NULL),
(13, 'MD. MUSA KHA', '01810945038', NULL, NULL, 'Village: Kolabaria, Post: Kolabaria, Upazila: Noragati, District; Narail', 5000.00, NULL, '20066512847020683', 'birth', '2005-12-31 12:00:00', '2019-12-31 12:00:00', 13, 27, 1, '2021-10-31 03:58:43', '2021-10-31 03:58:43', NULL),
(14, 'ASRAB ALI SARDER', '01745821017', NULL, NULL, 'Village: Sarderkandi, Post: Borhamgonj, Upazila: Shibchar, District: Madaripur', 7000.00, NULL, '4627562426', 'nid', '1992-07-31 12:00:00', '2020-07-31 12:00:00', 15, 27, 1, '2021-10-31 04:38:58', '2021-11-04 17:55:43', NULL),
(15, 'LOKNATH KORMOKAR SUVO', '01985382868', NULL, NULL, 'Village: Poschim Dasra, District: Manikganj', 15000.00, NULL, '20015624606009467', 'birth', '2001-09-09 12:00:00', '2021-10-09 12:00:00', 12, 27, 1, '2021-10-31 04:51:04', '2021-10-31 04:51:04', NULL),
(16, 'BIPLOP KUNDU', '01710692451', NULL, NULL, 'Village: Ghuatola, Post: Borhamgonj, Upazila: Shibchar, District: Madaripur', 10000.00, NULL, '9109479783', 'nid', '1993-02-10 12:00:00', '2019-10-31 12:00:00', 16, 27, 1, '2021-10-31 17:23:31', '2021-10-31 17:23:31', NULL),
(17, 'NASIM MIA', '01739227697', NULL, NULL, 'Village: Dattapara, Post: Chor Dattapara, Upazila: Shibchar, District: Madaripur', 5000.00, NULL, '9161387106', 'nid', '2001-07-04 12:00:00', '2021-10-11 12:00:00', 17, 27, 1, '2021-10-31 17:30:51', '2021-10-31 17:30:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `staff_types`
--

CREATE TABLE `staff_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_types`
--

INSERT INTO `staff_types` (`id`, `name`, `user_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Manager', 27, 1, '2021-10-29 17:39:49', '2021-10-29 18:31:01', NULL),
(2, 'Software Developer', 27, 1, '2021-10-29 17:57:54', '2021-10-29 19:15:26', NULL),
(3, 'Chinese Chef', 27, 1, '2021-10-29 17:59:44', '2021-10-29 18:15:43', NULL),
(4, 'Assistant Chinese Chef', 27, 1, '2021-10-29 18:05:43', '2021-10-29 18:23:21', NULL),
(5, 'Fry Man', 27, 1, '2021-10-29 18:06:31', '2021-10-29 18:06:31', NULL),
(6, 'Pickup Man', 27, 1, '2021-10-29 18:06:56', '2021-10-29 18:06:56', NULL),
(7, 'Dish Washer', 27, 1, '2021-10-29 18:08:26', '2021-10-29 18:08:26', NULL),
(8, 'Waiter1(Senior)', 27, 1, '2021-10-29 18:08:45', '2021-10-29 18:09:57', NULL),
(9, 'Waiter2', 27, 1, '2021-10-29 18:09:38', '2021-10-29 18:09:38', NULL),
(10, 'Waiter3', 27, 1, '2021-10-29 18:09:46', '2021-10-29 18:09:46', NULL),
(11, 'Cleaner Man', 27, 1, '2021-10-29 18:10:37', '2021-10-29 18:10:37', NULL),
(12, 'Fast-Food Chef', 27, 1, '2021-10-29 18:16:09', '2021-10-31 03:30:11', NULL),
(13, 'Assistant Fast-Food Chef', 27, 1, '2021-10-29 18:17:18', '2021-10-31 03:31:00', NULL),
(15, 'Night Gard', 27, 1, '2021-10-29 18:21:47', '2021-10-29 18:21:47', NULL),
(16, 'Electronic Servicer', 27, 1, '2021-10-29 18:22:20', '2021-10-30 19:41:39', NULL),
(17, 'Waiter4', 27, 1, '2021-10-31 17:26:27', '2021-10-31 17:26:27', NULL),
(18, 'Residential Staff', 27, 1, '2021-10-31 17:35:16', '2021-10-31 17:35:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `perches_item_id` int(11) DEFAULT NULL,
  `item_id` int(11) NOT NULL,
  `in` tinyint(4) DEFAULT NULL,
  `weast` tinyint(4) DEFAULT NULL,
  `out` tinyint(4) DEFAULT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reason` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `warehouse` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `company`, `phone`, `email`, `image`, `password`, `address`, `warehouse`, `description`, `user_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Random Suppliers', 'M/S Enter Prize', '01965432762', NULL, NULL, NULL, '71 Road, Shibchar', '71 Road, Shibchar', NULL, 27, 1, '2021-11-04 01:54:06', '2021-11-04 01:54:06', NULL),
(2, 'Hakim Howlader', 'MS/Hakim Enter Prize', '01839776647', 'hanifsheikh@gmail.com', NULL, NULL, 'Shibchar, Madaripur', 'Shibchar, Madaripur', 'There are true Call', 27, 1, '2021-11-07 19:16:52', '2021-11-08 01:56:36', NULL),
(3, 'Md. Rabbi Shiekh', 'MS/Rabbi Enter Prize', '01978734689', 'mamun@gmail.com', NULL, NULL, 'Shibchar, Madaripur', '71 Road, Shibchar, Madaripur', NULL, 27, 1, '2021-11-07 19:21:00', '2021-11-08 02:20:14', NULL),
(6, 'Jabbar Shiekh', 'MS/Jabbar Enter Price', '01984789589', 'masum@gmail.com', NULL, NULL, 'Shibchar, Madaripur', '71 Road, Shibchar', NULL, 27, 1, '2021-11-07 19:26:41', '2021-11-08 02:01:43', NULL),
(7, 'Md. Rasel Mollah', 'MS/Rasel Enter prize', '01899867689', 'rasel@gmail.com', NULL, NULL, 'Shibchar, Madaripur', 'Shibchar, Madaripur', NULL, 27, 1, '2021-11-07 19:33:03', '2021-11-07 19:33:03', NULL),
(8, 'Najim Howlader', 'MS/Najim Howlader Enter Prize', '01987784742', NULL, NULL, NULL, '71 Road, Shibchar, Madaripur', 'Shibchar, Madaripur', NULL, 27, 1, '2021-11-08 02:24:05', '2021-11-08 02:24:05', NULL),
(9, 'Shakil Khandar', 'MS/Shakil Enter Prize', '01834948895', NULL, NULL, NULL, '71 Road, Shibchar, Madaripur', 'Shibchar Madaripur', NULL, 27, 1, '2021-11-08 02:28:08', '2021-11-08 02:28:08', NULL),
(10, 'Hakim Sheikh', 'MS/Hakim Enter Prize', '01974976908', NULL, NULL, NULL, 'Shibchar, Madaripur', 'Shibchar, Madaripur', NULL, 27, 1, '2021-11-08 17:08:24', '2021-11-08 17:08:24', NULL),
(11, 'Md. Saddam', 'MS/Saddam Enter Prize', '01897897988', NULL, NULL, NULL, 'Shibchar, Madaripur', '71 Road', NULL, 27, 1, '2021-11-08 17:09:42', '2021-11-08 17:09:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tables`
--

CREATE TABLE `tables` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `capasity` int(11) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tables`
--

INSERT INTO `tables` (`id`, `name`, `capasity`, `image`, `description`, `user_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(30, 'Table 1', 0, NULL, NULL, 1, 1, '2021-12-21 03:58:17', '2021-12-21 03:58:17', NULL),
(31, 'Table 2', 0, NULL, NULL, 1, 1, '2021-12-21 03:58:17', '2021-12-21 03:58:17', NULL),
(32, 'Table 3', 0, NULL, NULL, 1, 1, '2021-12-21 03:58:17', '2021-12-21 03:58:17', NULL),
(33, 'Table 4', 0, NULL, NULL, 1, 1, '2021-12-21 03:58:17', '2021-12-21 03:58:17', NULL),
(34, 'Table 5', 0, NULL, NULL, 1, 1, '2021-12-21 03:58:17', '2021-12-21 03:58:17', NULL),
(35, 'Table 6', 0, NULL, NULL, 1, 1, '2021-12-21 03:58:17', '2021-12-21 03:58:17', NULL),
(36, 'Table 7', 0, NULL, NULL, 1, 1, '2021-12-21 03:58:17', '2021-12-21 03:58:17', NULL),
(37, 'Table 8', 0, NULL, NULL, 1, 1, '2021-12-21 03:58:18', '2021-12-21 03:58:18', NULL),
(38, 'Table 9', 0, NULL, NULL, 1, 1, '2021-12-21 03:58:18', '2021-12-21 03:58:18', NULL),
(39, 'Table 10', 0, NULL, NULL, 1, 1, '2021-12-21 03:58:18', '2021-12-21 03:58:18', NULL),
(40, 'Table 11', 0, NULL, NULL, 1, 1, '2021-12-21 03:58:18', '2021-12-21 03:58:18', NULL),
(41, 'Table 12', 0, NULL, NULL, 1, 1, '2021-12-21 03:58:18', '2021-12-21 03:58:18', NULL),
(42, 'Table 13', 0, NULL, NULL, 1, 1, '2021-12-21 03:58:18', '2021-12-21 03:58:18', NULL),
(43, 'Table 14', 0, NULL, NULL, 1, 1, '2021-12-21 03:58:18', '2021-12-21 03:58:18', NULL),
(44, 'Table 15', 0, NULL, NULL, 1, 1, '2021-12-21 03:58:18', '2021-12-21 03:58:18', NULL),
(45, 'Table 16', 0, NULL, NULL, 1, 1, '2021-12-21 03:58:18', '2021-12-21 03:58:18', NULL),
(46, 'Table 17', 0, NULL, NULL, 1, 1, '2021-12-21 03:58:18', '2021-12-21 03:58:18', NULL),
(47, 'Table 18', 0, NULL, NULL, 1, 1, '2021-12-21 03:58:18', '2021-12-21 03:58:18', NULL),
(48, 'Table 19', 0, NULL, NULL, 1, 1, '2021-12-21 03:58:18', '2021-12-21 03:58:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `name`, `slug`, `image`, `description`, `user_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'কাচা বাজার', 'kaca-bajar', NULL, NULL, NULL, 1, '2021-09-03 17:26:42', '2021-09-03 17:26:42', NULL),
(4, 'মুদি বাজর', 'mudi-bajr', NULL, NULL, NULL, 1, '2021-09-03 17:26:42', '2021-09-03 17:26:42', NULL),
(5, 'মাছ', 'mach', NULL, NULL, NULL, 1, '2021-09-03 17:26:42', '2021-09-03 17:26:42', NULL),
(6, 'মাংস', 'mangs', NULL, NULL, NULL, 1, '2021-09-03 17:26:42', '2021-09-03 17:26:42', NULL),
(7, 'ডিম', 'dim', NULL, NULL, NULL, 1, '2021-09-03 17:26:42', '2021-09-03 17:26:42', NULL),
(9, 'বেভারেজ', 'bevarej', NULL, NULL, NULL, 1, '2021-09-03 17:26:43', '2021-09-03 17:26:43', NULL),
(10, 'জ্বালানি', 'jwalani', NULL, NULL, NULL, 1, '2021-09-03 17:26:43', '2021-09-03 17:26:43', NULL),
(11, 'ফার্নিচার', 'furniture', NULL, NULL, 27, 1, '2021-10-22 18:35:19', '2021-10-31 19:49:24', NULL),
(12, 'স্টেশনারী', 'stenotairy', NULL, NULL, 27, 1, '2021-10-22 19:23:12', '2021-10-31 19:48:04', NULL),
(14, 'চাউল', 'caul', NULL, NULL, 27, 1, '2021-11-02 20:54:44', '2021-11-02 20:54:44', NULL),
(15, 'চাইনিজ বাজার', 'cainij-bajar', NULL, NULL, 27, 1, '2021-11-03 00:56:58', '2021-11-03 00:56:58', NULL),
(16, 'আইসক্রিম', 'aiskrim', NULL, NULL, 27, 1, '2021-11-04 23:21:18', '2021-11-04 23:21:18', NULL),
(17, 'ফল', 'fl', NULL, NULL, 27, 1, '2021-11-05 19:35:46', '2021-11-05 19:35:46', NULL),
(18, 'মিষ্টি', 'mishti', NULL, NULL, 27, 1, '2021-11-07 00:36:05', '2021-11-07 00:36:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `name`, `user_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'এমজি', 27, 1, '2021-09-04 11:41:31', '2021-10-31 19:45:44', NULL),
(2, 'গ্রাম', 27, 1, '2021-09-03 16:41:31', '2021-10-31 19:41:11', NULL),
(3, 'কেজি', 27, 1, '2021-09-03 16:41:31', '2021-10-31 19:40:27', NULL),
(4, 'মিলিগ্রাম', 27, 1, '2021-09-03 16:41:31', '2021-10-31 19:42:31', NULL),
(5, 'লিটার', 27, 1, '2021-09-03 16:41:31', '2021-10-31 19:42:58', NULL),
(6, 'ডজন', 27, 1, '2021-09-03 16:41:31', '2021-10-31 19:44:20', NULL),
(7, 'বানডেল', 27, 1, '2021-09-03 16:41:31', '2021-10-31 19:44:45', NULL),
(8, 'পিচ', 27, 1, '2021-09-03 16:41:31', '2021-11-03 01:03:57', NULL),
(9, 'দিস্তা', 27, 1, '2021-10-22 19:22:01', '2021-10-31 19:39:41', NULL),
(10, 'বস্তা', 27, 1, '2021-11-02 19:51:23', '2021-11-02 19:51:23', NULL),
(11, 'কেচ', 27, 1, '2021-11-02 20:01:26', '2021-11-02 20:01:26', NULL),
(12, 'কার্টুন', 27, 1, '2021-11-03 00:58:25', '2021-11-03 00:58:25', NULL),
(13, 'হালি', 27, 1, '2021-11-05 19:46:01', '2021-11-05 19:46:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(191) DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `image`, `status`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Shakil', NULL, NULL, 'shakil@admin.com', NULL, '$2y$10$MSCHPAvylyE1RKK/hFVMtOeSqqJvCZ.pFQ/EKXz045M8qqaA.BWAO', '5ULdzzMEifZ53tF4cRXz9OWDntibWDQ2Eywb2SLvX7XudXR2JosS2pyucEen', '2021-06-13 02:19:41', '2021-06-13 02:19:41'),
(2, 'Khan', NULL, NULL, 'khan@admin.com', NULL, '$2y$10$368vXdvnW478B9B6NmFw1OhIi1wV7BpWU7erQJbsWQGpmLaHrdzVW', NULL, '2021-06-13 02:19:41', '2021-06-13 02:19:41'),
(3, 'Mahmud', NULL, 1, 'mahmud@admin.com', NULL, '$2y$10$WGjwR4SKcUF7wG60qgtOH.JQw8CCOIIOMQ7Y8DI9mCpGujj73wvKK', NULL, '2021-06-13 02:19:41', '2021-08-25 04:00:52'),
(4, 'Robin', NULL, NULL, 'robin@admin.com', NULL, '$2y$10$RZMFPe.ZkxR41hJW2Vu7suxI3um1EwGm2EtTuFznTr9ctMJ0qTGWW', NULL, '2021-06-13 02:19:41', '2021-06-13 02:19:41'),
(5, 'Asif', NULL, NULL, 'asif@admin.com', NULL, '$2y$10$LnPYCQREN92./WffqJZ0GOdezCC.6dhBO45ouqV5nxdCr3Z6cYg1y', NULL, '2021-06-13 02:19:41', '2021-06-13 02:19:41'),
(6, 'Tatyana Ullrich', NULL, NULL, 'henderson13@example.net', '2021-09-05 00:08:36', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ONRtgwC0Np', '2021-09-05 00:08:36', '2021-09-05 00:08:36'),
(7, 'Cleve Wintheiser', NULL, NULL, 'erica.ziemann@example.net', '2021-09-05 00:08:36', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'RZCEk0EPbx', '2021-09-05 00:08:36', '2021-09-05 00:08:36'),
(8, 'Phyllis Cassin', NULL, NULL, 'bria.nikolaus@example.com', '2021-09-05 00:08:36', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'zUbXs0kX3g', '2021-09-05 00:08:36', '2021-09-05 00:08:36'),
(9, 'Gerson Wilderman', NULL, NULL, 'heller.harley@example.com', '2021-09-05 00:08:36', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'L7T5L0Mu1Q', '2021-09-05 00:08:36', '2021-09-05 00:08:36'),
(10, 'Estrella Pfeffer', NULL, NULL, 'sdibbert@example.net', '2021-09-05 00:08:36', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'oawVQCSus9', '2021-09-05 00:08:36', '2021-09-05 00:08:36'),
(11, 'Prof. Lilla Stokes Sr.', NULL, NULL, 'annabelle.schmeler@example.com', '2021-09-05 00:08:36', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'tpUoefMmYA', '2021-09-05 00:08:37', '2021-09-05 00:08:37'),
(12, 'Schuyler Fritsch', NULL, NULL, 'ronny88@example.org', '2021-09-05 00:08:36', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Twn7BaRTRh', '2021-09-05 00:08:37', '2021-09-05 00:08:37'),
(13, 'Toney Feil', NULL, NULL, 'kgerlach@example.org', '2021-09-05 00:08:36', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'kFiNxaLGmQ', '2021-09-05 00:08:37', '2021-09-05 00:08:37'),
(14, 'Marcia Ward DDS', NULL, NULL, 'pdooley@example.net', '2021-09-05 00:08:36', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '6xRSkiUsLx', '2021-09-05 00:08:37', '2021-09-05 00:08:37'),
(15, 'Dr. Thomas Bechtelar', NULL, NULL, 'georgianna17@example.com', '2021-09-05 00:08:36', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'SBKiPrAaJo', '2021-09-05 00:08:37', '2021-09-05 00:08:37'),
(16, 'Randy Schowalter', NULL, NULL, 'torp.garrison@example.com', '2021-09-05 00:09:09', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'i6rZ1f3k4V', '2021-09-05 00:09:09', '2021-09-05 00:09:09'),
(17, 'Prof. Terrill Wintheiser', NULL, NULL, 'leanne.borer@example.org', '2021-09-05 00:09:09', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'FhDjLT5Bg1', '2021-09-05 00:09:09', '2021-09-05 00:09:09'),
(18, 'Murl O\'Keefe I', NULL, NULL, 'wgibson@example.net', '2021-09-05 00:09:09', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'QDnNwFLt47', '2021-09-05 00:09:09', '2021-09-05 00:09:09'),
(19, 'Dr. Lucie Nolan', NULL, NULL, 'ywitting@example.com', '2021-09-05 00:09:09', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'atdqR4FZ27', '2021-09-05 00:09:09', '2021-09-05 00:09:09'),
(20, 'Candido Turcotte', NULL, NULL, 'jhilpert@example.com', '2021-09-05 00:09:09', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'bqgpRrJLuq', '2021-09-05 00:09:09', '2021-09-05 00:09:09'),
(21, 'Dr. Yadira Lind Sr.', NULL, NULL, 'colleen83@example.net', '2021-09-05 00:09:09', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'THnNDCRkQM', '2021-09-05 00:09:09', '2021-09-05 00:09:09'),
(22, 'Twila Blanda DVM', NULL, NULL, 'yturner@example.org', '2021-09-05 00:09:09', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'GoQHveDnKz', '2021-09-05 00:09:09', '2021-09-05 00:09:09'),
(23, 'Melissa Collier', NULL, NULL, 'sterling.carter@example.net', '2021-09-05 00:09:09', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ChvYt7bj4i', '2021-09-05 00:09:09', '2021-09-05 00:09:09'),
(24, 'Dr. Ronny O\'Reilly Sr.', NULL, NULL, 'schowalter.aurelie@example.net', '2021-09-05 00:09:09', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ImuG7IPUPt', '2021-09-05 00:09:09', '2021-09-05 00:09:09'),
(25, 'Melody Miller', NULL, NULL, 'ccassin@example.org', '2021-09-05 00:09:09', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'zy2yFeu9Iu', '2021-09-05 00:09:09', '2021-09-05 00:09:09'),
(26, 'hasan', NULL, 1, 'hasan@admin.com', NULL, '$2y$10$zNxVl9trOKhPwk4i1Dt5Pur6tmk5L8vQNPqtvV20dkIruaDQnH6U.', NULL, '2021-10-23 01:06:58', '2021-10-23 01:06:58');

-- --------------------------------------------------------

--
-- Table structure for table `verifybackup`
--

CREATE TABLE `verifybackup` (
  `id` int(10) UNSIGNED NOT NULL,
  `verify_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `verifybackup`
--

INSERT INTO `verifybackup` (`id`, `verify_status`) VALUES
(1, 'backup');

-- --------------------------------------------------------

--
-- Table structure for table `websites`
--

CREATE TABLE `websites` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_charge` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fb` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iframe` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `services` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_us` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_3` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `web_infos`
--

CREATE TABLE `web_infos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_charge` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fb` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iframe` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `services` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_us` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_3` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `web_infos`
--

INSERT INTO `web_infos` (`id`, `name`, `logo`, `icon`, `address`, `service_charge`, `vat`, `tax`, `email`, `phone`, `fb`, `twitter`, `instagram`, `youtube`, `iframe`, `image_1`, `image_2`, `image_3`, `image_4`, `services`, `about_us`, `text_3`, `text_4`, `created_at`, `updated_at`) VALUES
(1, '71 Utsav Chinese Restaurant & Party Center', '/uploads/images/web/imagelogo_1634972710.png', '/uploads/images/web/imageicon_1634972711.png', 'Madaripur Rd, Shibchar.', '0%', '0%', 'Est soluta molestia', 'xepyxu@mailinator.com', '01812391633', 'Illo corrupti dolor', 'Voluptates laudantiu', NULL, 'Illum ex amet libe', 'Ullamco enim quae su', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-20 01:06:51', '2021-12-21 04:10:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assets`
--
ALTER TABLE `assets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asset_categories`
--
ALTER TABLE `asset_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asset_purchases`
--
ALTER TABLE `asset_purchases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asset_purchase_items`
--
ALTER TABLE `asset_purchase_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendances`
--
ALTER TABLE `attendances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_phone_unique` (`phone`);

--
-- Indexes for table `expences`
--
ALTER TABLE `expences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expence_categories`
--
ALTER TABLE `expence_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incomes`
--
ALTER TABLE `incomes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `income_categories`
--
ALTER TABLE `income_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leaves`
--
ALTER TABLE `leaves`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ledgers`
--
ALTER TABLE `ledgers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ledgers_ledgerable_type_ledgerable_id_index` (`ledgerable_type`,`ledgerable_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `orders_order_code_unique` (`order_code`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payrolls`
--
ALTER TABLE `payrolls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_categories`
--
ALTER TABLE `products_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_items`
--
ALTER TABLE `purchase_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `staff_phone_unique` (`phone`),
  ADD UNIQUE KEY `staff_email_unique` (`email`);

--
-- Indexes for table `staff_types`
--
ALTER TABLE `staff_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `suppliers_phone_unique` (`phone`),
  ADD UNIQUE KEY `suppliers_email_unique` (`email`);

--
-- Indexes for table `tables`
--
ALTER TABLE `tables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `verifybackup`
--
ALTER TABLE `verifybackup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `websites`
--
ALTER TABLE `websites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_infos`
--
ALTER TABLE `web_infos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assets`
--
ALTER TABLE `assets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `asset_categories`
--
ALTER TABLE `asset_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `asset_purchases`
--
ALTER TABLE `asset_purchases`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `asset_purchase_items`
--
ALTER TABLE `asset_purchase_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attendances`
--
ALTER TABLE `attendances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `expences`
--
ALTER TABLE `expences`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `expence_categories`
--
ALTER TABLE `expence_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `incomes`
--
ALTER TABLE `incomes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `income_categories`
--
ALTER TABLE `income_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;

--
-- AUTO_INCREMENT for table `leaves`
--
ALTER TABLE `leaves`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ledgers`
--
ALTER TABLE `ledgers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `payrolls`
--
ALTER TABLE `payrolls`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=255;

--
-- AUTO_INCREMENT for table `products_categories`
--
ALTER TABLE `products_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=667;

--
-- AUTO_INCREMENT for table `purchases`
--
ALTER TABLE `purchases`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `purchase_items`
--
ALTER TABLE `purchase_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `staff_types`
--
ALTER TABLE `staff_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tables`
--
ALTER TABLE `tables`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `verifybackup`
--
ALTER TABLE `verifybackup`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `websites`
--
ALTER TABLE `websites`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `web_infos`
--
ALTER TABLE `web_infos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
